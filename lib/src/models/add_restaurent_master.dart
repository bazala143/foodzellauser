class AddRestaurentMaster {
  bool _success;
  String _message;
  Result _result;

  AddRestaurentMaster({bool success, String message, Result result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  bool get success => _success;

  set success(bool success) => _success = success;

  String get message => _message;

  set message(String message) => _message = message;

  Result get result => _result;

  set result(Result result) => _result = result;

  AddRestaurentMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class Result {
  String _name;
  int _deliveryFee;
  int _adminCommission;
  String _phone;
  String _mobile;
  String _address;
  String _latitude;
  String _longitude;
  String _description;
  String _information;
  String _updatedAt;
  String _createdAt;
  int _id;
  List<Null> _customFields;
  bool _hasMedia;
  Null _rate;
  List<Media> _media;

  Result(
      {String name,
      int deliveryFee,
      int adminCommission,
      String phone,
      String mobile,
      String address,
      String latitude,
      String longitude,
      String description,
      String information,
      String updatedAt,
      String createdAt,
      int id,
      List<Null> customFields,
      bool hasMedia,
      Null rate,
      List<Media> media}) {
    this._name = name;
    this._deliveryFee = deliveryFee;
    this._adminCommission = adminCommission;
    this._phone = phone;
    this._mobile = mobile;
    this._address = address;
    this._latitude = latitude;
    this._longitude = longitude;
    this._description = description;
    this._information = information;
    this._updatedAt = updatedAt;
    this._createdAt = createdAt;
    this._id = id;
    this._customFields = customFields;
    this._hasMedia = hasMedia;
    this._rate = rate;
    this._media = media;
  }

  String get name => _name;

  set name(String name) => _name = name;

  int get deliveryFee => _deliveryFee;

  set deliveryFee(int deliveryFee) => _deliveryFee = deliveryFee;

  int get adminCommission => _adminCommission;

  set adminCommission(int adminCommission) =>
      _adminCommission = adminCommission;

  String get phone => _phone;

  set phone(String phone) => _phone = phone;

  String get mobile => _mobile;

  set mobile(String mobile) => _mobile = mobile;

  String get address => _address;

  set address(String address) => _address = address;

  String get latitude => _latitude;

  set latitude(String latitude) => _latitude = latitude;

  String get longitude => _longitude;

  set longitude(String longitude) => _longitude = longitude;

  String get description => _description;

  set description(String description) => _description = description;

  String get information => _information;

  set information(String information) => _information = information;

  String get updatedAt => _updatedAt;

  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  String get createdAt => _createdAt;

  set createdAt(String createdAt) => _createdAt = createdAt;

  int get id => _id;

  set id(int id) => _id = id;

  List<Null> get customFields => _customFields;

  set customFields(List<Null> customFields) => _customFields = customFields;

  bool get hasMedia => _hasMedia;

  set hasMedia(bool hasMedia) => _hasMedia = hasMedia;

  Null get rate => _rate;

  set rate(Null rate) => _rate = rate;

  List<Media> get media => _media;

  set media(List<Media> media) => _media = media;

  Result.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _deliveryFee = json['delivery_fee'];
    _adminCommission = json['admin_commission'];
    _phone = json['phone'];
    _mobile = json['mobile'];
    _address = json['address'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _description = json['description'];
    _information = json['information'];
    _updatedAt = json['updated_at'];
    _createdAt = json['created_at'];
    _id = json['id'];

    _hasMedia = json['has_media'];
    _rate = json['rate'];
    if (json['media'] != null) {
      _media = new List<Media>();
      json['media'].forEach((v) {
        _media.add(new Media.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['delivery_fee'] = this._deliveryFee;
    data['admin_commission'] = this._adminCommission;
    data['phone'] = this._phone;
    data['mobile'] = this._mobile;
    data['address'] = this._address;
    data['latitude'] = this._latitude;
    data['longitude'] = this._longitude;
    data['description'] = this._description;
    data['information'] = this._information;
    data['updated_at'] = this._updatedAt;
    data['created_at'] = this._createdAt;
    data['id'] = this._id;

    data['has_media'] = this._hasMedia;
    data['rate'] = this._rate;
    if (this._media != null) {
      data['media'] = this._media.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Media {
  int _id;
  String _modelType;
  int _modelId;
  String _collectionName;
  String _name;
  String _fileName;
  String _mimeType;
  String _disk;
  int _size;
  CustomProperties _customProperties;
  int _orderColumn;
  String _createdAt;
  String _updatedAt;
  String _url;
  String _thumb;
  String _icon;
  String _formatedSize;

  Media(
      {int id,
      String modelType,
      int modelId,
      String collectionName,
      String name,
      String fileName,
      String mimeType,
      String disk,
      int size,
      CustomProperties customProperties,
      int orderColumn,
      String createdAt,
      String updatedAt,
      String url,
      String thumb,
      String icon,
      String formatedSize}) {
    this._id = id;
    this._modelType = modelType;
    this._modelId = modelId;
    this._collectionName = collectionName;
    this._name = name;
    this._fileName = fileName;
    this._mimeType = mimeType;
    this._disk = disk;
    this._size = size;
    this._customProperties = customProperties;
    this._orderColumn = orderColumn;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._url = url;
    this._thumb = thumb;
    this._icon = icon;
    this._formatedSize = formatedSize;
  }

  int get id => _id;

  set id(int id) => _id = id;

  String get modelType => _modelType;

  set modelType(String modelType) => _modelType = modelType;

  int get modelId => _modelId;

  set modelId(int modelId) => _modelId = modelId;

  String get collectionName => _collectionName;

  set collectionName(String collectionName) => _collectionName = collectionName;

  String get name => _name;

  set name(String name) => _name = name;

  String get fileName => _fileName;

  set fileName(String fileName) => _fileName = fileName;

  String get mimeType => _mimeType;

  set mimeType(String mimeType) => _mimeType = mimeType;

  String get disk => _disk;

  set disk(String disk) => _disk = disk;

  int get size => _size;

  set size(int size) => _size = size;

  CustomProperties get customProperties => _customProperties;

  set customProperties(CustomProperties customProperties) =>
      _customProperties = customProperties;

  int get orderColumn => _orderColumn;

  set orderColumn(int orderColumn) => _orderColumn = orderColumn;

  String get createdAt => _createdAt;

  set createdAt(String createdAt) => _createdAt = createdAt;

  String get updatedAt => _updatedAt;

  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  String get url => _url;

  set url(String url) => _url = url;

  String get thumb => _thumb;

  set thumb(String thumb) => _thumb = thumb;

  String get icon => _icon;

  set icon(String icon) => _icon = icon;

  String get formatedSize => _formatedSize;

  set formatedSize(String formatedSize) => _formatedSize = formatedSize;

  Media.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _modelType = json['model_type'];
    _modelId = json['model_id'];
    _collectionName = json['collection_name'];
    _name = json['name'];
    _fileName = json['file_name'];
    _mimeType = json['mime_type'];
    _disk = json['disk'];
    _size = json['size'];

    _customProperties = json['custom_properties'] != null
        ? new CustomProperties.fromJson(json['custom_properties'])
        : null;

    _orderColumn = json['order_column'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _url = json['url'];
    _thumb = json['thumb'];
    _icon = json['icon'];
    _formatedSize = json['formated_size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['model_type'] = this._modelType;
    data['model_id'] = this._modelId;
    data['collection_name'] = this._collectionName;
    data['name'] = this._name;
    data['file_name'] = this._fileName;
    data['mime_type'] = this._mimeType;
    data['disk'] = this._disk;
    data['size'] = this._size;

    if (this._customProperties != null) {
      data['custom_properties'] = this._customProperties.toJson();
    }

    data['order_column'] = this._orderColumn;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['url'] = this._url;
    data['thumb'] = this._thumb;
    data['icon'] = this._icon;
    data['formated_size'] = this._formatedSize;
    return data;
  }
}

class CustomProperties {
  String _uuid;
  GeneratedConversions _generatedConversions;

  CustomProperties({String uuid, GeneratedConversions generatedConversions}) {
    this._uuid = uuid;
    this._generatedConversions = generatedConversions;
  }

  String get uuid => _uuid;

  set uuid(String uuid) => _uuid = uuid;

  GeneratedConversions get generatedConversions => _generatedConversions;

  set generatedConversions(GeneratedConversions generatedConversions) =>
      _generatedConversions = generatedConversions;

  CustomProperties.fromJson(Map<String, dynamic> json) {
    _uuid = json['uuid'];
    _generatedConversions = json['generated_conversions'] != null
        ? new GeneratedConversions.fromJson(json['generated_conversions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this._uuid;
    if (this._generatedConversions != null) {
      data['generated_conversions'] = this._generatedConversions.toJson();
    }
    return data;
  }
}

class GeneratedConversions {
  bool _thumb;
  bool _icon;

  GeneratedConversions({bool thumb, bool icon}) {
    this._thumb = thumb;
    this._icon = icon;
  }

  bool get thumb => _thumb;

  set thumb(bool thumb) => _thumb = thumb;

  bool get icon => _icon;

  set icon(bool icon) => _icon = icon;

  GeneratedConversions.fromJson(Map<String, dynamic> json) {
    _thumb = json['thumb'];
    _icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumb'] = this._thumb;
    data['icon'] = this._icon;
    return data;
  }
}
