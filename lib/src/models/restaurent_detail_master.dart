import 'delivery_boys_master.dart';

class RestaurentDetailMaster {
  bool _success;
  String _message;
  RestaurentDetail _result;

  RestaurentDetailMaster(
      {bool success, String message, RestaurentDetail result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  RestaurentDetail get result => _result;
  set result(RestaurentDetail result) => _result = result;

  RestaurentDetailMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result = json['result'] != null
        ? new RestaurentDetail.fromJson(json['result'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class RestaurentDetail {
  String _id;
  String _name;
  String _description;
  String _address;
  String _latitude;
  String _longitude;
  String _phone;
  String _mobile;
  String _adminCommission;
  String _deliveryFee;
  String _information;
  String _image;
  String _rating;
  List<DeliveryBoysDetails> deliveryBoy;

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  RestaurentDetail(
      {String id,
      String name,
      String description,
      String address,
      String latitude,
      String longitude,
      String phone,
      String mobile,
      String adminCommission,
      String deliveryFee,
      String information,
      String image,
      String rating}) {
    this._id = id;
    this._name = name;
    this._description = description;
    this._address = address;
    this._latitude = latitude;
    this._longitude = longitude;
    this._phone = phone;
    this._mobile = mobile;
    this._adminCommission = adminCommission;
    this._deliveryFee = deliveryFee;
    this._information = information;
    this._image = image;
    this._rating = rating;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get address => _address;
  set address(String address) => _address = address;
  String get latitude => _latitude;
  set latitude(String latitude) => _latitude = latitude;
  String get longitude => _longitude;
  set longitude(String longitude) => _longitude = longitude;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;

  String get information => _information;
  set information(String information) => _information = information;
  String get image => _image;
  set image(String image) => _image = image;
  String get rating => _rating;
  set rating(String rating) => _rating = rating;

  RestaurentDetail.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _name = json['name'];
    _description = json['description'];
    _address = json['address'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _phone = json['phone'].toString();
    _mobile = json['mobile'].toString();
    _adminCommission = json['admin_commission'].toString();
    _deliveryFee = json['delivery_fee'].toString();
    _information = json['information'];
    _image = json['image'];
    _rating = json['rating'].toString();
    if (json['deliveryBoy'] != null) {
      deliveryBoy = new List<DeliveryBoysDetails>();
      json['deliveryBoy'].forEach((v) {
        deliveryBoy.add(new DeliveryBoysDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['description'] = this._description;
    data['address'] = this._address;
    data['latitude'] = this._latitude;
    data['longitude'] = this._longitude;
    data['phone'] = this._phone;
    data['mobile'] = this._mobile;
    data['admin_commission'] = this._adminCommission;
    data['delivery_fee'] = this._deliveryFee;
    data['information'] = this._information;
    data['image'] = this._image;
    data['rating'] = this._rating;
    if (this.deliveryBoy != null) {
      data['deliveryBoy'] = this.deliveryBoy.map((v) => v.toJson()).toList();
    }
    return data;
  }

  String get adminCommission => _adminCommission;

  set adminCommission(String value) {
    _adminCommission = value;
  }

  String get deliveryFee => _deliveryFee;

  set deliveryFee(String value) {
    _deliveryFee = value;
  }
}
