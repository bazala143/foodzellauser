class StatusMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<StatusDetails> _result;

  StatusMaster(
      {bool success,
      String message,
      int totalCount,
      List<StatusDetails> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  int get totalCount => _totalCount;
  set totalCount(int totalCount) => _totalCount = totalCount;
  List<StatusDetails> get result => _result;
  set result(List<StatusDetails> result) => _result = result;

  StatusMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<StatusDetails>();
      json['result'].forEach((v) {
        _result.add(new StatusDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class StatusDetails {
  int _id;
  String _name;
  bool _isSelected;

  StatusDetails({int id, String name}) {
    this._id = id;
    this._name = name;
  }

  bool get isSelected => _isSelected ?? false;

  set isSelected(bool value) {
    _isSelected = value ?? false;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;

  StatusDetails.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    return data;
  }
}
