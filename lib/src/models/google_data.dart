class GoogleData {
  String _name;
  String _email;
  String _profilePic;
  String _appleId;

  GoogleData({String name, String email, String profilePic}) {
    this._name = name;
    this._email = email;
    this._profilePic = profilePic;
  }

  String get appleId => _appleId;

  set appleId(String value) {
    _appleId = value;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get profilePic => _profilePic;
  set profilePic(String profilePic) => _profilePic = profilePic;

  GoogleData.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _email = json['email'];
    _profilePic = json['profilePic'];
    _appleId = json['appleId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['email'] = this._email;
    data['profilePic'] = this._profilePic;
    data['appleId'] = this._appleId;
    return data;
  }
}
