class FoodRequest {
  String _id;
  String _name;
  String _price;
  String _discountPrice;
  String _description;
  String _ingredients;
  String _weight;
  String _featured;
  String _restaurantId;
  String _categoryId;

  FoodRequest();

  String get name => _name;

  set name(String name) => _name = name;

  String get description => _description;

  set description(String description) => _description = description;

  String get ingredients => _ingredients;

  set ingredients(String ingredients) => _ingredients = ingredients;

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  FoodRequest.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _price = json['price'];
    _discountPrice = json['discountPrice'];
    _description = json['description'];
    _ingredients = json['ingredients'];
    _weight = json['weight'];
    _featured = json['featured'];
    _restaurantId = json['restaurantId'];
    _categoryId = json['categoryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['price'] = this._price;
    data['discountPrice'] = this._discountPrice;
    data['description'] = this._description;
    data['ingredients'] = this._ingredients;
    data['weight'] = this._weight;
    data['featured'] = this._featured;
    data['restaurantId'] = this._restaurantId;
    data['categoryId'] = this._categoryId;
    return data;
  }

  String get discountPrice => _discountPrice;

  set discountPrice(String value) {
    _discountPrice = value;
  }

  String get weight => _weight;

  set weight(String value) {
    _weight = value;
  }

  String get featured => _featured;

  set featured(String value) {
    _featured = value;
  }

  String get restaurantId => _restaurantId;

  set restaurantId(String value) {
    _restaurantId = value;
  }

  String get categoryId => _categoryId;

  set categoryId(String value) {
    _categoryId = value;
  }
}
