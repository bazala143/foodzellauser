class DashBoardMaster {
  bool _success;
  String _message;
  DashBoardDetails _result;

  DashBoardMaster({bool success, String message, DashBoardDetails result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  DashBoardDetails get result => _result;
  set result(DashBoardDetails result) => _result = result;

  DashBoardMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result = json['result'] != null
        ? new DashBoardDetails.fromJson(json['result'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class DashBoardDetails {
  String _earning;
  String _restaurent;
  String _food;
  String _orders;

  DashBoardDetails(
      {String earning, String restaurent, String food, String orders}) {
    this._earning = earning;
    this._restaurent = restaurent;
    this._food = food;
    this._orders = orders;
  }

  String get earning => _earning;
  set earning(String earning) => _earning = earning;

  String get restaurent => _restaurent;

  set restaurent(String value) {
    _restaurent = value;
  }

  DashBoardDetails.fromJson(Map<String, dynamic> json) {
    _earning = json['earning'];
    _restaurent = json['restaurent'].toString();
    _food = json['food'].toString();
    _orders = json['orders'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['earning'] = this._earning;
    data['restaurent'] = this._restaurent;
    data['food'] = this._food;
    data['orders'] = this._orders;
    return data;
  }

  String get food => _food;

  set food(String value) {
    _food = value;
  }

  String get orders => _orders;

  set orders(String value) {
    _orders = value;
  }
}
