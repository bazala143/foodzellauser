class PayTabsResponse {
  String _result;
  String _responseCode;
  String _paymentUrl;
  int _pId;

  PayTabsResponse(
      {String result, String responseCode, String paymentUrl, int pId}) {
    this._result = result;
    this._responseCode = responseCode;
    this._paymentUrl = paymentUrl;
    this._pId = pId;
  }

  String get result => _result;
  set result(String result) => _result = result;
  String get responseCode => _responseCode;
  set responseCode(String responseCode) => _responseCode = responseCode;
  String get paymentUrl => _paymentUrl;
  set paymentUrl(String paymentUrl) => _paymentUrl = paymentUrl;
  int get pId => _pId;
  set pId(int pId) => _pId = pId;

  PayTabsResponse.fromJson(Map<String, dynamic> json) {
    _result = json['result'];
    _responseCode = json['response_code'];
    _paymentUrl = json['payment_url'];
    _pId = json['p_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this._result;
    data['response_code'] = this._responseCode;
    data['payment_url'] = this._paymentUrl;
    data['p_id'] = this._pId;
    return data;
  }
}