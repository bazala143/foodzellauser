class User {
  String id;
  String name;
  String email;
  String password;
  String apiToken;
  String deviceToken;
  String phone;
  String address;
  String bio;
  String loginType;
  String isActive;
  String role;
  String userImage;
  String loginRole;

  // used for indicate if client logged in or not
  bool auth;

//  String role;

  User();

  User.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'].toString();
    name = jsonMap['name'];
    email = jsonMap['email'];
    apiToken = jsonMap['api_token'];
    loginType = jsonMap['loginType'];
    deviceToken = jsonMap['device_token'];
    userImage = jsonMap['userImage'];
    role = jsonMap['role'].toString();
    isActive = jsonMap['isActive'].toString() ?? "0";
    phone = jsonMap['phone'] ?? "";
    address = jsonMap['address'] ?? "";
    bio = jsonMap['bio'] ?? "";
    loginRole = jsonMap['loginRole'] ?? "1";
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["password"] = password;
    map["loginType"] = loginType;
    map["api_token"] = apiToken;
    map["role"] = role;
    map["isActive"] = isActive ?? "0";
    if (deviceToken != null) {
      map["device_token"] = deviceToken;
    }
    map["phone"] = phone;
    map["address"] = address;
    map["bio"] = bio;
    map["userImage"] = userImage;
    map["loginRole"] = loginRole;
    return map;
  }

  @override
  String toString() {
    var map = this.toMap();
    map["auth"] = this.auth;
    return map.toString();
  }
}
