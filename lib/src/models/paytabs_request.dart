class PayTabsRequest {
  String _merchantEmail;
  String _secretKey;
  String _siteUrl;
  String _returnUrl;
  String _title;
  String _ccFirstName;
  String _ccLastName;
  String _ccPhoneNumber;
  String _phoneNumber;
  String _email;
  String _productsPerTitle;
  String _unitPrice;
  String _quantity;
  String _otherCharges;
  String _amount;
  String _discount;
  String _currency;
  String _referenceNo;
  String _ipCustomer;
  String _ipMerchant;
  String _billingAddress;
  String _state;
  String _city;
  String _postalCode;
  String _country;
  String _shippingFirstName;
  String _shippingLastName;
  String _addressShipping;
  String _cityShipping;
  String _stateShipping;
  String _postalCodeShipping;
  String _countryShipping;
  String _msgLang;
  String _cmsWithVersion;

  PayTabsRequest(
      {String merchantEmail,
        String secretKey,
        String siteUrl,
        String returnUrl,
        String title,
        String ccFirstName,
        String ccLastName,
        String ccPhoneNumber,
        String phoneNumber,
        String email,
        String productsPerTitle,
        String unitPrice,
        String quantity,
        String otherCharges,
        String amount,
        String discount,
        String currency,
        String referenceNo,
        String ipCustomer,
        String ipMerchant,
        String billingAddress,
        String state,
        String city,
        String postalCode,
        String country,
        String shippingFirstName,
        String shippingLastName,
        String addressShipping,
        String cityShipping,
        String stateShipping,
        String postalCodeShipping,
        String countryShipping,
        String msgLang,
        String cmsWithVersion}) {
    this._merchantEmail = merchantEmail;
    this._secretKey = secretKey;
    this._siteUrl = siteUrl;
    this._returnUrl = returnUrl;
    this._title = title;
    this._ccFirstName = ccFirstName;
    this._ccLastName = ccLastName;
    this._ccPhoneNumber = ccPhoneNumber;
    this._phoneNumber = phoneNumber;
    this._email = email;
    this._productsPerTitle = productsPerTitle;
    this._unitPrice = unitPrice;
    this._quantity = quantity;
    this._otherCharges = otherCharges;
    this._amount = amount;
    this._discount = discount;
    this._currency = currency;
    this._referenceNo = referenceNo;
    this._ipCustomer = ipCustomer;
    this._ipMerchant = ipMerchant;
    this._billingAddress = billingAddress;
    this._state = state;
    this._city = city;
    this._postalCode = postalCode;
    this._country = country;
    this._shippingFirstName = shippingFirstName;
    this._shippingLastName = shippingLastName;
    this._addressShipping = addressShipping;
    this._cityShipping = cityShipping;
    this._stateShipping = stateShipping;
    this._postalCodeShipping = postalCodeShipping;
    this._countryShipping = countryShipping;
    this._msgLang = msgLang;
    this._cmsWithVersion = cmsWithVersion;
  }

  String get merchantEmail => _merchantEmail;
  set merchantEmail(String merchantEmail) => _merchantEmail = merchantEmail;
  String get secretKey => _secretKey;
  set secretKey(String secretKey) => _secretKey = secretKey;
  String get siteUrl => _siteUrl;
  set siteUrl(String siteUrl) => _siteUrl = siteUrl;
  String get returnUrl => _returnUrl;
  set returnUrl(String returnUrl) => _returnUrl = returnUrl;
  String get title => _title;
  set title(String title) => _title = title;
  String get ccFirstName => _ccFirstName;
  set ccFirstName(String ccFirstName) => _ccFirstName = ccFirstName;
  String get ccLastName => _ccLastName;
  set ccLastName(String ccLastName) => _ccLastName = ccLastName;
  String get ccPhoneNumber => _ccPhoneNumber;
  set ccPhoneNumber(String ccPhoneNumber) => _ccPhoneNumber = ccPhoneNumber;
  String get phoneNumber => _phoneNumber;
  set phoneNumber(String phoneNumber) => _phoneNumber = phoneNumber;
  String get email => _email;
  set email(String email) => _email = email;
  String get productsPerTitle => _productsPerTitle;
  set productsPerTitle(String productsPerTitle) =>
      _productsPerTitle = productsPerTitle;
  String get unitPrice => _unitPrice;
  set unitPrice(String unitPrice) => _unitPrice = unitPrice;
  String get quantity => _quantity;
  set quantity(String quantity) => _quantity = quantity;



  String get currency => _currency;
  set currency(String currency) => _currency = currency;
  String get referenceNo => _referenceNo;
  set referenceNo(String referenceNo) => _referenceNo = referenceNo;
  String get ipCustomer => _ipCustomer;
  set ipCustomer(String ipCustomer) => _ipCustomer = ipCustomer;
  String get ipMerchant => _ipMerchant;
  set ipMerchant(String ipMerchant) => _ipMerchant = ipMerchant;
  String get billingAddress => _billingAddress;
  set billingAddress(String billingAddress) => _billingAddress = billingAddress;
  String get state => _state;
  set state(String state) => _state = state;
  String get city => _city;
  set city(String city) => _city = city;
  String get postalCode => _postalCode;
  set postalCode(String postalCode) => _postalCode = postalCode;
  String get country => _country;
  set country(String country) => _country = country;
  String get shippingFirstName => _shippingFirstName;
  set shippingFirstName(String shippingFirstName) =>
      _shippingFirstName = shippingFirstName;
  String get shippingLastName => _shippingLastName;
  set shippingLastName(String shippingLastName) =>
      _shippingLastName = shippingLastName;
  String get addressShipping => _addressShipping;
  set addressShipping(String addressShipping) =>
      _addressShipping = addressShipping;
  String get cityShipping => _cityShipping;
  set cityShipping(String cityShipping) => _cityShipping = cityShipping;
  String get stateShipping => _stateShipping;
  set stateShipping(String stateShipping) => _stateShipping = stateShipping;
  String get postalCodeShipping => _postalCodeShipping;
  set postalCodeShipping(String postalCodeShipping) =>
      _postalCodeShipping = postalCodeShipping;
  String get countryShipping => _countryShipping;
  set countryShipping(String countryShipping) =>
      _countryShipping = countryShipping;
  String get msgLang => _msgLang;
  set msgLang(String msgLang) => _msgLang = msgLang;
  String get cmsWithVersion => _cmsWithVersion;
  set cmsWithVersion(String cmsWithVersion) => _cmsWithVersion = cmsWithVersion;

  PayTabsRequest.fromJson(Map<String, dynamic> json) {
    _merchantEmail = json['merchant_email'];
    _secretKey = json['secret_key'];
    _siteUrl = json['site_url'];
    _returnUrl = json['return_url'];
    _title = json['title'];
    _ccFirstName = json['cc_first_name'];
    _ccLastName = json['cc_last_name'];
    _ccPhoneNumber = json['cc_phone_number'];
    _phoneNumber = json['phone_number'];
    _email = json['email'];
    _productsPerTitle = json['products_per_title'];
    _unitPrice = json['unit_price'];
    _quantity = json['quantity'];
    _otherCharges = json['other_charges'];
    _amount = json['amount'];
    _discount = json['discount'];
    _currency = json['currency'];
    _referenceNo = json['reference_no'];
    _ipCustomer = json['ip_customer'];
    _ipMerchant = json['ip_merchant'];
    _billingAddress = json['billing_address'];
    _state = json['state'];
    _city = json['city'];
    _postalCode = json['postal_code'];
    _country = json['country'];
    _shippingFirstName = json['shipping_first_name'];
    _shippingLastName = json['shipping_last_name'];
    _addressShipping = json['address_shipping'];
    _cityShipping = json['city_shipping'];
    _stateShipping = json['state_shipping'];
    _postalCodeShipping = json['postal_code_shipping'];
    _countryShipping = json['country_shipping'];
    _msgLang = json['msg_lang'];
    _cmsWithVersion = json['cms_with_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_email'] = this._merchantEmail;
    data['secret_key'] = this._secretKey;
    data['site_url'] = this._siteUrl;
    data['return_url'] = this._returnUrl;
    data['title'] = this._title;
    data['cc_first_name'] = this._ccFirstName;
    data['cc_last_name'] = this._ccLastName;
    data['cc_phone_number'] = this._ccPhoneNumber;
    data['phone_number'] = this._phoneNumber;
    data['email'] = this._email;
    data['products_per_title'] = this._productsPerTitle;
    data['unit_price'] = this._unitPrice;
    data['quantity'] = this._quantity;
    data['other_charges'] = this._otherCharges;
    data['amount'] = this._amount;
    data['discount'] = this._discount;
    data['currency'] = this._currency;
    data['reference_no'] = this._referenceNo;
    data['ip_customer'] = this._ipCustomer;
    data['ip_merchant'] = this._ipMerchant;
    data['billing_address'] = this._billingAddress;
    data['state'] = this._state;
    data['city'] = this._city;
    data['postal_code'] = this._postalCode;
    data['country'] = this._country;
    data['shipping_first_name'] = this._shippingFirstName;
    data['shipping_last_name'] = this._shippingLastName;
    data['address_shipping'] = this._addressShipping;
    data['city_shipping'] = this._cityShipping;
    data['state_shipping'] = this._stateShipping;
    data['postal_code_shipping'] = this._postalCodeShipping;
    data['country_shipping'] = this._countryShipping;
    data['msg_lang'] = this._msgLang;
    data['cms_with_version'] = this._cmsWithVersion;
    return data;
  }

  String get discount => _discount;

  set discount(String value) {
    _discount = value;
  }

  String get amount => _amount;

  set amount(String value) {
    _amount = value;
  }

  String get otherCharges => _otherCharges;

  set otherCharges(String value) {
    _otherCharges = value;
  }
}