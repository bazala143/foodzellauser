class FoodDetailMaster {
  bool _success;
  String _message;
  FoodDetail _result;

  FoodDetailMaster({bool success, String message, FoodDetail result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  FoodDetail get result => _result;
  set result(FoodDetail result) => _result = result;

  FoodDetailMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result =
    json['result'] != null ? new FoodDetail.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class FoodDetail {
  String _id;
  String _restaurantId;
  String _categoryId;
  String _name;
  String _price;
  String _discountPrice;
  String _description;
  String _ingredients;
  String _weight;
  String _category;
  String _restaurent;
  String _updatedAt;
  bool _featured;
  String _image;

  FoodDetail(
      {String id,
        String restaurantId,
        String categoryId,
        String name,
        String price,
        String discountPrice,
        String description,
        String ingredients,
        String weight,
        String category,
        String restaurent,
        String updatedAt,
        bool featured,
        String image}) {
    this._id = id;
    this._restaurantId = restaurantId;
    this._categoryId = categoryId;
    this._name = name;
    this._price = price;
    this._discountPrice = discountPrice;
    this._description = description;
    this._ingredients = ingredients;
    this._weight = weight;
    this._category = category;
    this._restaurent = restaurent;
    this._updatedAt = updatedAt;
    this._featured = featured;
    this._image = image;
  }


  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get name => _name;
  set name(String name) => _name = name;

  String get discountPrice => _discountPrice;
  set discountPrice(String discountPrice) => _discountPrice = discountPrice;
  String get description => _description;
  set description(String description) => _description = description;
  String get ingredients => _ingredients;
  set ingredients(String ingredients) => _ingredients = ingredients;

  String get category => _category;
  set category(String category) => _category = category;
  String get restaurent => _restaurent;
  set restaurent(String restaurent) => _restaurent = restaurent;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  bool get featured => _featured;
  set featured(bool featured) => _featured = featured;
  String get image => _image;
  set image(String image) => _image = image;

  FoodDetail.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _restaurantId = json['restaurantId'].toString();
    _categoryId = json['categoryId'].toString();
    _name = json['name'];
    _price = json['price'].toString();
    _discountPrice = json['discountPrice'].toString();
    _description = json['description'];
    _ingredients = json['ingredients'];
    _weight = json['weight'].toString();
    _category = json['category'];
    _restaurent = json['restaurent'];
    _updatedAt = json['updatedAt'];
    _featured = json['featured'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['restaurantId'] = this._restaurantId;
    data['categoryId'] = this._categoryId;
    data['name'] = this._name;
    data['price'] = this._price;
    data['discountPrice'] = this._discountPrice;
    data['description'] = this._description;
    data['ingredients'] = this._ingredients;
    data['weight'] = this._weight;
    data['category'] = this._category;
    data['restaurent'] = this._restaurent;
    data['updatedAt'] = this._updatedAt;
    data['featured'] = this._featured;
    data['image'] = this._image;
    return data;
  }

  String get restaurantId => _restaurantId;

  set restaurantId(String value) {
    _restaurantId = value;
  }

  String get categoryId => _categoryId;

  set categoryId(String value) {
    _categoryId = value;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  String get weight => _weight;

  set weight(String value) {
    _weight = value;
  }
}