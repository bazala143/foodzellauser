class OrderListMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<OrderDetails> _result;

  OrderListMaster(
      {bool success,
      String message,
      int totalCount,
      List<OrderDetails> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  int get totalCount => _totalCount;
  set totalCount(int totalCount) => _totalCount = totalCount;
  List<OrderDetails> get result => _result;
  set result(List<OrderDetails> result) => _result = result;

  OrderListMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<OrderDetails>();
      json['result'].forEach((v) {
        _result.add(new OrderDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderDetails {
  String _id;
  String _createdAt;
  String _orderStatus;
  String _statusId;
  String _tax;
  String _deliveryFee;
  String _price;
  String _paymentStatus;
  String _paymentMethod;
  List<FoodItems> _foods;
  bool _isExpanded;

  bool get isExpanded => _isExpanded ?? false;

  set isExpanded(bool value) {
    _isExpanded = value ?? false;
  }

  String get statusId => _statusId;

  set statusId(String value) {
    _statusId = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  OrderDetails(
      {String id,
      String createdAt,
      String orderStatus,
      String tax,
      String deliveryFee,
      String price,
      String paymentStatus,
      String paymentMethod,
      List<FoodItems> foods}) {
    this._id = id;
    this._createdAt = createdAt;
    this._orderStatus = orderStatus;
    this._tax = tax;
    this._deliveryFee = deliveryFee;
    this._price = price;
    this._paymentStatus = paymentStatus;
    this._paymentMethod = paymentMethod;
    this._foods = foods;
  }

  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get orderStatus => _orderStatus;
  set orderStatus(String orderStatus) => _orderStatus = orderStatus;
  String get paymentStatus => _paymentStatus;
  set paymentStatus(String paymentStatus) => _paymentStatus = paymentStatus;
  String get paymentMethod => _paymentMethod;
  set paymentMethod(String paymentMethod) => _paymentMethod = paymentMethod;
  List<FoodItems> get foods => _foods;
  set foods(List<FoodItems> foods) => _foods = foods;

  OrderDetails.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _createdAt = json['createdAt'];
    _orderStatus = json['orderStatus'];
    _statusId = json['statusId'].toString();
    _tax = json['tax'].toString();
    _deliveryFee = json['deliveryFee'].toString();
    _price = json['price'].toString();
    _paymentStatus = json['paymentStatus'];
    _paymentMethod = json['paymentMethod'];
    if (json['foods'] != null) {
      _foods = new List<FoodItems>();
      json['foods'].forEach((v) {
        _foods.add(new FoodItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['createdAt'] = this._createdAt;
    data['orderStatus'] = this._orderStatus;
    data['statusId'] = this._statusId;
    data['tax'] = this._tax;
    data['deliveryFee'] = this._deliveryFee;
    data['price'] = this._price;
    data['paymentStatus'] = this._paymentStatus;
    data['paymentMethod'] = this._paymentMethod;
    if (this._foods != null) {
      data['foods'] = this._foods.map((v) => v.toJson()).toList();
    }
    return data;
  }

  String get tax => _tax;

  set tax(String value) {
    _tax = value;
  }

  String get deliveryFee => _deliveryFee;

  set deliveryFee(String value) {
    _deliveryFee = value;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }
}

class FoodItems {
  String _price;
  String _quantity;
  String _foodName;
  String _image;
  String _restaurentName;

  FoodItems(
      {String price,
      String quantity,
      String foodName,
      String image,
      String restaurentName}) {
    this._price = price;
    this._quantity = quantity;
    this._foodName = foodName;
    this._image = image;
    this._restaurentName = restaurentName;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  String get foodName => _foodName;
  set foodName(String foodName) => _foodName = foodName;
  String get image => _image;
  set image(String image) => _image = image;
  String get restaurentName => _restaurentName;
  set restaurentName(String restaurentName) => _restaurentName = restaurentName;

  FoodItems.fromJson(Map<String, dynamic> json) {
    _price = json['price'].toString();
    _quantity = json['quantity'].toString();
    _foodName = json['foodName'];
    _image = json['image'];
    _restaurentName = json['restaurentName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this._price;
    data['quantity'] = this._quantity;
    data['foodName'] = this._foodName;
    data['image'] = this._image;
    data['restaurentName'] = this._restaurentName;
    return data;
  }

  String get quantity => _quantity;

  set quantity(String value) {
    _quantity = value;
  }
}
