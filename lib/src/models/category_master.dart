class CategoryMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<CategoryDetail> _result;

  CategoryMaster(
      {bool success,
      String message,
      int totalCount,
      List<CategoryDetail> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  int get totalCount => _totalCount;
  set totalCount(int totalCount) => _totalCount = totalCount;
  List<CategoryDetail> get result => _result;
  set result(List<CategoryDetail> result) => _result = result;

  CategoryMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<CategoryDetail>();
      json['result'].forEach((v) {
        _result.add(new CategoryDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CategoryDetail {
  String _id;
  String _name;
  String _image;

  CategoryDetail({String id, String name, String image}) {
    this._id = id;
    this._name = name;
    this._image = image;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get image => _image;
  set image(String image) => _image = image;

  CategoryDetail.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _name = json['name'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['image'] = this._image;
    return data;
  }
}
