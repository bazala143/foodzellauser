class AddFoodMaster {
  bool _success;
  String _message;
  //Result _result;

  AddFoodMaster({bool success, String message /*, Result result*/}) {
    this._success = success;
    this._message = message;
    //   this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
//  Result get result => _result;
  // set result(Result result) => _result = result;

  AddFoodMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    //  _result = json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    /*if (this._result != null) {
      data['result'] = this._result.toJson();
    }*/
    return data;
  }
}

/*class Result {
  String _name;
  int _price;
  int _discountPrice;
  String _description;
  String _ingredients;
  int _weight;
  bool _featured;
  int _restaurantId;
  int _categoryId;
  String _updatedAt;
  String _createdAt;
  int _id;
  List<Null> _customFields;
  bool _hasMedia;
  Restaurant _restaurant;
  List<Media> _media;

  Result(
      {String name,
      int price,
      int discountPrice,
      String description,
      String ingredients,
      int weight,
      bool featured,
      int restaurantId,
      int categoryId,
      String updatedAt,
      String createdAt,
      int id,
      List<Null> customFields,
      bool hasMedia,
      Restaurant restaurant,
      List<Media> media}) {
    this._name = name;
    this._price = price;
    this._discountPrice = discountPrice;
    this._description = description;
    this._ingredients = ingredients;
    this._weight = weight;
    this._featured = featured;
    this._restaurantId = restaurantId;
    this._categoryId = categoryId;
    this._updatedAt = updatedAt;
    this._createdAt = createdAt;
    this._id = id;
    this._customFields = customFields;
    this._hasMedia = hasMedia;
    this._restaurant = restaurant;
    this._media = media;
  }

  String get name => _name;
  set name(String name) => _name = name;
  int get price => _price;
  set price(int price) => _price = price;
  int get discountPrice => _discountPrice;
  set discountPrice(int discountPrice) => _discountPrice = discountPrice;
  String get description => _description;
  set description(String description) => _description = description;
  String get ingredients => _ingredients;
  set ingredients(String ingredients) => _ingredients = ingredients;
  int get weight => _weight;
  set weight(int weight) => _weight = weight;
  bool get featured => _featured;
  set featured(bool featured) => _featured = featured;
  int get restaurantId => _restaurantId;
  set restaurantId(int restaurantId) => _restaurantId = restaurantId;
  int get categoryId => _categoryId;
  set categoryId(int categoryId) => _categoryId = categoryId;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  int get id => _id;
  set id(int id) => _id = id;
  List<Null> get customFields => _customFields;
  set customFields(List<Null> customFields) => _customFields = customFields;
  bool get hasMedia => _hasMedia;
  set hasMedia(bool hasMedia) => _hasMedia = hasMedia;
  Restaurant get restaurant => _restaurant;
  set restaurant(Restaurant restaurant) => _restaurant = restaurant;
  List<Media> get media => _media;
  set media(List<Media> media) => _media = media;

  Result.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _price = json['price'];
    _discountPrice = json['discount_price'];
    _description = json['description'];
    _ingredients = json['ingredients'];
    _weight = json['weight'];
    _featured = json['featured'];
    _restaurantId = json['restaurant_id'];
    _categoryId = json['category_id'];
    _updatedAt = json['updated_at'];
    _createdAt = json['created_at'];
    _id = json['id'];
    */ /*if (json['custom_fields'] != null) {
      _customFields = new List<Null>();
      json['custom_fields'].forEach((v) {
        _customFields.add(new Null.fromJson(v));
      });
    }*/ /*
    _hasMedia = json['has_media'];
    _restaurant = json['restaurant'] != null
        ? new Restaurant.fromJson(json['restaurant'])
        : null;
    if (json['media'] != null) {
      _media = new List<Media>();
      json['media'].forEach((v) {
        _media.add(new Media.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['price'] = this._price;
    data['discount_price'] = this._discountPrice;
    data['description'] = this._description;
    data['ingredients'] = this._ingredients;
    data['weight'] = this._weight;
    data['featured'] = this._featured;
    data['restaurant_id'] = this._restaurantId;
    data['category_id'] = this._categoryId;
    data['updated_at'] = this._updatedAt;
    data['created_at'] = this._createdAt;
    data['id'] = this._id;
    */ /*if (this._customFields != null) {
      data['custom_fields'] =
          this._customFields.map((v) => v.toJson()).toList();
    }*/ /*
    data['has_media'] = this._hasMedia;
    if (this._restaurant != null) {
      data['restaurant'] = this._restaurant.toJson();
    }
    if (this._media != null) {
      data['media'] = this._media.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Restaurant {
  int _id;
  String _name;
  int _deliveryFee;
  List<Null> _customFields;
  bool _hasMedia;
  String _rate;
  List<Media> _media;

  Restaurant(
      {int id,
      String name,
      int deliveryFee,
      List<Null> customFields,
      bool hasMedia,
      String rate,
      List<Media> media}) {
    this._id = id;
    this._name = name;
    this._deliveryFee = deliveryFee;
    this._customFields = customFields;
    this._hasMedia = hasMedia;
    this._rate = rate;
    this._media = media;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  int get deliveryFee => _deliveryFee;
  set deliveryFee(int deliveryFee) => _deliveryFee = deliveryFee;
  List<Null> get customFields => _customFields;
  set customFields(List<Null> customFields) => _customFields = customFields;
  bool get hasMedia => _hasMedia;
  set hasMedia(bool hasMedia) => _hasMedia = hasMedia;
  String get rate => _rate;
  set rate(String rate) => _rate = rate;
  List<Media> get media => _media;
  set media(List<Media> media) => _media = media;

  Restaurant.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _deliveryFee = json['delivery_fee'];
    */ /* if (json['custom_fields'] != null) {
      _customFields = new List<Null>();
      json['custom_fields'].forEach((v) {
        _customFields.add(new Null.fromJson(v));
      });
    }*/ /*
    _hasMedia = json['has_media'];
    _rate = json['rate'];
    if (json['media'] != null) {
      _media = new List<Media>();
      json['media'].forEach((v) {
        _media.add(new Media.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['delivery_fee'] = this._deliveryFee;
    */ /* if (this._customFields != null) {
      data['custom_fields'] =
          this._customFields.map((v) => v.toJson()).toList();
    }*/ /*
    data['has_media'] = this._hasMedia;
    data['rate'] = this._rate;
    if (this._media != null) {
      data['media'] = this._media.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Media {
  int _id;
  String _modelType;
  int _modelId;
  String _collectionName;
  String _name;
  String _fileName;
  String _mimeType;
  String _disk;
  int _size;
  List<Null> _manipulations;
  CustomProperties _customProperties;
  List<Null> _responsiveImages;
  int _orderColumn;
  String _createdAt;
  String _updatedAt;
  String _url;
  String _thumb;
  String _icon;
  String _formatedSize;

  Media(
      {int id,
      String modelType,
      int modelId,
      String collectionName,
      String name,
      String fileName,
      String mimeType,
      String disk,
      int size,
      List<Null> manipulations,
      CustomProperties customProperties,
      List<Null> responsiveImages,
      int orderColumn,
      String createdAt,
      String updatedAt,
      String url,
      String thumb,
      String icon,
      String formatedSize}) {
    this._id = id;
    this._modelType = modelType;
    this._modelId = modelId;
    this._collectionName = collectionName;
    this._name = name;
    this._fileName = fileName;
    this._mimeType = mimeType;
    this._disk = disk;
    this._size = size;
    this._manipulations = manipulations;
    this._customProperties = customProperties;
    this._responsiveImages = responsiveImages;
    this._orderColumn = orderColumn;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._url = url;
    this._thumb = thumb;
    this._icon = icon;
    this._formatedSize = formatedSize;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get modelType => _modelType;
  set modelType(String modelType) => _modelType = modelType;
  int get modelId => _modelId;
  set modelId(int modelId) => _modelId = modelId;
  String get collectionName => _collectionName;
  set collectionName(String collectionName) => _collectionName = collectionName;
  String get name => _name;
  set name(String name) => _name = name;
  String get fileName => _fileName;
  set fileName(String fileName) => _fileName = fileName;
  String get mimeType => _mimeType;
  set mimeType(String mimeType) => _mimeType = mimeType;
  String get disk => _disk;
  set disk(String disk) => _disk = disk;
  int get size => _size;
  set size(int size) => _size = size;
  List<Null> get manipulations => _manipulations;
  set manipulations(List<Null> manipulations) => _manipulations = manipulations;
  CustomProperties get customProperties => _customProperties;
  set customProperties(CustomProperties customProperties) =>
      _customProperties = customProperties;
  List<Null> get responsiveImages => _responsiveImages;
  set responsiveImages(List<Null> responsiveImages) =>
      _responsiveImages = responsiveImages;
  int get orderColumn => _orderColumn;
  set orderColumn(int orderColumn) => _orderColumn = orderColumn;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get url => _url;
  set url(String url) => _url = url;
  String get thumb => _thumb;
  set thumb(String thumb) => _thumb = thumb;
  String get icon => _icon;
  set icon(String icon) => _icon = icon;
  String get formatedSize => _formatedSize;
  set formatedSize(String formatedSize) => _formatedSize = formatedSize;

  Media.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _modelType = json['model_type'];
    _modelId = json['model_id'];
    _collectionName = json['collection_name'];
    _name = json['name'];
    _fileName = json['file_name'];
    _mimeType = json['mime_type'];
    _disk = json['disk'];
    _size = json['size'];
    */ /* if (json['manipulations'] != null) {
      _manipulations = new List<Null>();
      json['manipulations'].forEach((v) {
        _manipulations.add(new Null.fromJson(v));
      });
    }*/ /*
    _customProperties = json['custom_properties'] != null
        ? new CustomProperties.fromJson(json['custom_properties'])
        : null;
    */ /* if (json['responsive_images'] != null) {
      _responsiveImages = new List<Null>();
      json['responsive_images'].forEach((v) {
        _responsiveImages.add(new Null.fromJson(v));
      });
    }*/ /*
    _orderColumn = json['order_column'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _url = json['url'];
    _thumb = json['thumb'];
    _icon = json['icon'];
    _formatedSize = json['formated_size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['model_type'] = this._modelType;
    data['model_id'] = this._modelId;
    data['collection_name'] = this._collectionName;
    data['name'] = this._name;
    data['file_name'] = this._fileName;
    data['mime_type'] = this._mimeType;
    data['disk'] = this._disk;
    data['size'] = this._size;
    */ /*  if (this._manipulations != null) {
      data['manipulations'] =
          this._manipulations.map((v) => v.toJson()).toList();
    }*/ /*
    if (this._customProperties != null) {
      data['custom_properties'] = this._customProperties.toJson();
    }
    */ /* if (this._responsiveImages != null) {
      data['responsive_images'] =
          this._responsiveImages.map((v) => v.toJson()).toList();
    }*/ /*
    data['order_column'] = this._orderColumn;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['url'] = this._url;
    data['thumb'] = this._thumb;
    data['icon'] = this._icon;
    data['formated_size'] = this._formatedSize;
    return data;
  }
}

class CustomProperties {
  String _uuid;
  GeneratedConversions _generatedConversions;

  CustomProperties({String uuid, GeneratedConversions generatedConversions}) {
    this._uuid = uuid;
    this._generatedConversions = generatedConversions;
  }

  String get uuid => _uuid;
  set uuid(String uuid) => _uuid = uuid;
  GeneratedConversions get generatedConversions => _generatedConversions;
  set generatedConversions(GeneratedConversions generatedConversions) =>
      _generatedConversions = generatedConversions;

  CustomProperties.fromJson(Map<String, dynamic> json) {
    _uuid = json['uuid'];
    _generatedConversions = json['generated_conversions'] != null
        ? new GeneratedConversions.fromJson(json['generated_conversions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this._uuid;
    if (this._generatedConversions != null) {
      data['generated_conversions'] = this._generatedConversions.toJson();
    }
    return data;
  }
}

class GeneratedConversions {
  bool _thumb;
  bool _icon;

  GeneratedConversions({bool thumb, bool icon}) {
    this._thumb = thumb;
    this._icon = icon;
  }

  bool get thumb => _thumb;
  set thumb(bool thumb) => _thumb = thumb;
  bool get icon => _icon;
  set icon(bool icon) => _icon = icon;

  GeneratedConversions.fromJson(Map<String, dynamic> json) {
    _thumb = json['thumb'];
    _icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumb'] = this._thumb;
    data['icon'] = this._icon;
    return data;
  }
}*/
