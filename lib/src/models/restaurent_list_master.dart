class RestaurentListMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<RestaurentDetails> _result;

  RestaurentListMaster(
      {bool success,
      String message,
      int totalCount,
      List<RestaurentDetails> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  int get totalCount => _totalCount;
  set totalCount(int totalCount) => _totalCount = totalCount;
  List<RestaurentDetails> get result => _result;
  set result(List<RestaurentDetails> result) => _result = result;

  RestaurentListMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<RestaurentDetails>();
      json['result'].forEach((v) {
        _result.add(new RestaurentDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RestaurentDetails {
  String _id;
  String _name;
  String _description;
  String _image;
  String _rating;

  RestaurentDetails(
      {String id,
      String name,
      String description,
      String image,
      String rating}) {
    this._id = id;
    this._name = name;
    this._description = description;
    this._image = image;
    this._rating = rating;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get image => _image;
  set image(String image) => _image = image;
  String get rating => _rating;
  set rating(String rating) => _rating = rating;

  RestaurentDetails.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _name = json['name'];
    _description = json['description'];
    _image = json['image'];
    _rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['description'] = this._description;
    data['image'] = this._image;
    data['rating'] = this._rating;
    return data;
  }
}
