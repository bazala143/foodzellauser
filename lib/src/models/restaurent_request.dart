class RestaurentRequest {
  String _id;
  String _name;
  String _userId;
  List<int> _deliveryBoy;
  String _deliveryFee;
  String _adminCommission;
  String _phone;
  String _mobile;
  String _address;
  String _latitude;
  String _longitude;
  String _description;
  String _information;

  RestaurentRequest(
      {String name,
      String userId,
      List<int> deliveryBoy,
      String deliveryFee,
      String adminCommission,
      String phone,
      String mobile,
      String address,
      String latitude,
      String longitude,
      String description,
      String information}) {
    this._name = name;
    this._userId = userId;
    this._deliveryBoy = deliveryBoy;
    this._deliveryFee = deliveryFee;
    this._adminCommission = adminCommission;
    this._phone = phone;
    this._mobile = mobile;
    this._address = address;
    this._latitude = latitude;
    this._longitude = longitude;
    this._description = description;
    this._information = information;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get userId => _userId;
  set userId(String userId) => _userId = userId;
  List<int> get deliveryBoy => _deliveryBoy;
  set deliveryBoy(List<int> deliveryBoy) => _deliveryBoy = deliveryBoy;
  String get deliveryFee => _deliveryFee;
  set deliveryFee(String deliveryFee) => _deliveryFee = deliveryFee;
  String get adminCommission => _adminCommission;
  set adminCommission(String adminCommission) =>
      _adminCommission = adminCommission;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;
  String get address => _address;
  set address(String address) => _address = address;
  String get latitude => _latitude;
  set latitude(String latitude) => _latitude = latitude;
  String get longitude => _longitude;
  set longitude(String longitude) => _longitude = longitude;
  String get description => _description;
  set description(String description) => _description = description;
  String get information => _information;
  set information(String information) => _information = information;

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  RestaurentRequest.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _userId = json['userId'];
    _deliveryBoy = json['deliveryBoy'].cast<int>();
    _deliveryFee = json['deliveryFee'];
    _adminCommission = json['adminCommission'];
    _phone = json['phone'];
    _mobile = json['mobile'];
    _address = json['address'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _description = json['description'];
    _information = json['information'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['userId'] = this._userId;
    data['deliveryBoy'] = this._deliveryBoy;
    data['deliveryFee'] = this._deliveryFee;
    data['adminCommission'] = this._adminCommission;
    data['phone'] = this._phone;
    data['mobile'] = this._mobile;
    data['address'] = this._address;
    data['latitude'] = this._latitude;
    data['longitude'] = this._longitude;
    data['description'] = this._description;
    data['information'] = this._information;
    return data;
  }
}
