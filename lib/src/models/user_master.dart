import 'dart:convert';

import 'package:app/src/models/user.dart';

class UserMaster {
  bool success;
  String message;
  User user;

  UserMaster({this.success, this.message, this.user});

  UserMaster.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    user = json['data'] != null ? new User.fromJSON(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.user != null) {
      data['data'] = jsonEncode(this.user.toMap());
    }
    return data;
  }
}
