class DeliveryBoysMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<DeliveryBoysDetails> _result;

  DeliveryBoysMaster(
      {bool success,
      String message,
      int totalCount,
      List<DeliveryBoysDetails> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;
  set success(bool success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  int get totalCount => _totalCount;
  set totalCount(int totalCount) => _totalCount = totalCount;
  List<DeliveryBoysDetails> get result => _result;
  set result(List<DeliveryBoysDetails> result) => _result = result;

  DeliveryBoysMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<DeliveryBoysDetails>();
      json['result'].forEach((v) {
        _result.add(new DeliveryBoysDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DeliveryBoysDetails {
  String _id;
  String _name;
  String _email;
  String _image;
  bool _isSelected;

  DeliveryBoysDetails({String name, String email, String image}) {
    this._name = name;
    this._email = email;
    this._image = image;
  }

  bool get isSelected => _isSelected ?? false;

  set isSelected(bool value) {
    _isSelected = value ?? false;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get image => _image;
  set image(String image) => _image = image;

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  DeliveryBoysDetails.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _email = json['email'];
    _image = json['image'];
    _id = json['id'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['email'] = this._email;
    data['image'] = this._image;
    data['id'] = this._id;
    return data;
  }
}
