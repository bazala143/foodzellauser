class FoodListMaster {
  bool _success;
  String _message;
  int _totalCount;
  List<FoodDetails> _result;

  FoodListMaster(
      {bool success,
      String message,
      int totalCount,
      List<FoodDetails> result}) {
    this._success = success;
    this._message = message;
    this._totalCount = totalCount;
    this._result = result;
  }

  bool get success => _success;

  set success(bool success) => _success = success;

  String get message => _message;

  set message(String message) => _message = message;

  int get totalCount => _totalCount;

  set totalCount(int totalCount) => _totalCount = totalCount;

  List<FoodDetails> get result => _result;

  set result(List<FoodDetails> result) => _result = result;

  FoodListMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<FoodDetails>();
      json['result'].forEach((v) {
        _result.add(new FoodDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FoodDetails {
  String _id;
  bool _featured;
  String _restaurantId;
  String _name;
  String _price;
  String _discountPrice;
  String _weight;
  String _category;
  String _restaurent;
  String _updatedAt;
  String _image;

  FoodDetails(
      {String id,
      bool featured,
      String restaurantId,
      String name,
      String price,
      String discountPrice,
      String weight,
      String category,
      String restaurent,
      String updatedAt,
      String image}) {
    this._id = id;
    this._featured = featured;
    this._restaurantId = restaurantId;
    this._name = name;
    this._price = price;
    this._discountPrice = discountPrice;
    this._weight = weight;
    this._category = category;
    this._restaurent = restaurent;
    this._updatedAt = updatedAt;
    this._image = image;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  bool get featured => _featured;

  set featured(bool featured) => _featured = featured;

  String get name => _name;

  set name(String name) => _name = name;

  String get category => _category;

  set category(String category) => _category = category;

  String get restaurent => _restaurent;

  set restaurent(String restaurent) => _restaurent = restaurent;

  String get updatedAt => _updatedAt;

  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  String get image => _image;

  set image(String image) => _image = image;

  FoodDetails.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _featured = json['featured'];
    _restaurantId = json['restaurantId'].toString();
    _name = json['name'];
    _price = json['price'].toString();
    _discountPrice = json['discountPrice'].toString();
    _weight = json['weight'].toString();
    _category = json['category'];
    _restaurent = json['restaurent'];
    _updatedAt = json['updatedAt'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['featured'] = this._featured;
    data['restaurantId'] = this._restaurantId;
    data['name'] = this._name;
    data['price'] = this._price;
    data['discountPrice'] = this._discountPrice;
    data['weight'] = this._weight;
    data['category'] = this._category;
    data['restaurent'] = this._restaurent;
    data['updatedAt'] = this._updatedAt;
    data['image'] = this._image;
    return data;
  }

  String get restaurantId => _restaurantId;

  set restaurantId(String value) {
    _restaurantId = value;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  String get discountPrice => _discountPrice;

  set discountPrice(String value) {
    _discountPrice = value;
  }

  String get weight => _weight;

  set weight(String value) {
    _weight = value;
  }
}
