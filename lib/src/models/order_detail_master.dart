import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/models/order_list_master.dart';

class OrderDetailMaster {
  bool _success;
  String _message;
  OrderDetailResult _result;

  OrderDetailMaster({bool success, String message, OrderDetailResult result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  bool get success => _success;

  set success(bool success) => _success = success;

  String get message => _message;

  set message(String message) => _message = message;

  OrderDetailResult get result => _result;

  set result(OrderDetailResult result) => _result = result;

  OrderDetailMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result = json['result'] != null
        ? new OrderDetailResult.fromJson(json['result'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class OrderDetailResult {
  String _id;
  String _createdAt;
  String _orderStatus;
  String _statusId;
  String _deliveryBoyId;
  String _tax;
  String _deliveryFee;
  String _price;
  String _paymentStatus;
  String _paymentMethod;
  List<FoodItems> _foods;
  String _subTotal;
  String _fullName;
  String _deliveryAddress;
  String _phone;
  List<DeliveryBoysDetails> _deliveryBoys;

  OrderDetailResult(
      {String id,
      String createdAt,
      String orderStatus,
      String tax,
      String deliveryFee,
      String price,
      String paymentStatus,
      String paymentMethod,
      List<FoodItems> foods,
      String subTotal,
      String fullName,
      String deliveryAddress,
      String phone,
      List<DeliveryBoysDetails> deliveryBoys}) {
    this._id = id;
    this._createdAt = createdAt;
    this._orderStatus = orderStatus;
    this._tax = tax;
    this._deliveryFee = deliveryFee;
    this._price = price;
    this._paymentStatus = paymentStatus;
    this._paymentMethod = paymentMethod;
    this._foods = foods;
    this._subTotal = subTotal;
    this._fullName = fullName;
    this._deliveryAddress = deliveryAddress;
    this._phone = phone;
    this._deliveryBoys = deliveryBoys;
  }

  String get statusId => _statusId;

  set statusId(String value) {
    _statusId = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get createdAt => _createdAt;

  set createdAt(String createdAt) => _createdAt = createdAt;

  String get orderStatus => _orderStatus;

  set orderStatus(String orderStatus) => _orderStatus = orderStatus;

  String get paymentStatus => _paymentStatus;

  set paymentStatus(String paymentStatus) => _paymentStatus = paymentStatus;

  String get paymentMethod => _paymentMethod;

  set paymentMethod(String paymentMethod) => _paymentMethod = paymentMethod;

  List<FoodItems> get foods => _foods;

  set foods(List<FoodItems> foods) => _foods = foods;

  String get fullName => _fullName;

  set fullName(String fullName) => _fullName = fullName;

  String get deliveryAddress => _deliveryAddress;

  set deliveryAddress(String deliveryAddress) =>
      _deliveryAddress = deliveryAddress;

  String get phone => _phone;

  set phone(String phone) => _phone = phone;

  List<DeliveryBoysDetails> get deliveryBoys => _deliveryBoys;

  set deliveryBoys(List<DeliveryBoysDetails> deliveryBoys) =>
      _deliveryBoys = deliveryBoys;

  OrderDetailResult.fromJson(Map<String, dynamic> json) {
    _id = json['id'].toString();
    _createdAt = json['createdAt'];
    _orderStatus = json['orderStatus'];
    _statusId = json['statusId'].toString();
    _deliveryBoyId = json['deliveryBoyid'].toString();
    _tax = json['tax'].toString();
    _deliveryFee = json['deliveryFee'].toString();
    _price = json['price'].toString();
    _paymentStatus = json['paymentStatus'];
    _paymentMethod = json['paymentMethod'];
    if (json['foods'] != null) {
      _foods = new List<FoodItems>();
      json['foods'].forEach((v) {
        _foods.add(new FoodItems.fromJson(v));
      });
    }
    _subTotal = json['subTotal'].toString();
    _fullName = json['fullName'];
    _deliveryAddress = json['deliveryAddress'];
    _phone = json['phone'];
    if (json['deliveryBoys'] != null) {
      _deliveryBoys = new List<DeliveryBoysDetails>();
      json['deliveryBoys'].forEach((v) {
        _deliveryBoys.add(new DeliveryBoysDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['createdAt'] = this._createdAt;
    data['orderStatus'] = this._orderStatus;
    data['tax'] = this._tax;
    data['deliveryFee'] = this._deliveryFee;
    data['price'] = this._price;
    data['paymentStatus'] = this._paymentStatus;
    data['paymentMethod'] = this._paymentMethod;
    if (this._foods != null) {
      data['foods'] = this._foods.map((v) => v.toJson()).toList();
    }
    data['subTotal'] = this._subTotal;
    data['fullName'] = this._fullName;
    data['deliveryAddress'] = this._deliveryAddress;
    data['phone'] = this._phone;
    if (this._deliveryBoys != null) {
      data['deliveryBoys'] = this._deliveryBoys.map((v) => v.toJson()).toList();
    }
    return data;
  }

  String get tax => _tax;

  set tax(String value) {
    _tax = value;
  }

  String get deliveryFee => _deliveryFee;

  set deliveryFee(String value) {
    _deliveryFee = value;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  String get subTotal => _subTotal;

  set subTotal(String value) {
    _subTotal = value;
  }

  String get deliveryBoyId => _deliveryBoyId;

  set deliveryBoyId(String value) {
    _deliveryBoyId = value;
  }
}
