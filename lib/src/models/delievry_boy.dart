class DeliveryBoy {
  bool _isSelected;

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    _isSelected = value;
  }
}
