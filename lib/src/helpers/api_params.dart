class ApiParams {
  static final String JSON_CONTENT = "json_content";
  static var NAME = "name";

  static var USER_ID = "user_id";
  static var OWNER_ID = "ownerId";
  static var START_LIMIT = "startLimit";

  static var ORDER_STATUS = "orderStatus";

  static var ID = "id";

  static var RESTAURENT_ID = "restaurentId";

  static var ORDER_ID = "orderId";
  static var STATUS_ID = "statusId";
  static var DELIVERY_BOY_ID = "deliveryBoyId";
}
