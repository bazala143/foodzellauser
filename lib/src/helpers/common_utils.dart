import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CommonUtils {
  static bool isAndroidPlatform() {
    if (Platform.isAndroid) {
      return true;
    } else {
      return false;
    }
  }

  static bool isValidEmail(String email) {
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    return emailValid;
  }

  static void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static Widget getBottomProgressBar(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      color: Colors.transparent,
      margin: EdgeInsets.only(top: 10.0),
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
        ),
      ),
    );
  }

  static Color getStatusColor(String statusId) {
    if (statusId == "1") {
      return Colors.blue;
    } else if (statusId == "2") {
      return Colors.lightBlueAccent;
    } else if (statusId == "3") {
      return Colors.amber;
    } else if (statusId == "4") {
      return Colors.blueAccent;
    } else if (statusId == "5") {
      return Colors.indigoAccent;
    } else if (statusId == "6") {
      return Colors.red;
    }
  }
}
