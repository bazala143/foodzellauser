class ApiUrl {
  static final String BASE_URL = "https://foodzela.com/api/V1/";
  static final String ADD_RESTAURENT = BASE_URL + "estaurants/store";
  static var GET_DELIVERY_BOYS = BASE_URL + "driver/list";

  static var GET_RESTAURENTS_LIST = BASE_URL + "restaurants/list";

  static var GET_ALL_CATEGORY = BASE_URL + "category/list";
  static var GET_ORDERS = BASE_URL + "order/list";
  static var GET_STATUSES = BASE_URL + "order/statusList";

  static String ADD_FOOD = BASE_URL + "foods/store";

  static var GET_RESTAURENT_DETAILS = BASE_URL + "restaurants/details";

  static String UPDATE_RESTAURENT = BASE_URL + "restaurants/update";

  static var DELETE_RESTAURENT = BASE_URL + "restaurants/delete";

  static var GET_FOOD_LIST = BASE_URL + "foods/list";

  static var DELETE_FOOD = BASE_URL + "foods/delete";
  static var UPDATE_FOOD = BASE_URL + "foods/update";

  static var GET_FOOD_DETAILS = BASE_URL + "foods/details";

  static var GET_ORDER_DETAILS = BASE_URL + "order/details";
  static var GET_DASHBOARD = BASE_URL + "dashboard";
  static var GET_ORDER_UPDATE = BASE_URL + "order/orderUpdate";

  static var PAYMENT_URL = "https://www.paytabs.com/apiv2/create_pay_page";
}
