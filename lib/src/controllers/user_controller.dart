import 'dart:convert';

import 'package:app/src/helpers/app_contsant.dart';
import 'package:app/src/models/google_data.dart';
import 'package:app/src/models/user_master.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../generated/i18n.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as repository;

class UserController extends ControllerMVC {
  User user = new User();
  bool hidePassword = true;

  GlobalKey<FormState> loginFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  FirebaseMessaging _firebaseMessaging;
  BuildContext mContext;
  bool isFromLogin = false;
  TextEditingController userNameController = new TextEditingController();
  TextEditingController userEmailController = new TextEditingController();
  static const platformChannel =
      const MethodChannel('com.foodzela.app.channel');
  bool isUserApiLoading = false;

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((String _deviceToken) {
      user.deviceToken = _deviceToken;
    });
  }

  void attchContext(BuildContext context) {
    this.mContext = context;
  }

  void login({bool isFromregister, bool isSocial}) async {
    isUserApiLoading = true;
    notifyListeners();
    user.loginRole = selectedUserTypeIndex.toString() == "1"
        ? AppConstant.USER_TYPE_OWNER
        : AppConstant.USER_TYPE_CUSTOMER;
    user.role = selectedUserTypeIndex.toString() == "1"
        ? AppConstant.USER_TYPE_OWNER
        : AppConstant.USER_TYPE_CUSTOMER;

    if (isSocial) {
      loginApi(user, isSocial);
    } else {
      loginFormKey.currentState.save();
      loginApi(user, false);
    }

    /*  if (isFromregister != null && isFromregister) {
    } else if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      loginApi(user, false);
    }*/
  }

  void register({bool isSocial}) async {
    isUserApiLoading = true;
    notifyListeners();
    user.loginRole = selectedUserTypeIndex.toString() == "1"
        ? AppConstant.USER_TYPE_OWNER
        : AppConstant.USER_TYPE_CUSTOMER;
    user.role = selectedUserTypeIndex.toString() == "1"
        ? AppConstant.USER_TYPE_OWNER
        : AppConstant.USER_TYPE_CUSTOMER;
    user.isActive = "0";

    /*register api*/
    if (isSocial != null && isSocial) {
      repository.register(user).then((userMaster) {
        isUserApiLoading = false;
        notifyListeners();
        if (userMaster.user != null && userMaster.user.apiToken != null) {
          handleResponse(userMaster, true);
        } else {
          login(isFromregister: true, isSocial: true);
        }
      });
    } else {
      loginFormKey.currentState.save();
      repository.register(user).then((userMaster) {
        if (userMaster.user != null && userMaster.user.apiToken != null) {
          handleResponse(userMaster, true);
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Email Already registered."),
          ));
        }
        isUserApiLoading = false;
        notifyListeners();
      });
    }
  }

  void resetPassword() {
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      repository.resetPassword(user).then((value) {
        if (value != null && value == true) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content:
                Text(S.current.your_reset_link_has_been_sent_to_your_email),
            action: SnackBarAction(
              label: S.current.login,
              onPressed: () {
                Navigator.of(scaffoldKey.currentContext)
                    .pushReplacementNamed('/Login');
              },
            ),
            duration: Duration(seconds: 10),
          ));
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.error_verify_email_settings),
          ));
        }
      });
    }
  }

  void loginWithGoogle() {
    repository.startGoogleLogin().then((googleSignInAccount) {
      if (googleSignInAccount != null) {
        isUserApiLoading = true;
        notifyListeners();
        user.email = googleSignInAccount.email;
        user.loginType = "Google";
        user.name = googleSignInAccount.displayName;
        user.password =
            user.password = googleSignInAccount.email.split("@")[0] + "@123";
        user.role = selectedUserTypeIndex == 0
            ? AppConstant.USER_TYPE_CUSTOMER
            : AppConstant.USER_TYPE_OWNER;
        user.loginRole = selectedUserTypeIndex == 0
            ? AppConstant.USER_TYPE_CUSTOMER
            : AppConstant.USER_TYPE_OWNER;
        /* repository.login(user).then((value) {
          //print(value.apiToken);
          if (value.user != null && value.user.apiToken != null) {
            scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(S.current.welcome + value.user.name),
            ));
            Navigator.of(scaffoldKey.currentContext)
                .pushReplacementNamed('/Pages', arguments: 2);
          } else {
            scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(S.current.emailIsNotRegistered),
              action: SnackBarAction(
                label: S.current.register,
                onPressed: () {
                  user.email = googleSignInAccount.email;
                  user.name = googleSignInAccount.displayName;
                  user.loginType = "Google";
                  user.password = user.password =
                      googleSignInAccount.email.split("@")[0] + "@123";
                  print("Register Api Params = > " + jsonEncode(user.toMap()));
                  register(isSocial: true);
                },
              ),
              duration: Duration(seconds: 10),
            ));
          }
          isUserApiLoading = false;
          notifyListeners();
        });*/
        loginApi(user, true);
      } else {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text("Unable to get email address"),
        ));
      }
    });
  }

  void registerWithGoogle() {
    repository.startGoogleLogin().then((googleSignInAccount) {
      if (googleSignInAccount != null) {
        user.email = googleSignInAccount.email;
        user.name = googleSignInAccount.displayName;
        user.password =
            user.password = googleSignInAccount.email.split("@")[0] + "@123";
        user.loginType = "Google";
        user.role = selectedUserTypeIndex == 0
            ? AppConstant.USER_TYPE_CUSTOMER
            : AppConstant.USER_TYPE_OWNER;
        user.loginRole = selectedUserTypeIndex == 0
            ? AppConstant.USER_TYPE_CUSTOMER
            : AppConstant.USER_TYPE_OWNER;
        register(isSocial: true);
        notifyListeners();
      } else {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text("Unable to get email address"),
        ));
      }
    });
  }

  void iosGoogleLogin(bool isFromLogin) {
    this.isFromLogin = isFromLogin;
    platformChannel.invokeMethod("GoogleLogin");
  }

  void iosAppleLogin(bool isFromLogin) {
    this.isFromLogin = isFromLogin;
    platformChannel.invokeMethod("AppleLogin");
  }

  void initPlatFormChannel() {
    platformChannel.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "GoogleLogin":
        _redirectToNotificationDetails(call.arguments, true);
        return new Future.value("");
      case "AppleLogin":
        _redirectToNotificationDetails(call.arguments, false);
        return new Future.value("");
    }
  }

  Future<void> _redirectToNotificationDetails(
      String data, bool isGoogleLogin) async {
    Map<String, dynamic> dataMap = json.decode(data);
    GoogleData googleData = GoogleData.fromJson(dataMap);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("IsGoogleLogin " + isGoogleLogin.toString());
    if (!isGoogleLogin) {
      if (googleData.email != null) {
        prefs.setString("appleData", jsonEncode(googleData));
      } else {
        String appleData = prefs.getString("appleData");
        googleData = GoogleData.fromJson(jsonDecode(appleData));
      }
      print("AppleData ===> " + jsonEncode(googleData));
    }
    if (googleData != null) {
      if (isFromLogin) {
        loginWithGoogleIOS(googleData);
      } else {
        // register api call for apple
        user.email = googleData.email;
        user.name = googleData.name;
        user.password = googleData.email.split("@")[0] + "@123";
        user.loginType = "Google";
        print("Register Api Params ===> " + jsonEncode(user.toMap()));
        register(isSocial: true);
      }
    }
  }

  void loginWithGoogleIOS(GoogleData googleData) {
    if (googleData != null) {
      user.email = googleData.email;
      user.loginType = "Google";
      user.name = googleData.name;
      user.password = user.password = googleData.email.split("@")[0] + "@123";
      user.role = selectedUserTypeIndex == 0
          ? AppConstant.USER_TYPE_CUSTOMER
          : AppConstant.USER_TYPE_OWNER;
      user.loginRole = selectedUserTypeIndex == 0
          ? AppConstant.USER_TYPE_CUSTOMER
          : AppConstant.USER_TYPE_OWNER;
      isUserApiLoading = true;
      notifyListeners();
      loginApi(user, true);
    } else {
      scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Uable to get email address"),
        action: SnackBarAction(
          label: S.current.register,
          onPressed: () {
            register(isSocial: true);
          },
        ),
        duration: Duration(seconds: 10),
      ));
    }

    /*repository.login(user).then((userMaster) {
      if (userMaster.success) {
        if (userMaster.user != null && userMaster.user.apiToken != null) {
          if (userMaster.user.loginRole == AppConstant.USER_TYPE_OWNER &&
              userMaster.user.isActive == "1" &&
              userMaster.user.role == AppConstant.USER_TYPE_OWNER) {
            Navigator.of(scaffoldKey.currentContext)
                .pushReplacementNamed('/OwnerDashboard', arguments: 2);
          } else if (userMaster.user.loginRole ==
              AppConstant.USER_TYPE_CUSTOMER) {
            Navigator.of(scaffoldKey.currentContext)
                .pushReplacementNamed('/Pages', arguments: 2);
          }
        }
      } else {
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(S.current.emailIsNotRegistered),
          action: SnackBarAction(
            label: S.current.register,
            onPressed: () {
              register(isSocial: true);
            },
          ),
          duration: Duration(seconds: 10),
        ));
      }
      isUserApiLoading = false;
      notifyListeners();
    });*/
  }

  List<String> userTypeList = new List();
  int selectedUserTypeIndex = 0;

  void buildList({BuildContext context}) {
    userTypeList.clear();
    userTypeList.add("Customer");
    userTypeList.add("Restaurent owner");
    notifyListeners();
  }

  void changeSelectedIndex(int index) {
    selectedUserTypeIndex = index;
    notifyListeners();
  }

  void loginApi(User user, bool isSocial) {
    repository.login(user).then((userMaster) {
      if (userMaster.success) {
        handleResponse(userMaster, false);
      } else {
        if (isSocial) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.emailIsNotRegistered),
            action: SnackBarAction(
              label: S.current.register,
              onPressed: () {
                register(isSocial: true);
              },
            ),
            duration: Duration(seconds: 10),
          ));
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(userMaster.message),
          ));
        }
      }

      isUserApiLoading = false;
      notifyListeners();
    });
  }

  void handleResponse(UserMaster userMaster, bool isRegister) {
    if (userMaster.user != null && userMaster.user.apiToken != null) {
      if (userMaster.user.loginRole == AppConstant.USER_TYPE_OWNER &&
          userMaster.user.isActive == "1" &&
          userMaster.user.role == AppConstant.USER_TYPE_OWNER) {
        Navigator.of(scaffoldKey.currentContext)
            .pushReplacementNamed('/OwnerDashboard', arguments: 2);
      } else if (userMaster.user.loginRole == AppConstant.USER_TYPE_CUSTOMER) {
        Navigator.of(scaffoldKey.currentContext)
            .pushReplacementNamed('/Pages', arguments: 2);
      } else {
        if (!isRegister) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
              "Currently you are not approved as Restaurant Manager.Please contact support team to get approval",
              textAlign: TextAlign.start,
            ),
          ));
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
              "You are registered successfullly. Please wait for admin approval",
              textAlign: TextAlign.start,
            ),
          ));
        }
      }
    }
  }
}
