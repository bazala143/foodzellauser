import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:app/src/models/google_address.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart' as pred;
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../repository/settings_repository.dart' as sett;

class SelectLocationController extends ControllerMVC {
  List<Marker> allMarkers = <Marker>[];
  LocationData currentLocation;
  CameraPosition cameraPosition;
  Completer<GoogleMapController> mapController = Completer();
  bool isAddressFetching = false;

  String fetchedAddress = "Full address";
  String addressTitle = "Landmark";
  pred.GoogleMapsPlaces _places =
      pred.GoogleMapsPlaces(apiKey: sett.setting.value.googleMapsKey);
  LatLng selectedLatLng;

  Future<void> setDefaultLocation() async {
    setState(() {
      cameraPosition = CameraPosition(
        target: LatLng(0.0, 0.0),
        zoom: 14.4746,
      );
    });
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/img/my_marker.png', 120);
    final Marker marker = Marker(
        markerId: MarkerId("1001"),
        icon: BitmapDescriptor.fromBytes(markerIcon),
        anchor: Offset(0.5, 0.5),
        position: LatLng(0.0, 0.0));
    allMarkers.add(marker);
  }

  void getCurrentLocation() async {
    try {
      currentLocation = await sett.getCurrentLocation("Location Controller");
      if (currentLocation != null) {
        setState(() {
          cameraPosition = CameraPosition(
            target: LatLng(currentLocation.latitude, currentLocation.longitude),
            zoom: 14.4746,
          );
        });
        final Uint8List markerIcon =
            await getBytesFromAsset('assets/img/my_marker.png', 120);
        final Marker marker = Marker(
            markerId: MarkerId("1001"),
            icon: BitmapDescriptor.fromBytes(markerIcon),
            anchor: Offset(0.5, 0.5),
            position:
                LatLng(currentLocation.latitude, currentLocation.longitude));
        allMarkers.add(marker);
      } else {
        setState(() {
          cameraPosition = CameraPosition(
            target: LatLng(0.0, 0.0),
            zoom: 14.4746,
          );
        });
        final Uint8List markerIcon =
            await getBytesFromAsset('assets/img/my_marker.png', 120);
        final Marker marker = Marker(
            markerId: MarkerId("1001"),
            icon: BitmapDescriptor.fromBytes(markerIcon),
            anchor: Offset(0.5, 0.5),
            position: LatLng(0.0, 0.0));
        allMarkers.add(marker);
      }
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
        cameraPosition = CameraPosition(
          target: LatLng(0.0, 0.0),
          zoom: 14.4746,
        );
      }
    }
  }

  Future<void> goCurrentLocation() async {
    if (currentLocation != null) {
      final GoogleMapController controller = await mapController.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 14.4746,
      )));
    } else {
      getCurrentLocation();
    }

    notifyListeners();
  }

  Future<void> getAddressFromLatlong(
      {BuildContext context, double lat, double long}) async {
    isAddressFetching = true;
    notifyListeners();
    final response = await http.get(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
            lat.toString() +
            "," +
            long.toString() +
            "+&key=" +
            sett.setting.value.googleMapsKey);
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      print("Response: " + response.body);
      GoogleAdress master = GoogleAdress.fromJson(json.decode(response.body));
      if (master != null) {
        selectedLatLng = LatLng(lat, long);
        addMarker();
        fetchedAddress = master.results[0].formattedAddress;
        addressTitle = master.results[0].formattedAddress;
        isAddressFetching = false;
        notifyListeners();
        return master.results[0].formattedAddress;
      }
    }
  }

  /*Search autocomplete*/
  Future OpenPlaceAutoComplete(
      {BuildContext mContext, Function onSuccess}) async {
    pred.Prediction p = await PlacesAutocomplete.show(
      context: mContext,
      apiKey: sett.setting.value.googleMapsKey,
      mode: Mode.fullscreen,
      language: "en",
      //components: [new Component(Component.country, "IRQ")]
    );
    if (p != null) {
      pred.PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      print(lat.toString() + "//" + lng.toString());
      fetchedAddress = detail.result.formattedAddress;
      addressTitle = detail.result.name;
      selectedLatLng = new LatLng(lat, lng);
      updateCamera(selectedLatLng);
      onSuccess(selectedLatLng);
      notifyListeners();
    }
  }

  Future<void> addMarker() async {
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/img/my_marker.png', 120);
    final Marker marker = Marker(
        markerId: MarkerId("1001"),
        icon: BitmapDescriptor.fromBytes(markerIcon),
        anchor: Offset(0.5, 0.5),
        position: LatLng(selectedLatLng.latitude, selectedLatLng.longitude));
    allMarkers.add(marker);
    notifyListeners();
  }

  Future<void> updateCamera(LatLng latLng) async {
    final GoogleMapController controller = await mapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(latLng.latitude, latLng.longitude),
      zoom: 14.4746,
    )));
    addMarker();
    notifyListeners();
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<void> storeLocation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setDouble('currentLat', selectedLatLng.latitude);
    await prefs.setDouble('currentLon', selectedLatLng.longitude);
  }
}
