import 'dart:convert';

import 'package:app/src/helpers/app_contsant.dart';
import 'package:app/src/models/user.dart';
import 'package:app/src/pages/select_location_page.dart';
import 'package:app/src/repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../controllers/splash_screen_controller.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends StateMVC<SplashScreen> {
  SplashScreenController _con;
  BuildContext mContext;

  SplashScreenState() : super(SplashScreenController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      loadData();
    });
  }

  void loadData() {
    var f = null;
    f = () {
      double progress = 0;
      _con.progress.value.values.forEach((_progress) {
        progress += _progress;
      });
      if (progress == 100) {
        if (f != null) {
          _con.progress.removeListener(f);
        }

        _con.getLocationData().then((value) {
          if (value == null) {
            Navigator.push(mContext, MaterialPageRoute(builder: (context) {
              return SelectLocationWidget();
            }));
          } else {
            getCurrentUser().then((User user) {
              print("UserJson" + jsonEncode(user.toMap()));
              if (user != null) {
                if (user.role == AppConstant.USER_TYPE_OWNER &&
                    user.isActive == "1") {
                  Navigator.of(mContext)
                      .pushReplacementNamed('/OwnerDashboard', arguments: 2);
                } else {
                  Navigator.of(mContext)
                      .pushReplacementNamed('/Pages', arguments: 2);
                }
              } else {
                Navigator.of(mContext)
                    .pushReplacementNamed('/Pages', arguments: 2);
              }
            });
          }
        });
      }
    };

    _con.progress.addListener(f);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      body: Builder(builder: (_context) {
        mContext = _context;
        return Container(
          decoration: BoxDecoration(
            color: Theme.of(context).scaffoldBackgroundColor,
          ),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/img/ic_launcher.png',
                  width: 150,
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 50),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Theme.of(context).hintColor),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
