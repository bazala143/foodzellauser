import 'package:app/generated/i18n.dart';
import 'package:app/src/controllers/select_location_controller.dart';
import 'package:app/src/elements/CircularLoadingWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

import '../helpers/app_config.dart' as config;

class SelectLocationWidget extends StatefulWidget {
  final String from;

  SelectLocationWidget({this.from});

  @override
  _SelectLocationWidgetState createState() => _SelectLocationWidgetState();
}

class _SelectLocationWidgetState extends StateMVC<SelectLocationWidget> {
  SelectLocationController _con;
  LatLng selectedLocation;
  String _mapStyle;

  _SelectLocationWidgetState() : super(SelectLocationController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    //  _con.getCurrentLocation();
    _con.setDefaultLocation();
    rootBundle.loadString('assets/cfg/map_style.txt').then((string) {
      setState(() {
        _mapStyle = string;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final serachbar = InkWell(
      onTap: () {
        _con.OpenPlaceAutoComplete(
            mContext: context,
            onSuccess: (LatLng latLng) {
              print("Lattitude" + latLng.latitude.toString());
              selectedLocation = latLng;
            });
      },
      child: Container(
        margin: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
        padding: EdgeInsets.all(12.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
            boxShadow: [
              new BoxShadow(
                color: Colors.grey,
                blurRadius: 30.0,
              ),
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              S.of(context).searchAddress,
              style: TextStyle(color: Colors.grey),
            ),
            Icon(Icons.search)
          ],
        ),
      ),
    );

    final myLocationButton = Align(
      alignment: Alignment.bottomRight,
      child: InkWell(
        onTap: () {
          _con.goCurrentLocation();
        },
        child: Container(
          margin:
              EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0, bottom: 20.0),
          height: 50.0,
          width: 50.0,
          alignment: Alignment.center,
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: Colors.white),
          child: Icon(
            Icons.location_searching,
            color: Colors.grey,
            size: 20.0,
          ),
        ),
      ),
    );

    final googleMap = _con.cameraPosition == null
        ? Container(
            height: 100,
            width: MediaQuery.of(context).size.width,
          )
        : Container(
            height: MediaQuery.of(context).size.height * 0.90,
            child: GoogleMap(
              mapType: MapType.normal,
              zoomControlsEnabled: false,
              onTap: (LatLng latLng) {
                selectedLocation = latLng;
                _con.getAddressFromLatlong(
                    context: context,
                    lat: latLng.latitude,
                    long: latLng.longitude);
              },
              initialCameraPosition: _con.cameraPosition,
              markers: Set.from(_con.allMarkers),
              onMapCreated: (GoogleMapController controller) {
                _con.mapController.complete(controller);
                controller.setMapStyle(_mapStyle);
              },
              onCameraMove: (CameraPosition cameraPosition) {
                /* _con.cameraPosition = cameraPosition;
                _con.getAddressFromLatlong(
                    context: context,
                    lat: cameraPosition.target.latitude,
                    long: cameraPosition.target.longitude);*/
              },
            ),
          );

    final centerMarker = Align(
      alignment: Alignment.center,
      child: Image.asset(
        "assets/img/ic_current_marker.png",
        height: 40,
        width: 40,
      ),
    );

    final addressTitle = Text(
      _con.addressTitle,
      maxLines: 1,
      style: TextStyle(
          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
    );

    final shimmerTitle = Shimmer.fromColors(
      baseColor: Colors.grey,
      highlightColor: Colors.white,
      child: Text(
        _con.addressTitle,
        maxLines: 1,
        style: TextStyle(
            color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    final fullAddress = Text(
      _con.fetchedAddress,
      maxLines: 2,
      style: TextStyle(
          color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 14),
    );

    final shimmerFull = Shimmer.fromColors(
      baseColor: Colors.grey,
      highlightColor: Colors.white,
      child: Text(
        _con.fetchedAddress,
        maxLines: 2,
        style: TextStyle(
            color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 14),
      ),
    );

    final btnsetLocation = InkWell(
      onTap: () {
        if (widget.from != null && widget.from == "AddRestaurentView") {
          Navigator.pop(context, [_con.selectedLatLng, _con.fetchedAddress]);
        } else {
          _con.storeLocation().then((value) {
            Navigator.of(context).pushReplacementNamed('/Pages', arguments: 2);
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        padding: EdgeInsets.all(12.0),
        decoration: BoxDecoration(
            color: config.Colors().mainColor(1),
            borderRadius: BorderRadius.circular(30.0),
            boxShadow: [
              new BoxShadow(
                color: Colors.grey,
                blurRadius: 10.0,
              ),
            ]),
        child: Center(
          child: Text(
            S.of(context).confirmLocation,
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.normal,
                fontSize: 16),
          ),
        ),
      ),
    );

    final bottomColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _con.isAddressFetching ? shimmerTitle : addressTitle,
        _con.isAddressFetching ? shimmerFull : fullAddress,
        btnsetLocation
      ],
    );

    return Scaffold(
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10.0),
        width: MediaQuery.of(context).size.width,
        height: 160,
        child: bottomColumn,
      ),
      body: Stack(
        alignment: AlignmentDirectional.bottomStart,
        children: <Widget>[
          _con.cameraPosition == null
              ? CircularLoadingWidget(height: 0)
              : Stack(
                  children: <Widget>[googleMap, serachbar, myLocationButton],
                ),
        ],
      ),
    );
  }
}
