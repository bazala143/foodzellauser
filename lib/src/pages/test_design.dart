import 'dart:async';

import 'package:app/generated/i18n.dart';
import 'package:flutter/material.dart';

class TestDesign extends StatefulWidget {
  @override
  _TestDesignState createState() => _TestDesignState();
}

class _TestDesignState extends State<TestDesign>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  AnimationController animationController, animLoginButtonController;
  Animation<Offset> animationOffset, animLoginButtionOffset;
  bool isVisible = false;

  final _formKey = GlobalKey<FormState>();

  @override
  bool get wantKeepAlive => false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    animationOffset = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero)
        .animate(animationController);

    animLoginButtonController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    animLoginButtionOffset =
        Tween<Offset>(begin: Offset(0.0, 5.0), end: Offset.zero)
            .animate(animLoginButtonController);

    setState(() {
      isVisible = true;
    });
    Timer(Duration(milliseconds: 100), () {
      animationController.forward();
      animLoginButtonController.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    final btnLogin = InkWell(
      onTap: () {},
      child: animLoginButtionOffset != null && isVisible
          ? SlideTransition(
              position: animLoginButtionOffset,
              child: Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blueAccent,
                ),
                child: Center(
                  child: Icon(Icons.check_circle),
                ),
              ),
            )
          : Container(),
    );

    final bottomRow = Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.only(top: 40, bottom: 80),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          btnLogin,
        ],
      ),
    );

    final middleColumn = animationOffset != null
        ? SlideTransition(
            position: animationOffset,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(bottom: 12),
                  height: 94,
                  child: Image.asset("assets/img/logo.png", fit: BoxFit.fill),
                  //child: Image.asset(LocalImages.logo),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    S.of(context).login,
                    textAlign: TextAlign.center,
                  ),
                ),
                getInputWidget(TextEditingController(), S.of(context).full_name,
                    S.of(context).email, TextInputType.emailAddress, true),
                getInputWidget(TextEditingController(), S.of(context).password,
                    S.of(context).password, TextInputType.text, true),
                InkWell(
                  onTap: () {
                    //Navigator.push(context, SlideRightRoute(page: SignUpView()));
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                    padding:
                        EdgeInsets.only(left: 30, right: 30, top: 2, bottom: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Icon(
                            Icons.help_outline,
                            size: 15,
                          ),
                        ),
                        Container(
                          width: 5,
                        ),
                        Text(
                          S.of(context).password,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
                btnLogin,
                SizedBox(
                  height: MediaQuery.of(context).viewInsets.bottom,
                )
              ],
            ))
        : Container();

    final appBarView = Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.only(
          left: 8,
          right: 16,
          top: 35,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 40,
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 15,
                  color: Colors.white,
                ),
              ),
            ),
            Text(""),
          ],
        ),
      ),
    );

    final mBody = Container(
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              middleColumn,
              //bottomRow,
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.deepOrange,
      resizeToAvoidBottomPadding: true,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Align(alignment: Alignment.center, child: mBody),
              appBarView,
            ],
          ),
        ),
      ),
    );
  }

  Widget getInputWidget(TextEditingController controller, String hint,
      String validationText, TextInputType textInputType, bool isValid) {
    final crossIcon = isValid
        ? Container()
        : Flexible(
            child: Visibility(
            child: Container(
              margin: EdgeInsets.only(left: 5.0, right: 5.0),
              child: Icon(
                Icons.cancel,
                color: Colors.red,
              ),
            ),
            visible: !isValid,
          ));

    double margin = isValid ? 30 : 0.0;

    return Container(
      margin: EdgeInsets.only(left: margin, right: 30, top: 15),
      width: MediaQuery.of(context).size.width,
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          crossIcon,
          Flexible(
              flex: 10,
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: isValid ? Colors.white : Colors.red, width: 1),
                    // boxShadow: [
                    //   new BoxShadow(
                    //     color:
                    //         CommonColors.primaryShadowColor.withOpacity(0.16),
                    //     blurRadius: 6,
                    //   ),
                    // ],
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(25))),
                padding:
                    EdgeInsets.only(left: 30, right: 30, top: 2, bottom: 2),
                child: TextFormField(
                  controller: controller,
                  obscureText: false,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      counterText: "",
                      hintText: hint,
                      contentPadding: EdgeInsets.only(bottom: 12),
                      border: InputBorder.none,
                      isDense: false),
                  keyboardType: textInputType,
                  maxLines: 1,
                  autocorrect: false,
                  textAlign: TextAlign.left,
                ),
              ))
        ],
      ),
    );
  }
}
