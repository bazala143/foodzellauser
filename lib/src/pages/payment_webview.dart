import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/payment.dart';
import 'package:app/src/models/route_argument.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class PaymentWebView extends StatefulWidget {

   String payUrl;
   PaymentWebView({this.payUrl});

   @override
  _PaymentWebViewState createState() => _PaymentWebViewState();
}

class _PaymentWebViewState extends State<PaymentWebView> {
  InAppWebViewController webView;
  String url = "";
  double progress = 0;
  String total = "0.0";


  @override
  void initState() {
    super.initState();
   url = widget.payUrl;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text('Make Payment'),
      ),
      body: Container(
          child: Column(children: <Widget>[
        Container(
            child: progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container()),
        Expanded(
          child: Container(
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: InAppWebView(
              initialUrl: "",
              initialHeaders: {},
              initialOptions: InAppWebViewGroupOptions(
                  crossPlatform: InAppWebViewOptions(
                debuggingEnabled: true,
              )),
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
                controller.loadUrl(
                    url: url);
              },
              onLoadStart: (InAppWebViewController controller, String url) {
                setState(() {
                  this.url = url;
                });
                if (url.endsWith("success.php")) {
                  Payment payment = new Payment("debit card");
                  payment.status = "Paid";
                  Navigator.of(context).pushNamed("/OrderSuccess",
                      arguments: RouteArgument(param: payment));
                } else if (url.contains("status=failed")) {
                  CommonUtils.showToast("Payment Failed");
                }
              },
              onLoadStop:
                  (InAppWebViewController controller, String url) async {
                setState(() {
                  this.url = url;
                });
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ),
      ])),
    );
  }
}
