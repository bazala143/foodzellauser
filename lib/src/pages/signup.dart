import 'package:app/generated/i18n.dart';
import 'package:app/src/elements/GoogleButton.dart';
import 'package:app/src/elements/apple_signin_button.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../controllers/user_controller.dart';
import '../elements/BlockButtonWidget.dart';
import '../helpers/app_config.dart' as config;

class SignUpWidget extends StatefulWidget {
  final User user;

  SignUpWidget({this.user});

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends StateMVC<SignUpWidget> {
  UserController _con;
  BuildContext mContext;

  _SignUpWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.user != null) {
      _con.userNameController.text = widget.user.name;
      _con.userEmailController.text = widget.user.email;
    }
    Future.delayed(Duration.zero, () {
      _con.buildList(context: context);
    });
    _con.initPlatFormChannel();
  }

  @override
  Widget build(BuildContext context) {
    print("height" + (config.App(context).appHeight(100) + 30).toString());
    final progressBar = Center(
      child: CircularProgressIndicator(),
    );
    final btnLogin = BlockButtonWidget(
      text: Text(
        S.of(context).register,
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      color: Theme.of(context).accentColor,
      onPressed: () {
        if (_con.loginFormKey.currentState.validate()) {
          _con.register(isSocial: false);
        }
      },
    );

    final whiteCardView = Positioned(
      top: config.App(context).appHeight(20),
      child: Container(
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                blurRadius: 50,
                color: Theme.of(context).hintColor.withOpacity(0.2),
              )
            ]),
        margin: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 27),
        width: config.App(context).appWidth(88),
//              height: config.App(context).appHeight(55),
        child: Form(
          key: _con.loginFormKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                enabled: _con.isUserApiLoading ? false : true,
                keyboardType: TextInputType.text,
                controller: _con.userNameController,
                onSaved: (input) => _con.user.name = input,
                validator: (input) => input.length < 3
                    ? S.of(context).should_be_more_than_3_letters
                    : null,
                decoration: InputDecoration(
                  labelText: S.of(context).full_name,
                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                  contentPadding: EdgeInsets.all(12),
                  hintText: S.of(context).john_doe,
                  hintStyle: TextStyle(
                      color: Theme.of(context).focusColor.withOpacity(0.7)),
                  prefixIcon: Icon(Icons.person_outline,
                      color: Theme.of(context).accentColor),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.5))),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                ),
              ),
              SizedBox(height: 10),
              TextFormField(
                enabled: _con.isUserApiLoading ? false : true,
                controller: _con.userEmailController,
                keyboardType: TextInputType.emailAddress,
                onSaved: (input) => _con.user.email = input,
                validator: (input) {
                  if (!CommonUtils.isValidEmail(input)) {
                    return S.of(context).should_be_a_valid_email;
                  } else {
                    return null;
                  }
                },
                decoration: InputDecoration(
                  labelText: S.of(context).email,
                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                  contentPadding: EdgeInsets.all(12),
                  hintText: 'johndoe@gmail.com',
                  hintStyle: TextStyle(
                      color: Theme.of(context).focusColor.withOpacity(0.7)),
                  prefixIcon: Icon(Icons.alternate_email,
                      color: Theme.of(context).accentColor),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.5))),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                ),
              ),
              SizedBox(height: 10),
              TextFormField(
                enabled: _con.isUserApiLoading ? false : true,
                obscureText: _con.hidePassword,
                onSaved: (input) => _con.user.password = input,
                validator: (input) => input.length < 6
                    ? S.of(context).should_be_more_than_6_letters
                    : null,
                decoration: InputDecoration(
                  labelText: S.of(context).password,
                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                  contentPadding: EdgeInsets.all(12),
                  hintText: '••••••••••••',
                  hintStyle: TextStyle(
                      color: Theme.of(context).focusColor.withOpacity(0.7)),
                  prefixIcon: Icon(Icons.lock_outline,
                      color: Theme.of(context).accentColor),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        _con.hidePassword = !_con.hidePassword;
                      });
                    },
                    color: Theme.of(context).focusColor,
                    icon: Icon(_con.hidePassword
                        ? Icons.visibility
                        : Icons.visibility_off),
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.5))),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              Theme.of(context).focusColor.withOpacity(0.2))),
                ),
              ),
              SizedBox(height: 10),
              getSelectionView(_con.selectedUserTypeIndex),
              SizedBox(height: 10),
              _con.isUserApiLoading ? progressBar : btnLogin,
              SizedBox(height: 10),
              GoogleButton(
                color: Colors.grey,
                onPressed: () {
                  if (CommonUtils.isAndroidPlatform()) {
                    _con.registerWithGoogle();
                  } else {
                    _con.iosGoogleLogin(false);
                  }
                },
              ),
              SizedBox(height: 10),
              AppleSignInButton(
                style: AppleButtonStyle.black,
                type: AppleButtonType.continueButton,
                cornerRadius: 5,
                onPressed: () {
                  _con.iosAppleLogin(false);
                },
              ),

//                      FlatButton(
//                        onPressed: () {
//                          Navigator.of(context).pushNamed('/MobileVerification');
//                        },
//                        padding: EdgeInsets.symmetric(vertical: 14),
//                        color: Theme.of(context).accentColor.withOpacity(0.1),
//                        shape: StadiumBorder(),
//                        child: Text(
//                          'Register with Google',
//                          textAlign: TextAlign.start,
//                          style: TextStyle(
//                            color: Theme.of(context).accentColor,
//                          ),
//                        ),
//                      ),
            ],
          ),
        ),
      ),
    );

    final topOrangeView = Positioned(
      top: 0,
      child: Container(
        width: config.App(context).appWidth(100),
        height: config.App(context).appHeight(29.5),
        decoration: BoxDecoration(color: Theme.of(context).accentColor),
      ),
    );

    final lblRegister = Positioned(
      top: config.App(context).appHeight(10),
      child: Container(
        width: config.App(context).appWidth(84),
        height: config.App(context).appHeight(29.5),
        child: Text(
          S.of(context).lets_start_with_register,
          style: Theme.of(context)
              .textTheme
              .headline2
              .merge(TextStyle(color: Theme.of(context).primaryColor)),
        ),
      ),
    );

    final bottomLoginText =FlatButton(
      onPressed: () {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Navigator.pushNamed(context, "/Login");
        });
      },
      textColor: Theme.of(context).hintColor,
      child: Text(S.of(context).i_have_account_back_to_login),
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: Builder(builder: (context) {
          mContext = context;
          _con.attchContext(context);
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 650,
                  child: Stack(
                    alignment: AlignmentDirectional.topCenter,
                    children: <Widget>[
                      topOrangeView,
                      lblRegister,
                      whiteCardView,
                    ],
                  ),
                ),
                bottomLoginText
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget getSelectionView(int selectedIndex) {
    final row = Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(color: Theme.of(context).accentColor, width: 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(_con.userTypeList.length, (index) {
          return Flexible(
            child: InkWell(
              onTap: () {
                _con.changeSelectedIndex(index);
              },
              child: Container(
                alignment: Alignment.center,
                height: config.App(context).appHeight(5),
                decoration: BoxDecoration(
                    color: index == selectedIndex
                        ? Theme.of(context).accentColor
                        : Colors.white,
                    borderRadius: BorderRadius.circular(10.0)),
                child: Text(
                  _con.userTypeList[index],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: index != selectedIndex
                          ? Theme.of(context).accentColor
                          : Colors.white),
                ),
              ),
            ),
            flex: 1,
          );
        }),
      ),
    );

    return row;
  }
}
