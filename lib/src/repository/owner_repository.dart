import 'dart:convert';
import 'dart:io';

import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/api_url.dart';
import 'package:app/src/helpers/app_contsant.dart';
import 'package:app/src/models/category_master.dart';
import 'package:app/src/models/dashboard_master.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/models/food_detail_master.dart';
import 'package:app/src/models/food_list_master.dart';
import 'package:app/src/models/message_master.dart';
import 'package:app/src/models/order_detail_master.dart';
import 'package:app/src/models/order_list_master.dart';
import 'package:app/src/models/paytabs_request.dart';
import 'package:app/src/models/paytabs_response.dart';
import 'package:app/src/models/restaurent_detail_master.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:app/src/models/status_master.dart';
import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

Future<MessageMaster> addRestaurent(
    {params,
    imagePath,
    Function(MessageMaster) onSuccess,
    Function onFailed,
    Function noInternet}) async {
  final request = await http.MultipartRequest(
      "POST", Uri.parse("https://foodzela.com/api/V1/restaurants/store"));
  request.fields[ApiParams.JSON_CONTENT] = params;
  print("ADD " + params);
  try {
    if (imagePath != null || !imagePath.startsWith("http")) {
      File videoFile = new File(imagePath);
      var stream =
          new http.ByteStream(DelegatingStream.typed(videoFile.openRead()));
      var length = await videoFile.length();
      var multipartFile = new http.MultipartFile("image", stream, length,
          filename: basename(videoFile.path));
      request.files.add(multipartFile);
    }
  } catch (e) {}

  request.send().then((response) {
    print("URL: " + response.request.url.toString());
    try {
      response.stream.transform(utf8.decoder).listen((value) {
        print("Response: " + value);
        print("statusCode: " + response.statusCode.toString());
        MessageMaster addRestaurentMaster =
            MessageMaster.fromJson(jsonDecode(value));
        if (response.statusCode == 200) {
          onSuccess(addRestaurentMaster);
        } else {
          onFailed(addRestaurentMaster);
        }
      });
    } catch (e) {
      print("Error: " + e.toString());
    }
  });
}

Future<MessageMaster> updateRestaurent(
    {params,
    imagePath,
    Function(MessageMaster) onSuccess,
    Function onFailed,
    Function noInternet}) async {
  final request =
      await http.MultipartRequest("POST", Uri.parse(ApiUrl.UPDATE_RESTAURENT));
  request.fields[ApiParams.JSON_CONTENT] = params;
  print("ADD " + params);
  try {
    if (imagePath != null || !imagePath.startsWith("http")) {
      File videoFile = new File(imagePath);
      var stream =
          new http.ByteStream(DelegatingStream.typed(videoFile.openRead()));
      var length = await videoFile.length();
      var multipartFile = new http.MultipartFile("image", stream, length,
          filename: basename(videoFile.path));
      request.files.add(multipartFile);
    }
  } catch (e) {}

  request.send().then((response) {
    print("URL: " + response.request.url.toString());
    try {
      response.stream.transform(utf8.decoder).listen((value) {
        print("Response: " + value);
        print("statusCode: " + response.statusCode.toString());
        MessageMaster addRestaurentMaster =
            MessageMaster.fromJson(jsonDecode(value));
        if (response.statusCode == 200) {
          onSuccess(addRestaurentMaster);
        } else {
          onFailed(addRestaurentMaster);
        }
      });
    } catch (e) {
      print("Error: " + e.toString());
    }
  });
}

Future<MessageMaster> updateFood(
    {params,
    imagePath,
    Function(MessageMaster) onSuccess,
    Function onFailed,
    Function noInternet}) async {
  final request =
      await http.MultipartRequest("POST", Uri.parse(ApiUrl.UPDATE_FOOD));
  request.fields[ApiParams.JSON_CONTENT] = params;
  print("ADD " + params);
  try {
    if (imagePath != null || !imagePath.startsWith("http")) {
      File videoFile = new File(imagePath);
      var stream =
          new http.ByteStream(DelegatingStream.typed(videoFile.openRead()));
      var length = await videoFile.length();
      var multipartFile = new http.MultipartFile("image", stream, length,
          filename: basename(videoFile.path));
      request.files.add(multipartFile);
    }
  } catch (e) {}

  request.send().then((response) {
    print("URL: " + response.request.url.toString());
    try {
      response.stream.transform(utf8.decoder).listen((value) {
        print("Response: " + value);
        print("statusCode: " + response.statusCode.toString());
        MessageMaster addRestaurentMaster =
            MessageMaster.fromJson(jsonDecode(value));
        if (response.statusCode == 200) {
          onSuccess(addRestaurentMaster);
        } else {
          onFailed(addRestaurentMaster);
        }
      });
    } catch (e) {
      print("Error: " + e.toString());
    }
  });
}



Future<DeliveryBoysMaster> getDeliveryBoys(
    {name,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    String getLoginParams() {
      var map = new Map<String, dynamic>();
      map[ApiParams.NAME] = name;
      print("Parameter: " + json.encode(map));
      // return json.encode(map);
    }

    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response = await http
          .post(ApiUrl.GET_DELIVERY_BOYS, body: {ApiParams.NAME: name});
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return DeliveryBoysMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<FoodListMaster> getFoodList(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_FOOD_LIST, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return FoodListMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<MessageMaster> deleteRestaurent(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response = await http
          .post(ApiUrl.DELETE_RESTAURENT, body: {ApiParams.ID: params});
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return MessageMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<MessageMaster> deleteFood(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.DELETE_FOOD, body: {ApiParams.ID: params});
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return MessageMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<RestaurentListMaster> getRestaurentsList(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_RESTAURENTS_LIST, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return RestaurentListMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<MessageMaster> addFoodItem(
    {params,
    imagePath,
    Function(MessageMaster) onSuccess,
    Function onFailed,
    Function noInternet}) async {
  final request =
      await http.MultipartRequest("POST", Uri.parse(ApiUrl.ADD_FOOD));
  request.fields[ApiParams.JSON_CONTENT] = params;
  print("ADD " + params);
  try {
    if (imagePath != null || !imagePath.startsWith("http")) {
      File videoFile = new File(imagePath);
      var stream =
          new http.ByteStream(DelegatingStream.typed(videoFile.openRead()));
      var length = await videoFile.length();
      var multipartFile = new http.MultipartFile("image", stream, length,
          filename: basename(videoFile.path));
      request.files.add(multipartFile);
    }
  } catch (e) {}

  request.send().then((response) {
    print("URL: " + response.request.url.toString());
    try {
      response.stream.transform(utf8.decoder).listen((value) {
        print("Response: " + value);
        print("statusCode: " + response.statusCode.toString());
        MessageMaster addRestaurentMaster =
            MessageMaster.fromJson(jsonDecode(value));
        if (response.statusCode == 200) {
          onSuccess(addRestaurentMaster);
        } else {
          onFailed(addRestaurentMaster);
        }
      });
    } catch (e) {
      print("Error: " + e.toString());
    }
  });
}

Future<CategoryMaster> getAllCategory(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_ALL_CATEGORY, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return CategoryMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<OrderListMaster> getOrderList(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_ORDERS, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return OrderListMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<StatusMaster> getStatusList(
    {Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response = await http.post(ApiUrl.GET_STATUSES);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return StatusMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<RestaurentDetailMaster> getRestarentDetails(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_RESTAURENT_DETAILS, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return RestaurentDetailMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<FoodDetailMaster> getFoodDetail(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_FOOD_DETAILS, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return FoodDetailMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<OrderDetailMaster> getOrderDetails(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_ORDER_DETAILS, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return OrderDetailMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<DashBoardMaster> getDashBoardDetails(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_DASHBOARD, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return DashBoardMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}

Future<OrderDetailMaster> updateOrder(
    {params,
    Function onStartLoading,
    Function onStopLoading,
    Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
          await http.post(ApiUrl.GET_ORDER_UPDATE, body: params);
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      if (onStopLoading != null) onStopLoading();
      return OrderDetailMaster.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }


}

Future<PayTabsResponse> makePaymentRequest(
    {PayTabsRequest payTabsRequest,
      Function onStartLoading,
      Function onStopLoading,
      Function onNoInternet}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {

    print("RRRRR"+json.encode(payTabsRequest));

    try {
      if (onStartLoading != null) onStartLoading();
      final http.Response response =
      await http.post(ApiUrl.PAYMENT_URL, body: {
        "merchant_email":"info@foodzela.com",
        "secret_key":"CrjkqNwjux6GwYLcWuXS7qbfzgPLP9MsA5M0TijoQq4mpQylkwerWpHwGwhvF4Yg9Lg33ArU1wgqmOLHl09hpQO5m5B2aAijlzmp",
        "site_url":" https://foodzela.com/",
        "return_url":"https://foodzela.com/MoyasarDemo/success.php",
        "title":"Food",
        "cc_first_name":payTabsRequest.ccFirstName,
        "cc_last_name":payTabsRequest.ccLastName,
        "cc_phone_number":"+966",
        "phone_number": (payTabsRequest.phoneNumber == null || payTabsRequest.phoneNumber.isEmpty) ? "9585748596" : payTabsRequest.phoneNumber,
        "email":payTabsRequest.email,
        "products_per_title":"Biriyani",
        "unit_price":double.parse(payTabsRequest.unitPrice).toStringAsFixed(0),
        "quantity":"1",
        "other_charges":"0",
        "amount":double.parse(payTabsRequest.unitPrice).toStringAsFixed(0),
        "discount":"0",
        "currency":"SAR",
        "reference_no":DateTime.now().toString(),
        "ip_customer":"121212121",
        "ip_merchant":"12121221",
        "billing_address":"Navabilodra,nadiad",
        "state":"India",
        "city":"India",
        "postal_code":"123456",
        "country":"SAU",
        "shipping_first_name":payTabsRequest.shippingFirstName,
        "shipping_last_name":payTabsRequest.shippingLastName,
        "address_shipping":"Navabilodra,nadiad",
        "city_shipping":"UAE",
        "state_shipping":"Riyadh",
        "postal_code_shipping":"123456",
        "country_shipping":"SAU",
        "msg_lang":"API TOKEN GENERATE",
        "cms_with_version":"English"
      });
      print("URL: " + response.request.url.toString());
      print("response: " + response.body);
      print("responseCode: " + response.statusCode.toString());
      if (onStopLoading != null) onStopLoading();
      return PayTabsResponse.fromJson(json.decode(response.body));
    } catch (err) {
      if (onStopLoading != null) onStopLoading();
      return null;
    }
  } else {
    if (onNoInternet != null) onNoInternet();
  }
}
