import 'package:app/src/models/delivery_boys_master.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DeliveryBoysItem extends StatelessWidget {
  DeliveryBoysDetails deliveryBoy;
  Function onTap;

  DeliveryBoysItem(this.deliveryBoy, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2.0,
              ),
            ],
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(10.0)),
        child: InkWell(
          onTap: onTap,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        child: CachedNetworkImage(
                          height: 40,
                          width: 40,
                          fit: BoxFit.cover,
                          imageUrl: deliveryBoy.image,
                          placeholder: (context, url) => Image.asset(
                            'assets/img/loading.gif',
                            fit: BoxFit.cover,
                            height: 65,
                            width: 65,
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      )),
                  SizedBox(width: 15),
                  Flexible(
                    flex: 8,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                deliveryBoy.name,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                maxLines: 2,
                                style: Theme.of(context).textTheme.title.merge(
                                    TextStyle(
                                        color: Theme.of(context).hintColor)),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          deliveryBoy.email,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.caption,
                        )
                      ],
                    ),
                  ),
                  Spacer(),
                  Flexible(
                      flex: 2,
                      child: Icon(
                        Icons.check_circle,
                        color: deliveryBoy.isSelected
                            ? Theme.of(context).accentColor
                            : Theme.of(context).hintColor,
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
