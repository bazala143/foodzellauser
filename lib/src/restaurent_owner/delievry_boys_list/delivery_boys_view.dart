import 'dart:convert';

import 'package:app/generated/i18n.dart';
import 'package:app/src/elements/user_list_shimmer.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/restaurent_owner/delievry_boys_list/selected_boys_item.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'delievry_boys_item.dart';
import 'delivery_boys_controller.dart';

class DeliveryBoysView extends StatefulWidget {
  final List<DeliveryBoysDetails> boysList;
  DeliveryBoysView(this.boysList);

  @override
  _DeliveryBoysViewState createState() => _DeliveryBoysViewState();
}

class _DeliveryBoysViewState extends StateMVC<DeliveryBoysView> {
  DeliveryBoysController _con;
  TextEditingController searchController = new TextEditingController();

  _DeliveryBoysViewState() : super(DeliveryBoysController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      if (widget.boysList.length > 0) {
        _con.setDeliveryBoysList(widget.boysList);
      }
      _con.getDeliveryBoys(context: context, name: "");
    });
  }

  @override
  Widget build(BuildContext context) {
    final serachBar = Container(
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1.0, color: Theme.of(context).accentColor),
          borderRadius: BorderRadius.circular(10.0)),
      child: TextField(
        onChanged: (String query) {
          if (!_con.isApiLoading && query.length > 2) {
            _con.getDeliveryBoys(context: context, name: query);
          }
        },
        controller: searchController,
        decoration: InputDecoration(
            hintText: S.of(context).searchByName,
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey,
            ),
            border: InputBorder.none),
      ),
    );

    final selectedBoys = _con.selectedBoysList.length == 0
        ? Container(
            height: 1,
            width: 1,
          )
        : Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: app.App(context).appHeight(15.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  physics: ClampingScrollPhysics(),
                  itemCount: _con.selectedBoysList.length,
                  itemBuilder: (_context, index) {
                    return SelectedBoysItem(_con.selectedBoysList[index], () {
                      _con.removeItem(index);
                    });
                  }),
            ),
          );

    final boysListview = ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: _con.boysList.length,
        itemBuilder: (_context, index) {
          return DeliveryBoysItem(_con.boysList[index], () {
            _con.addList(_con.boysList[index]);
          });
        });

    final loader = Container(
      height: MediaQuery.of(context).size.height - 54.0,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.blue),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        actions: [
          _con.selectedBoysList.length > 0
              ? InkWell(
                  onTap: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    await prefs.setString(
                        'list', json.encode(_con.selectedBoysList));
                    Navigator.pop(context, _con.selectedBoysList);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: 50.0,
                    height: 50.0,
                    child: Text(
                      S.of(context).done,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor,
                          fontSize: 12.0),
                    ),
                  ),
                )
              : Container(
                  height: 1,
                  width: 1,
                )
        ],
        title: Text(
          S.of(context).selectDeliveryBoys,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              serachBar,
              selectedBoys,
              _con.isApiLoading ? UserListShimmer() : boysListview
            ],
          ),
        ),
      ),
    );
  }
}
