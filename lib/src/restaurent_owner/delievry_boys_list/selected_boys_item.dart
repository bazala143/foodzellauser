import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SelectedBoysItem extends StatelessWidget {
  DeliveryBoysDetails deliveryBoysDetails;
  Function onDelete;

  SelectedBoysItem(this.deliveryBoysDetails, this.onDelete);

  @override
  Widget build(BuildContext context) {
    final circuleImageView = ClipOval(
      clipBehavior: Clip.antiAlias,
      child: CachedNetworkImage(
        fit: BoxFit.cover,
        height: app.App(context).appHeight(7),
        width: app.App(context).appHeight(7),
        imageUrl: deliveryBoysDetails.image,
      ),
    );

    final name = Container(
      alignment: Alignment.center,
      width: app.App(context).appHeight(12),
      child: Text(
        deliveryBoysDetails.name,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.caption,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
    );

    final iconDelete = Align(
      alignment: Alignment.topRight,
      child: InkWell(
        onTap: onDelete,
        child: Icon(
          Icons.cancel,
          color: Colors.red,
        ),
      ),
    );

    final column = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        circuleImageView,
        SizedBox(
          height: 5.0,
        ),
        name
      ],
    );

    return Align(
      alignment: Alignment.center,
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(right: 10.0),
        height: app.App(context).appHeight(12),
        width: app.App(context).appHeight(12),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 5.0,
          ),
        ], borderRadius: BorderRadius.circular(5.0), color: Colors.white),
        child: Stack(
          children: [column, iconDelete],
        ),
      ),
    );
  }
}
