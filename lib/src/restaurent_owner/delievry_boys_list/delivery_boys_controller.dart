import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class DeliveryBoysController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<DeliveryBoysDetails> boysList = new List();
  List<DeliveryBoysDetails> selectedBoysList = new List();
  int offset = 0;
  bool isApiLoading = false;

  DeliveryBoysController() {
    offset = 0;
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void addList(DeliveryBoysDetails deliveryBoysDetails) {
    bool isAdded = false;
    for (DeliveryBoysDetails details in selectedBoysList) {
      if (details.id == deliveryBoysDetails.id) {
        isAdded = true;
        break;
      }
    }

    if (!isAdded) {
      selectedBoysList.add(deliveryBoysDetails);
      notifyListeners();
    }
  }

  Future<void> getDeliveryBoys({BuildContext context, String name}) async {
    isApiLoading = true;
    notifyListeners();
    DeliveryBoysMaster deliveryBoysMaster =
        await ownerRepository.getDeliveryBoys(
            name: name,
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      boysList.clear();
      if (deliveryBoysMaster.result.length > 0) {
        boysList.addAll(deliveryBoysMaster.result);
        notifyListeners();
      } else {
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }

    isApiLoading = false;
    notifyListeners();
  }

  void removeItem(int index) {
    selectedBoysList.removeAt(index);
    notifyListeners();
  }

  void setDeliveryBoysList(List<DeliveryBoysDetails> apibBoysList) {
    selectedBoysList = apibBoysList;
    notifyListeners();
  }
}
