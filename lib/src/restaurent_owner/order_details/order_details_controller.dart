import 'dart:convert';

import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/models/order_detail_master.dart';
import 'package:app/src/models/status_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OrderDetailsController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<StatusDetails> statusList = new List();
  int startLimit = 0;
  bool isOrderApi = false;
  OrderDetailMaster master;
  String statusId = "", deliveryBoyId = "";

  OrderDetailsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void setDeliveyBoyIndex(int index) {
    if (index == -1) {
      deliveryBoyId = "";
    } else {
      for (int i = 0; i < master.result.deliveryBoys.length; i++) {
        DeliveryBoysDetails deliveryBoysDetails = master.result.deliveryBoys[i];
        if (index == i) {
          deliveryBoysDetails.isSelected = true;
          deliveryBoyId = deliveryBoysDetails.id;
        } else {
          deliveryBoysDetails.isSelected = false;
        }
      }
    }
    notifyListeners();
  }

  Future<void> getOrderDetails({BuildContext context, String id}) async {
    master = await ownerRepository.getOrderDetails(
        params: getListParams(id),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (master != null) {
      if (master.success) {
        for (DeliveryBoysDetails boysDetails in master.result.deliveryBoys) {
          if (boysDetails.id == master.result.deliveryBoyId) {
            boysDetails.isSelected = true;
            break;
          }
        }
        getStatusList(context: context);
        notifyListeners();
      } else {
        CommonUtils.showToast(master.message);
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getListParams(String id) {
    Map<String, dynamic> map = new Map();
    map[ApiParams.ID] = id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> updateOrderApi({BuildContext context}) async {
    isOrderApi = true;
    notifyListeners();
    master = await ownerRepository.updateOrder(
        params: updateOrderParams(),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (master != null) {
      if (master.success) {
        for (DeliveryBoysDetails boysDetails in master.result.deliveryBoys) {
          if (boysDetails.id == master.result.deliveryBoyId) {
            boysDetails.isSelected = true;
          } else {
            boysDetails.isSelected = false;
          }
        }

        for (StatusDetails statusDetails in statusList) {
          if (statusDetails.id.toString() == master.result.statusId) {
            statusDetails.isSelected = true;
          } else {
            statusDetails.isSelected = false;
          }
        }
        CommonUtils.showToast(master.message);
        notifyListeners();
        Navigator.pop(context, true);
      } else {
        CommonUtils.showToast(master.message);
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }

    isOrderApi = false;
    notifyListeners();
  }

  String updateOrderParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.ORDER_ID] = master.result.id;
    map[ApiParams.STATUS_ID] = statusId;
    map[ApiParams.DELIVERY_BOY_ID] = deliveryBoyId;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getStatusList({BuildContext context}) async {
    StatusMaster deliveryBoysMaster = await ownerRepository.getStatusList(
        onStartLoading: () {}, onStopLoading: () {}, onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      statusList.clear();
      if (deliveryBoysMaster.result.length > 0) {
        statusList.addAll(deliveryBoysMaster.result);
        for (StatusDetails statusDetails in statusList) {
          if (statusDetails.id.toString() == master.result.statusId) {
            statusDetails.isSelected = true;
            break;
          }
        }
        notifyListeners();
      } else {
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  void setSelectedStatus(int index) {
    for (StatusDetails statusDetails in statusList) {
      if (statusDetails == statusList[index]) {
        statusDetails.isSelected = true;
        statusId = statusDetails.id.toString();
      } else {
        statusDetails.isSelected = false;
      }
    }
    notifyListeners();
  }
}
