import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/helpers/helper.dart';
import 'package:app/src/models/order_list_master.dart';
import 'package:app/src/restaurent_owner/delievry_boys_list/delievry_boys_item.dart';
import 'package:app/src/restaurent_owner/order_details/order_details_controller.dart';
import 'package:app/src/restaurent_owner/order_details/order_status_item.dart';
import 'package:app/src/restaurent_owner/owner_home/order_item.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OrderDetailsPage extends StatefulWidget {
  OrderDetails orderDetails;

  OrderDetailsPage({this.orderDetails});

  @override
  _OrderDetailsPageState createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends StateMVC<OrderDetailsPage> {
  OrderDetailsController _con;

  _OrderDetailsPageState() : super(OrderDetailsController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _con.getOrderDetails(context: context, id: widget.orderDetails.id);
  }

  @override
  Widget build(BuildContext context) {
    final btnDelete = InkWell(
      onTap: () {
        _con.setSelectedStatus(_con.statusList.length - 1);
        _con.setDeliveyBoyIndex(-1);
        _con.updateOrderApi(context: context);
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).cancel,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnUpdate = InkWell(
      onTap: () {
        _con.updateOrderApi(context: context);
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).update,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnRow = Container(
      height: 60,
      child: Row(
        children: [
          Flexible(
            child: btnDelete,
            flex: 1,
          ),
          Flexible(
            child: btnUpdate,
            flex: 1,
          )
        ],
      ),
    );

    return Scaffold(
      bottomNavigationBar: (_con.isOrderApi ||
              _con.master == null ||
              _con.master.result.statusId == "6")
          ? Container(
              height: 1,
              width: 1,
            )
          : btnRow,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).orderDetails,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: (_con.isOrderApi || _con.master == null)
          ? Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.height,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  getOrderInfo(),
                  getOrderItems(),
                  getCustomerInfo(),
                  getDeliveryBoysView(),
                  getStatusListView(),
                  getBottomSheet()
                ],
              ),
            ),
    );
  }

  Widget getOrderInfo() {
    final idColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.of(context).order_id + _con.master.result.id,
          style: Theme.of(context).textTheme.headline6,
        ),
        Text(
          _con.master.result.createdAt,
          style: Theme.of(context).textTheme.subtitle1,
        )
      ],
    );

    final tvPrice = new RichText(
      text: new TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: 14.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(
              text: getTotal().toString(),
              style: Theme.of(context).textTheme.headline2),
          new TextSpan(
              text: 'SAR', style: Theme.of(context).textTheme.headline5)
        ],
      ),
    );

    final tvStatus = Align(
      alignment: Alignment.bottomRight,
      child: Container(
        margin: EdgeInsets.only(left: 10.0, right: 10.0),
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        decoration: BoxDecoration(
            color: CommonUtils.getStatusColor(_con.master.result.statusId),
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(10.0),
                topLeft: Radius.circular(10.0))),
        child: Text(
          _con.master.result.orderStatus,
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ),
    );

    return Container(
      height: app.App(context).appHeight(14),
      child: Stack(
        children: [
          Container(
            height: app.App(context).appHeight(14),
            margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            padding: EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  new BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2.0,
                  )
                ],
                borderRadius: BorderRadius.circular(10.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [idColumn, tvPrice],
            ),
          ),
          tvStatus
        ],
      ),
    );
  }

  Widget getOrderItems() {
    final tvOrderItems = Align(
      alignment: Alignment.centerLeft,
      child: Text(
        S.of(context).orderItems,
        style: Theme.of(context).textTheme.subtitle2,
      ),
    );

    return Container(
      margin: EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0, bottom: 10.0),
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
          boxShadow: [
            new BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
            )
          ],
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(10.0)),
      child: ClipRRect(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            tvOrderItems,
            SizedBox(
              height: 10.0,
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _con.master.result.foods.length,
                itemBuilder: (context, index) {
                  return OrderItem(
                    food: _con.master.result.foods[index],
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget getCustomerInfo() {
    final tvUserInfo = Align(
      alignment: Alignment.centerLeft,
      child: Text(
        S.of(context).customerInfo,
        style: Theme.of(context).textTheme.subtitle2,
      ),
    );

    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
            )
          ]),
      child: Column(
        children: [
          tvUserInfo,
          getRowWidget(Icons.person, _con.master.result.fullName),
          getRowWidget(Icons.phone, _con.master.result.phone),
          getRowWidget(Icons.location_on, _con.master.result.deliveryAddress),
        ],
      ),
    );
  }

  Widget getBottomSheet() {
    return Container(
      margin: EdgeInsets.all(10.0),
      width: MediaQuery.of(context).size.width,
      height: app.App(context).appHeight(20),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(10.0)),
      child: getColumn(context),
    );
  }

  Widget getColumn(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        children: [
          getPriceView(
              context,
              S.of(context).delivery_fee,
              double.parse(_con.master.result.deliveryFee),
              Theme.of(context).textTheme.headline6),
          getPriceView(
              context,
              S.of(context).tax,
              double.parse(_con.master.result.tax),
              Theme.of(context).textTheme.headline6),
          getPriceView(context, S.of(context).total, getTotal(),
              Theme.of(context).textTheme.headline2),
        ],
      ),
    );
  }

  Widget getPriceView(
      BuildContext context, String label, double price, TextStyle textStyle) {
    final tvLable = Text(
      label,
      style: textStyle,
    );
    final tvPrice = Helper.getPrice(price, context, style: textStyle);
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [tvLable, tvPrice],
      ),
    );
  }

  Widget getRowWidget(IconData icons, String value) {
    return Container(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 35,
            width: 35,
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: Theme.of(context).accentColor),
            child: Icon(
              icons,
              size: 20.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.subtitle1,
          )
        ],
      ),
    );
  }

  Widget getDeliveryBoysView() {
    final tvAssign = Align(
      alignment: Alignment.centerLeft,
      child: Text(
        S.of(context).assignDeliveryBoys,
        style: Theme.of(context).textTheme.subtitle2,
      ),
    );
    final boysListview = ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: _con.master.result.deliveryBoys.length,
        itemBuilder: (_context, index) {
          return DeliveryBoysItem(_con.master.result.deliveryBoys[index], () {
            _con.setDeliveyBoyIndex(index);
          });
        });

    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
            )
          ]),
      child: Column(
        children: [tvAssign, boysListview],
      ),
    );
  }

  Widget getStatusListView() {
    final tvAssign = Align(
      alignment: Alignment.centerLeft,
      child: Text(
        S.of(context).changeStatus,
        style: Theme.of(context).textTheme.subtitle2,
      ),
    );
    final boysListview = ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: _con.statusList.length,
        itemBuilder: (_context, index) {
          return OrderStatusItem(_con.statusList[index], () {
            _con.setSelectedStatus(index);
          });
        });

    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
            )
          ]),
      child: Column(
        children: [tvAssign, boysListview],
      ),
    );
  }

  double getTotal() {
    double sum = 0;
    for (FoodItems foodItems in _con.master.result.foods) {
      sum = sum +
          (double.parse(foodItems.price) * double.parse(foodItems.quantity));
    }
    sum = sum +
        double.parse(_con.master.result.deliveryFee) +
        double.parse(_con.master.result.tax);
    return sum;
  }
}
