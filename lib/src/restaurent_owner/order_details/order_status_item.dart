import 'package:app/src/models/status_master.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderStatusItem extends StatelessWidget {
  StatusDetails deliveryBoy;
  Function onTap;

  OrderStatusItem(this.deliveryBoy, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2.0,
              ),
            ],
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(10.0)),
        child: InkWell(
          onTap: onTap,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 8,
                    child: Text(
                      deliveryBoy.name,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      maxLines: 2,
                      style: Theme.of(context)
                          .textTheme
                          .title
                          .merge(TextStyle(color: Theme.of(context).hintColor)),
                    ),
                  ),
                  Spacer(),
                  Flexible(
                      flex: 2,
                      child: Icon(
                        deliveryBoy.isSelected
                            ? Icons.radio_button_checked
                            : Icons.radio_button_unchecked,
                        color: deliveryBoy.isSelected
                            ? Theme.of(context).accentColor
                            : Theme.of(context).hintColor,
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
