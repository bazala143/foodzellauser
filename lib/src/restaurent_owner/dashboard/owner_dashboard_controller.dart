import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OwnerDashBoardController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;

  int currentPage = 0;

  OwnerDashBoardController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void setCurrentPage(int index) {
    this.currentPage = index;
    notifyListeners();
  }
}
