import 'package:app/src/pages/notifications.dart';
import 'package:app/src/restaurent_owner/dashboard/owner_dashboard_controller.dart';
import 'package:app/src/restaurent_owner/drawer/owner_drawer.dart';
import 'package:app/src/restaurent_owner/my_foods/my_foods_view.dart';
import 'package:app/src/restaurent_owner/my_restaurents/my_restaurents.dart';
import 'package:app/src/restaurent_owner/owner_home/owner_home.dart';
import 'package:app/src/restaurent_owner/profile/owner_profile.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OwnerDashBoard extends StatefulWidget {
  int currentTab;
  OwnerDashBoard({this.currentTab});

  @override
  _OwnerDashBoardState createState() => _OwnerDashBoardState();
}

class _OwnerDashBoardState extends StateMVC<OwnerDashBoard> {
  OwnerDashBoardController _con;

  _OwnerDashBoardState() : super(OwnerDashBoardController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _con.currentPage = widget.currentTab;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _con.scaffoldKey,
        drawer: OwnerDrawer(),
        body: callPage(_con.currentPage),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Theme.of(context).accentColor,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 28),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          currentIndex: _con.currentPage,
          onTap: (int i) {
            _con.setCurrentPage(i);
          },
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              title: new Container(height: 0.0),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: new Container(height: 0.0),
            ),
            BottomNavigationBarItem(
                title: new Container(height: 5.0),
                icon: Container(
                  width: 42,
                  height: 42,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).accentColor.withOpacity(0.4),
                          blurRadius: 40,
                          offset: Offset(0, 15)),
                      BoxShadow(
                          color: Theme.of(context).accentColor.withOpacity(0.4),
                          blurRadius: 13,
                          offset: Offset(0, 3))
                    ],
                  ),
                  child: new Icon(Icons.home,
                      color: Theme.of(context).primaryColor),
                )),
            BottomNavigationBarItem(
              icon: new Icon(Icons.fastfood),
              title: new Container(height: 0.0),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.restaurant),
              title: new Container(height: 0.0),
            ),
          ],
        ),
      ),
    );
  }

  Widget callPage(int currentPage) {
    switch (currentPage) {
      case 0:
        return NotificationsWidget(
          parentScaffoldKey: _con.scaffoldKey,
        );
        break;

      case 1:
        return OwnerProfile(
          parentScaffoldKey: _con.scaffoldKey,
        );

        break;

      case 2:
        return OwnerHome(parentScaffoldKey: _con.scaffoldKey);

        break;

      case 3:
        return MyFoodsView(
          parentScaffoldKey: _con.scaffoldKey,
        );

        break;
      case 4:
        return MyRestaurentView(
          parentScaffoldKey: _con.scaffoldKey,
        );

        break;

      default:
        break;
    }
  }
}
