import 'dart:convert';

import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/category_master.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/models/food_detail_master.dart';
import 'package:app/src/models/food_request.dart';
import 'package:app/src/models/message_master.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:google_maps_flutter_platform_interface/src/types/location.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class CreateFoodController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  final nameController = TextEditingController();
  final priceController = TextEditingController();
  final discountPriceController = TextEditingController();
  final weightController = TextEditingController();
  final deliveryFeeController = TextEditingController();
  final descriptionController = TextEditingController();
  final ingriedientsController = TextEditingController();

  bool isAddApiLoading = false;
  String foodId;

  List<DeliveryBoysDetails> selectedBoysList = new List();

  List<RestaurentDetails> restaurentList = new List();
  List<CategoryDetail> categoryList = new List();

  RestaurentDetails selectedRestaurent;

  CategoryDetail selectedCategory;

  bool isFeatured = false;

  String imagePath;

  void setId(String foodId) {
    this.foodId = foodId;
  }

  final picker = ImagePicker();
  PickedFile image;

  Future<void> pickFile() async {
    image = await picker.getImage(source: ImageSource.gallery);
    imagePath = image.path;
    notifyListeners();
  }

  CreateFoodController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  Future<void> getAllRestaurentList({BuildContext context}) async {
    isAddApiLoading = true;
    notifyListeners();
    RestaurentListMaster deliveryBoysMaster =
        await ownerRepository.getRestaurentsList(
            params: getListParams(),
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      isAddApiLoading = false;
      notifyListeners();
      if (deliveryBoysMaster.result.length > 0) {
        restaurentList.addAll(deliveryBoysMaster.result);
        selectedRestaurent = restaurentList[0];
        getCategoryApi(context: context);
      } else {
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      isAddApiLoading = false;
      notifyListeners();
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getListParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.USER_ID] = userRepository.currentUser.value.id;
    map[ApiParams.START_LIMIT] = -1;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getCategoryApi({BuildContext context}) async {
    isAddApiLoading = true;
    notifyListeners();
    CategoryMaster deliveryBoysMaster = await ownerRepository.getAllCategory(
        params: getListParams(),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      categoryList.clear();
      if (deliveryBoysMaster.result.length > 0) {
        categoryList.addAll(deliveryBoysMaster.result);
        selectedCategory = categoryList[0];
        if (foodId != null) {
          getFoodDetails(id: foodId);
        } else {
          isAddApiLoading = false;
          notifyListeners();
        }
      } else {
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      isAddApiLoading = false;
      notifyListeners();
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getCatListParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.USER_ID] = userRepository.currentUser.value.id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> createFoodApi() async {
    isAddApiLoading = true;
    notifyListeners();
    FoodRequest request = new FoodRequest();
    request.name = nameController.text;
    request.price = priceController.text;
    request.discountPrice = discountPriceController.text;
    request.description = descriptionController.text;
    request.categoryId = selectedCategory.id;
    request.restaurantId = selectedRestaurent.id;
    request.featured = isFeatured ? "1" : "0";
    request.weight = weightController.text;
    request.ingredients = ingriedientsController.text;

    await ownerRepository.addFoodItem(
        params: jsonEncode(request),
        imagePath: imagePath,
        onFailed: () {
          CommonUtils.showToast("Failed to add");
          isAddApiLoading = false;
          notifyListeners();
        },
        onSuccess: (MessageMaster addRestaurentMaster) {
          if (addRestaurentMaster.success == true) {
            CommonUtils.showToast("Food Item added");
            Navigator.pop(context, selectedRestaurent);
          } else {
            CommonUtils.showToast(addRestaurentMaster.message);
          }
          isAddApiLoading = false;
          notifyListeners();
        });
  }

  Future<void> updateFoodApi({String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    FoodRequest request = new FoodRequest();
    request.id = id;
    request.name = nameController.text;
    request.price = priceController.text;
    request.discountPrice = discountPriceController.text;
    request.description = descriptionController.text;
    request.categoryId = selectedCategory.id;
    request.restaurantId = selectedRestaurent.id;
    request.featured = isFeatured ? "1" : "0";
    request.weight = weightController.text;
    request.ingredients = ingriedientsController.text;

    await ownerRepository.updateFood(
        params: jsonEncode(request),
        imagePath: imagePath,
        onFailed: () {
          CommonUtils.showToast("Failed to add");
          isAddApiLoading = false;
          notifyListeners();
        },
        onSuccess: (MessageMaster addRestaurentMaster) {
          if (addRestaurentMaster.success == true) {
            CommonUtils.showToast("Food Updated");
            Navigator.pop(context, selectedRestaurent);
          } else {
            CommonUtils.showToast(addRestaurentMaster.message);
          }
          isAddApiLoading = false;
          notifyListeners();
        });
  }

  Future<void> deleteFood({String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    MessageMaster restaurentDetailMaster = await ownerRepository.deleteFood(
        params: id,
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (restaurentDetailMaster != null) {
      if (restaurentDetailMaster.success) {
        CommonUtils.showToast(restaurentDetailMaster.message);
        Navigator.pop(context, true);
      } else {
        CommonUtils.showToast(restaurentDetailMaster.message);
      }
    } else {
      CommonUtils.showToast("Oops something went wrong");
    }

    isAddApiLoading = false;
    notifyListeners();
  }

  String getOrderListParams(String id) {
    Map<String, dynamic> map = new Map();
    map[ApiParams.ID] = id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getFoodDetails({String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    FoodDetailMaster restaurentDetailMaster =
        await ownerRepository.getFoodDetail(
            params: getOrderListParams(id),
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (restaurentDetailMaster != null) {
      if (restaurentDetailMaster.success) {
        nameController.text = restaurentDetailMaster.result.name;
        descriptionController.text = restaurentDetailMaster.result.description;
        deliveryFeeController.text =
            restaurentDetailMaster.result.discountPrice.toString();
        priceController.text = restaurentDetailMaster.result.price.toString();
        discountPriceController.text =
            restaurentDetailMaster.result.discountPrice;
        weightController.text = restaurentDetailMaster.result.weight.toString();
        imagePath = restaurentDetailMaster.result.image;
        ingriedientsController.text = restaurentDetailMaster.result.ingredients;
        isFeatured = restaurentDetailMaster.result.featured;

        for (RestaurentDetails restaurentDetails in restaurentList) {
          if (restaurentDetails.id ==
              restaurentDetailMaster.result.restaurantId) {
            selectedRestaurent = restaurentDetails;
            break;
          }
        }

        for (CategoryDetail categoryDetails in categoryList) {
          if (categoryDetails.id == restaurentDetailMaster.result.categoryId) {
            selectedCategory = categoryDetails;
            break;
          }
        }

        notifyListeners();
      } else {
        CommonUtils.showToast(restaurentDetailMaster.message);
      }
      isAddApiLoading = false;
      notifyListeners();
    } else {
      isAddApiLoading = false;
      notifyListeners();
      CommonUtils.showToast("Oops something went wrong");
    }
  }

  bool checkValiDation() {
    if (imagePath == null) {
      CommonUtils.showToast(S.of(context).pleaseSelectFoodPicture);
      return false;
    } else if (nameController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseSelectFoodPicture);
      return false;
    } else if (priceController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterPrice);
      return false;
    } else if (discountPriceController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterDiscountPrice);
      return false;
    } else if (descriptionController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterDescription);
      return false;
    } else if (ingriedientsController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterDiscountPrice);
      return false;
    } else if (weightController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterWeight);
      return false;
    } else if (selectedRestaurent == null) {
      CommonUtils.showToast(S.of(context).pleaseSelectRestaurent);
      return false;
    } else if (selectedCategory == null) {
      CommonUtils.showToast(S.of(context).pleaseSelectCategory);
      return false;
    }
    return true;
  }

  void removeItem(int index) {
    selectedBoysList.removeAt(index);
    notifyListeners();
  }

  LatLng selectedLocation;

  void setLocation(LatLng latLng) {
    selectedLocation = latLng;
    notifyListeners();
  }

  void setselectedRestaurent(RestaurentDetails newValue) {
    selectedRestaurent = newValue;
    notifyListeners();
  }

  void setSelectedCategory(CategoryDetail newValue) {
    selectedCategory = newValue;
    notifyListeners();
  }

  void setFeatured() {
    isFeatured = !isFeatured;
    notifyListeners();
  }
}
