import 'dart:io';

import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/models/category_master.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

import 'create_food_controller.dart';

class CreateFoodView extends StatefulWidget {
  String id;

  CreateFoodView(this.id);

  @override
  _CreateFoodViewState createState() => _CreateFoodViewState();
}

class _CreateFoodViewState extends StateMVC<CreateFoodView> {
  CreateFoodController _con;

  _CreateFoodViewState() : super(CreateFoodController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _con.setId(widget.id);
      _con.getAllRestaurentList(context: context);
    });
  }

  @override
  Widget build(BuildContext context) {
    final imageView = InkWell(
      onTap: () {
        _con.pickFile();
      },
      child: Container(
        width: app.App(context).appWidth(100),
        height: app.App(context).appHeight(30),
        decoration: BoxDecoration(
          color: app.Colors().accentColor(0.2),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          clipBehavior: Clip.antiAlias,
          child: _con.imagePath != null
              ? FadeInImage(
                  width: app.App(context).appWidth(100),
                  height: app.App(context).appHeight(30),
                  fit: BoxFit.cover,
                  placeholder: AssetImage("assets/img/ic_place_holder.jpg"),
                  image: _con.imagePath.startsWith("http")
                      ? NetworkImage(_con.imagePath)
                      : FileImage(new File(_con.imagePath)))
              : FadeInImage(
                  fit: BoxFit.cover,
                  placeholder: AssetImage("assets/img/ic_place_holder.jpg"),
                  image: AssetImage("assets/img/ic_place_holder.jpg")),
        ),
      ),
    );

    final btnDelete = InkWell(
      onTap: () {
        _con.deleteFood(id: widget.id);
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).delete,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnUpdate = InkWell(
      onTap: () {
        if (_con.checkValiDation()) {
          _con.updateFoodApi(id: widget.id);
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).update,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnRow = Container(
      child: Row(
        children: [
          Flexible(
            child: btnDelete,
            flex: 1,
          ),
          Flexible(
            child: btnUpdate,
            flex: 1,
          )
        ],
      ),
    );

    final btnSave = InkWell(
      onTap: () {
        if (_con.checkValiDation()) {
          _con.createFoodApi();
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).save,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final checkBox = Icon(
      _con.isFeatured ? Icons.check_box : Icons.check_box_outline_blank,
      color: Theme.of(context).accentColor,
    );

    final tvFeatured = Container(
      child: Text(
        S.of(context).featured,
        style: Theme.of(context).textTheme.button,
      ),
    );

    final row = InkWell(
      onTap: () {
        _con.setFeatured();
      },
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Row(
          children: [
            checkBox,
            SizedBox(
              width: 10.0,
            ),
            tvFeatured
          ],
        ),
      ),
    );

    return Scaffold(
      bottomNavigationBar: Visibility(
        child: Container(
          child: widget.id == null ? btnSave : btnRow,
        ),
        visible: !(_con.isAddApiLoading),
      ),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).createFood,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: (_con.isAddApiLoading)
          ? Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Form(
              child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: [
                  imageView,
                  getLabeledInput(
                      label: S.of(context).name,
                      controller: _con.nameController,
                      textInputType: TextInputType.text,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).price,
                      controller: _con.priceController,
                      textInputType: TextInputType.phone,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).discountPrice,
                      controller: _con.discountPriceController,
                      textInputType: TextInputType.number,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).description,
                      controller: _con.descriptionController,
                      textInputType: TextInputType.text,
                      maxlines: 4),
                  getLabeledInput(
                      label: S.of(context).ingredients,
                      controller: _con.ingriedientsController,
                      textInputType: TextInputType.text,
                      maxlines: 4),
                  getLabeledInput(
                      label: S.of(context).Weight,
                      controller: _con.weightController,
                      textInputType: TextInputType.number,
                      maxlines: 1),
                  row,
                  getRestaurentDropDownView(_con.restaurentList),
                  getcategoryDropDownView(_con.categoryList),
                ],
              ),
            )),
    );
  }

  Widget getLabeledInput(
      {String label,
      int maxlines,
      TextEditingController controller,
      TextInputType textInputType}) {
    final tvLable = Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        label,
        style: Theme.of(context).textTheme.button,
      ),
    );

    final inputField = Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      decoration: BoxDecoration(
          color: app.Colors().accentColor(0.1),
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: app.Colors().accentColor(0.2))),
      child: TextField(
        onSubmitted: (_) {
          if (controller == _con.weightController) {
            FocusScope.of(context).unfocus();
          } else {
            FocusScope.of(context).nextFocus();
          }
        },
        controller: controller,
        keyboardType: textInputType,
        textAlign: TextAlign.start,
        textInputAction: TextInputAction.next,
        maxLines: maxlines,
        style: Theme.of(context).textTheme.button,
        decoration: InputDecoration(border: InputBorder.none),
      ),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        tvLable,
        SizedBox(
          height: 10.0,
        ),
        inputField
      ],
    );
  }

  Widget getRestaurentDropDownView(List<RestaurentDetails> restaurentList) {
    return _con.selectedRestaurent == null
        ? Shimmer.fromColors(
            child: new Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: app.Colors().accentColor(0.2))),
              margin: EdgeInsets.only(top: 10.0),
              width: app.App(context).appWidth(100),
              height: 50.0,
              child: Text("Please wait..."),
            ),
            baseColor: Theme.of(context).accentColor,
            highlightColor: Colors.white)
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text(
                  S.of(context).restaurent,
                  style: Theme.of(context).textTheme.button,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                width: app.App(context).appWidth(100),
                decoration: BoxDecoration(
                    color: app.Colors().accentColor(0.1),
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: app.Colors().accentColor(0.2))),
                child: DropdownButtonHideUnderline(
                    child: DropdownButton<RestaurentDetails>(
                  value: _con.selectedRestaurent,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  onChanged: (RestaurentDetails newValue) {
                    _con.setselectedRestaurent(newValue);
                  },
                  items: restaurentList
                      .map<DropdownMenuItem<RestaurentDetails>>(
                          (RestaurentDetails value) {
                    return DropdownMenuItem<RestaurentDetails>(
                      value: value,
                      child: Container(
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Text(
                          value.name,
                          style: Theme.of(context).textTheme.button,
                        ),
                      ),
                    );
                  }).toList(),
                )),
              )
            ],
          );
  }

  Widget getcategoryDropDownView(List<CategoryDetail> categoryList) {
    return _con.selectedCategory == null
        ? Shimmer.fromColors(
            child: new Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: app.Colors().accentColor(0.2))),
              margin: EdgeInsets.only(top: 10.0),
              width: app.App(context).appWidth(100),
              height: 50.0,
              child: Text("Please wait..."),
            ),
            baseColor: Theme.of(context).accentColor,
            highlightColor: Colors.white)
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text(
                  S.of(context).category,
                  style: Theme.of(context).textTheme.button,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                width: app.App(context).appWidth(100),
                decoration: BoxDecoration(
                    color: app.Colors().accentColor(0.1),
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: app.Colors().accentColor(0.2))),
                child: DropdownButtonHideUnderline(
                    child: DropdownButton<CategoryDetail>(
                  value: _con.selectedCategory,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  onChanged: (CategoryDetail newValue) {
                    _con.setSelectedCategory(newValue);
                  },
                  items: categoryList.map<DropdownMenuItem<CategoryDetail>>(
                      (CategoryDetail value) {
                    return DropdownMenuItem<CategoryDetail>(
                      value: value,
                      child: Container(
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Text(
                          value.name,
                          style: Theme.of(context).textTheme.button,
                        ),
                      ),
                    );
                  }).toList(),
                )),
              )
            ],
          );
  }
}
