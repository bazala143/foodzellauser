import 'package:app/generated/i18n.dart';
import 'package:app/src/elements/empty_food_widget.dart';
import 'package:app/src/elements/list_shimmer.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:app/src/restaurent_owner/create_food/create_food_view.dart';
import 'package:app/src/restaurent_owner/my_foods/my_food_item.dart';
import 'package:app/src/restaurent_owner/my_foods/my_foods_controller.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class MyFoodsView extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MyFoodsView({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _MyFoodsViewState createState() => _MyFoodsViewState();
}

class _MyFoodsViewState extends StateMVC<MyFoodsView> {
  MyFoodsController _con;

  _MyFoodsViewState() : super(MyFoodsController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _con.getAllRestaurentList();
    });
  }

  @override
  Widget build(BuildContext context) {
    final restaurentListview = ListView.builder(
        padding: EdgeInsets.all(10.0),
        shrinkWrap: true,
        itemCount: _con.foodsList.length,
        physics: ClampingScrollPhysics(),
        itemBuilder: (_context, index) {
          return MyFoodItem(
            food: _con.foodsList[index],
            onClick: () async {
              RestaurentDetails restaurentDetails = await Navigator.push(
                  context, MaterialPageRoute(builder: (_context) {
                return CreateFoodView(_con.foodsList[index].id.toString());
              }));

              if (restaurentDetails != null) {
                _con.setselectedRestaurentId(restaurentDetails);
              }
            },
          );
        });

    final notificationListview = NotificationListener<ScrollNotification>(
      child: restaurentListview,
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (_con.startLimit < _con.totalCount && !_con.isApiLoading) {
            _con.startLimit = _con.startLimit + 5;
            _con.getFoodList();
          }
        }
      },
    );

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          RestaurentDetails details = await Navigator.push(context,
              MaterialPageRoute(builder: (_context) {
            return CreateFoodView(null);
          })) as RestaurentDetails;

          if (details != null) {
            _con.setselectedRestaurentId(details);
          }
        },
        child: Text(
          S.of(context).add,
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ),
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).myFoods,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            getRestaurentDropDownView(_con.restaurentList),
            SizedBox(
              height: 10.0,
            ),
            _con.isApiLoading
                ? ShimmerListView()
                : (_con.isEmptyFoods ? EmptyFoodWidget() : notificationListview)
          ],
        ),
      ),
    );
  }

  Widget getRestaurentDropDownView(List<RestaurentDetails> restaurentList) {
    return _con.selectedRestaurent == null
        ? Shimmer.fromColors(
            child: new Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: app.Colors().accentColor(0.2))),
              margin: EdgeInsets.all(10.0),
              width: app.App(context).appWidth(100),
              height: 50.0,
              child: Text("Please wait..."),
            ),
            baseColor: Theme.of(context).accentColor,
            highlightColor: Colors.white)
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  S.of(context).restaurent,
                  style: Theme.of(context).textTheme.button,
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                width: app.App(context).appWidth(100),
                decoration: BoxDecoration(
                    color: app.Colors().accentColor(0.1),
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: app.Colors().accentColor(0.2))),
                child: DropdownButtonHideUnderline(
                    child: DropdownButton<RestaurentDetails>(
                  value: _con.selectedRestaurent,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  onChanged: (RestaurentDetails newValue) {
                    _con.setselectedRestaurentId(newValue);
                  },
                  items: restaurentList
                      .map<DropdownMenuItem<RestaurentDetails>>(
                          (RestaurentDetails value) {
                    return DropdownMenuItem<RestaurentDetails>(
                      value: value,
                      child: Container(
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Text(
                          value.name,
                          style: Theme.of(context).textTheme.button,
                        ),
                      ),
                    );
                  }).toList(),
                )),
              )
            ],
          );
  }
}
