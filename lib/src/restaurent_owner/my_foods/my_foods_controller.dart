import 'dart:convert';

import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/food_list_master.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyFoodsController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<FoodDetails> foodsList = new List();
  List<RestaurentDetails> restaurentList = new List();
  RestaurentDetails selectedRestaurent;
  BuildContext mContext;
  bool isApiLoading = true;
  int startLimit = 0;
  int totalCount = 0;
  bool isEmptyFoods = false;

  MyFoodsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void attachContext(BuildContext context) {
    this.mContext = context;
  }

  Future<void> getAllRestaurentList() async {
    isApiLoading = true;
    notifyListeners();
    RestaurentListMaster deliveryBoysMaster =
        await ownerRepository.getRestaurentsList(
            params: getAllListParams(),
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      isApiLoading = false;
      notifyListeners();
      if (deliveryBoysMaster.result.length > 0) {
        restaurentList.addAll(deliveryBoysMaster.result);
        selectedRestaurent = restaurentList[0];
        getFoodList(id: selectedRestaurent.id.toString());
      } else {
        //CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      isApiLoading = false;
      notifyListeners();
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getAllListParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.USER_ID] = userRepository.currentUser.value.id;
    map[ApiParams.START_LIMIT] = -1;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getFoodList({String id}) async {
    isApiLoading = true;
    isEmptyFoods = false;
    if (startLimit == 0) {
      foodsList.clear();
    }
    notifyListeners();
    FoodListMaster deliveryBoysMaster = await ownerRepository.getFoodList(
        params: getListParams(id ?? selectedRestaurent.id.toString()),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      totalCount = deliveryBoysMaster.totalCount;

      if (deliveryBoysMaster.result != null &&
          deliveryBoysMaster.result.length > 0) {
        foodsList.addAll(deliveryBoysMaster.result);
        notifyListeners();
      } else {
        if (startLimit == 0) {
          isEmptyFoods = true;
        }
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
      isApiLoading = false;
      notifyListeners();
    } else {
      if (startLimit == 0) {
        isEmptyFoods = true;
      }
      isApiLoading = false;
      notifyListeners();
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getListParams(String id) {
    Map<String, dynamic> map = new Map();
    map[ApiParams.START_LIMIT] = startLimit;
    map[ApiParams.RESTAURENT_ID] = id;
    print("food_params" + jsonEncode(map));
    return jsonEncode(map);
  }

  void setselectedRestaurent(RestaurentDetails newValue) {}

  void setselectedRestaurentId(RestaurentDetails details) {
    for (RestaurentDetails restaurentDetails in restaurentList) {
      if (restaurentDetails.id == details.id) {
        selectedRestaurent = restaurentDetails;
        getFoodList(id: selectedRestaurent.id.toString());
        break;
      }
    }
    notifyListeners();
  }
}
