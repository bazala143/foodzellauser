import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OwnerProfileController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  PickedFile image;
  bool isUpdating = false;

  OwnerHomeController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  bool isEditMode = false;

  void setEditMode(bool isEditMode) {
    this.isEditMode = isEditMode;
    notifyListeners();
  }

  final picker = ImagePicker();

  Future<void> pickFile() async {
    image = await picker.getImage(source: ImageSource.gallery);
    notifyListeners();
  }

  Future<void> updateProfile(name, email, address, bio, phone) async {
    isUpdating = true;
    notifyListeners();
    await userRepository.updateOwnerProfile(
        name: name,
        email: email,
        address: address,
        bio: bio,
        phone: phone,
        userId: userRepository.currentUser.value.id,
        imagePath: image == null ? null : image.path,
        onSuccess: () {
          isUpdating = false;
          isEditMode = false;
          notifyListeners();
          CommonUtils.showToast("Profile Updates");
        },
        onFailed: () {
          isUpdating = false;
          isEditMode = false;
          notifyListeners();
          CommonUtils.showToast("Failed to upate");
        });
  }
}
