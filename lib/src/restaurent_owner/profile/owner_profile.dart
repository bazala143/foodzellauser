import 'package:app/generated/i18n.dart';
import 'package:app/src/repository/user_repository.dart';
import 'package:app/src/restaurent_owner/profile/owner_profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'owner_profile_widget.dart';

class OwnerProfile extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  OwnerProfile({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _OwnerProfileState createState() => _OwnerProfileState();
}

class _OwnerProfileState extends StateMVC<OwnerProfile> {
  OwnerProfileController _con;
  TextEditingController aboutController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();

  _OwnerProfileState() : super(OwnerProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      aboutController.text = currentUser.value?.bio ?? "";
      emailController.text = currentUser.value?.email ?? "";
      phoneController.text = currentUser.value?.phone ?? "";
      addressController.text = currentUser.value?.address ?? "";
      nameController.text = currentUser.value?.name ?? "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            onTap: () {
              if (_con.isEditMode) {
                _con.updateProfile(
                    nameController.text.toString(),
                    emailController.text.trim(),
                    addressController.text.toString(),
                    aboutController.text,
                    phoneController.text);
              } else {
                _con.setEditMode(true);
              }
            },
            child: Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: _con.isUpdating
                  ? Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      ),
                    )
                  : (_con.isEditMode
                      ? Icon(
                          Icons.save,
                          color: Theme.of(context).primaryColor,
                        )
                      : Icon(
                          Icons.edit,
                          color: Theme.of(context).primaryColor,
                        )),
            ),
          )
        ],
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).profile,
          style: Theme.of(context).textTheme.title.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            OwnerProfileAvatarWidget(
              onTap: () {
                _con.pickFile();
              },
              imagePath: _con.image != null ? _con.image.path : null,
              user: currentUser.value,
              nameController: nameController,
              isEditMode: _con.isEditMode,
            ),
            getInputView(S.of(context).about, aboutController, Icons.person),
            getInputView(S.of(context).email, emailController, Icons.email),
            getInputView(
                S.of(context).phone, phoneController, Icons.phone_android),
            getInputView(S.of(context).address, addressController, Icons.home),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }

  Widget getInputView(String lable, TextEditingController textEditingController,
      IconData iconData) {
    final etLable = Text(
      lable,
      style: Theme.of(context).textTheme.headline5,
    );

    final icon = Icon(
      iconData,
      color: Theme.of(context).accentColor,
    );

    final row = Row(
      children: [
        icon,
        SizedBox(
          width: 10.0,
        ),
        etLable
      ],
    );

    final textfield = TextField(
      controller: textEditingController,
      maxLines: null,
      decoration: _con.isEditMode
          ? InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Theme.of(context).hintColor,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Theme.of(context).hintColor,
                ),
              ),
              border: new OutlineInputBorder(
                  borderSide:
                      new BorderSide(color: Theme.of(context).hintColor)))
          : InputDecoration(border: InputBorder.none),
      style: Theme.of(context).textTheme.bodyText2,
    );

    return Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.only(top: 10.0, right: 10.0, left: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).primaryColor,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(1),
            spreadRadius: 1,
            blurRadius: 1,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          row,
          SizedBox(
            height: 10.0,
          ),
          textfield
        ],
      ),
    );
  }
}
