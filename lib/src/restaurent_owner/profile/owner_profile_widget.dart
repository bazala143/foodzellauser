import 'dart:io';

import 'package:app/src/models/user.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class OwnerProfileAvatarWidget extends StatelessWidget {
  final User user;
  final bool isEditMode;
  final Function onTap;
  final imagePath;
  final TextEditingController nameController;

  OwnerProfileAvatarWidget(
      {Key key,
      this.user,
      this.nameController,
      this.onTap,
      this.imagePath,
      this.isEditMode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: 160,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
//              SizedBox(
//                width: 50,
//                height: 50,
//                child: FlatButton(
//                  padding: EdgeInsets.all(0),
//                  onPressed: () {},
//                  child: Icon(Icons.add, color: Theme.of(context).primaryColor),
//                  color: Theme.of(context).accentColor,
//                  shape: StadiumBorder(),
//                ),
//              ),
                Container(
                  height: 135,
                  width: 135,
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(300)),
                        child: imagePath != null
                            ? FadeInImage(
                                placeholder: AssetImage("assets/img/logo.png"),
                                height: 135,
                                width: 135,
                                fit: BoxFit.cover,
                                image: FileImage(new File(imagePath)),
                              )
                            : CachedNetworkImage(
                                height: 135,
                                width: 135,
                                fit: BoxFit.cover,
                                imageUrl: user?.userImage,
                                placeholder: (context, url) => Image.asset(
                                  'assets/img/loading.gif',
                                  fit: BoxFit.cover,
                                  height: 135,
                                  width: 135,
                                ),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Visibility(
                          child: InkWell(
                            onTap: onTap,
                            child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  shape: BoxShape.circle),
                              child: Icon(
                                Icons.camera_alt,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                          ),
                          visible: isEditMode,
                        ),
                      )
                    ],
                  ),
                ),
//              SizedBox(
//                width: 50,
//                height: 50,
//                child: FlatButton(
//                  padding: EdgeInsets.all(0),
//                  onPressed: () {},
//                  child: Icon(Icons.chat, color: Theme.of(context).primaryColor),
//                  color: Theme.of(context).accentColor,
//                  shape: StadiumBorder(),
//                ),
//              ),
              ],
            ),
          ),
          IntrinsicWidth(
            child: Container(
              height: 50.0,
              child: TextField(
                controller: nameController,
                maxLines: 1,
                decoration: isEditMode
                    ? InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        border: new UnderlineInputBorder(
                            borderSide: new BorderSide(
                                color: Theme.of(context).primaryColor)))
                    : InputDecoration(border: InputBorder.none),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .merge(TextStyle(color: Theme.of(context).primaryColor)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
