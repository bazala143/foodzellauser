import 'dart:convert';

import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/dashboard_master.dart';
import 'package:app/src/models/order_list_master.dart';
import 'package:app/src/models/status_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class OwnerHomeController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<StatusDetails> statusList = new List();
  List<OrderDetails> orderList = new List();
  StatusDetails selectedStatus;
  DashBoardMaster dashBoardMaster;
  int startLimit = 0;
  bool isOrderApi = false;
  bool isEmptyOrderList = false;

  OwnerHomeController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  Future<void> getStatusList({BuildContext context}) async {
    StatusMaster deliveryBoysMaster = await ownerRepository.getStatusList(
        onStartLoading: () {}, onStopLoading: () {}, onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      statusList.clear();
      if (deliveryBoysMaster.result.length > 0) {
        StatusDetails statusDetails = new StatusDetails();
        statusDetails.id = 0;
        statusDetails.name = "ALL";
        statusList.add(statusDetails);
        statusList.addAll(deliveryBoysMaster.result);
        selectedStatus = statusList[0];
        getOrderList(context: context, orderStatus: selectedStatus.id);
        notifyListeners();
      } else {
        CommonUtils.showToast(deliveryBoysMaster.message);
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }
  }

  String getListParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.USER_ID] = userRepository.currentUser.value.id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getOrderList({BuildContext context, int orderStatus}) async {
    isOrderApi = true;
    if (statusList == 0) {
      isEmptyOrderList = false;
    }
    notifyListeners();

    OrderListMaster deliveryBoysMaster = await ownerRepository.getOrderList(
        params: getOrderListParams(orderStatus),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      orderList.clear();
      if (deliveryBoysMaster.success) {
        if (deliveryBoysMaster.result.length > 0) {
          orderList.addAll(deliveryBoysMaster.result);
          notifyListeners();
        } else {
          if (startLimit == 0) {
            isEmptyOrderList = true;
          }
          CommonUtils.showToast(deliveryBoysMaster.message);
        }
      } else {
        if (startLimit == 0) {
          isEmptyOrderList = true;
        }
      }
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }
    isOrderApi = false;
    notifyListeners();
  }

  String getOrderListParams(int orderStatus) {
    Map<String, dynamic> map = new Map();
    map[ApiParams.OWNER_ID] = userRepository.currentUser.value.id;
    map[ApiParams.START_LIMIT] = startLimit;
    map[ApiParams.ORDER_STATUS] = orderStatus;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  Future<void> getEarningsApi({BuildContext context}) async {
    isOrderApi = true;
    notifyListeners();

    dashBoardMaster = await ownerRepository.getDashBoardDetails(
        params: getEarningsParams(),
        onStartLoading: () {},
        onStopLoading: () {},
        onNoInternet: () {});

    if (dashBoardMaster != null) {
      notifyListeners();
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
    }
    isOrderApi = false;
    notifyListeners();
  }

  String getEarningsParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.ID] = userRepository.currentUser.value.id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }
}
