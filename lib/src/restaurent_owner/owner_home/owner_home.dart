import 'package:app/generated/i18n.dart';
import 'package:app/src/elements/empty_food_widget.dart';
import 'package:app/src/elements/list_shimmer.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/models/order_list_master.dart';
import 'package:app/src/restaurent_owner/order_details/order_details_page.dart';
import 'package:app/src/restaurent_owner/owner_home/order_list_item.dart';
import 'package:app/src/restaurent_owner/owner_home/owner_home_controller.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shimmer/shimmer.dart';

class OwnerHome extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  OwnerHome({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _OwnerHomeState createState() => _OwnerHomeState();
}

class _OwnerHomeState extends StateMVC<OwnerHome> {
  OwnerHomeController _con;

  _OwnerHomeState() : super(OwnerHomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _con.getEarningsApi(context: context);
      _con.getStatusList(context: context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).profile,
          style: Theme.of(context).textTheme.title.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            _con.dashBoardMaster == null ? getHeaderShimmer() : getHeaderView(),
            getChipsLayout(),
            getOrderListView(),
          ],
        ),
      ),
    );
  }

  Widget getHeaderView() {
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(children: [
          getheaderItemView(
             S.of(context).totalEarnings, _con.dashBoardMaster.result.earning + "SAR"),
          getheaderItemView( S.of(context).totalOrders, _con.dashBoardMaster.result.orders),
          getheaderItemView(
              S.of(context).totalRestaurents, _con.dashBoardMaster.result.restaurent),
          getheaderItemView(S.of(context).totalFoodItems, _con.dashBoardMaster.result.food),
        ]),
      ),
    );
  }

  Widget getHeaderShimmer() {
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
            children: List.generate(
                3,
                (index) => Shimmer.fromColors(
                    child: Container(
                      margin: EdgeInsets.all(10.0),
                      height: 100,
                      width: 120,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Theme.of(context).accentColor),
                    ),
                    baseColor: Theme.of(context).accentColor,
                    highlightColor: Theme.of(context).primaryColor))),
      ),
    );
  }

  Widget getheaderItemView(String label, String value) {
    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            new BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
            )
          ]),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            label,
            maxLines: 2,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.subtitle1,
          )
        ],
      ),
    );
  }

  Widget getChipsLayout() {
    final shimmerChips = Shimmer.fromColors(
        child: ListView.builder(
            padding: EdgeInsets.all(10.0),
            itemCount: 10,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (_context, index) {
              return Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.only(left: 5.0, right: 10.0),
                  padding: EdgeInsets.only(
                      left: 30.0, right: 30.0, top: 10.0, bottom: 10.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      boxShadow: [
                        new BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0,
                        )
                      ],
                      borderRadius: BorderRadius.circular(30.0)),
                  child: Text(
                    "Loading",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
              );
            }),
        baseColor: Theme.of(context).accentColor,
        highlightColor: Theme.of(context).primaryColor);

    final statusListView = ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: _con.statusList.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_context, index) {
          return Align(
            alignment: Alignment.center,
            child: InkWell(
              onTap: () {
                _con.selectedStatus = _con.statusList[index];
                _con.getOrderList(
                    context: context, orderStatus: _con.statusList[index].id);
              },
              child: Container(
                margin: EdgeInsets.only(left: 5.0, right: 10.0),
                padding: EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    color: _con.selectedStatus == _con.statusList[index]
                        ? Theme.of(context).accentColor
                        : Theme.of(context).primaryColor,
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2.0,
                      )
                    ],
                    borderRadius: BorderRadius.circular(30.0)),
                child: Text(
                  _con.statusList[index].name,
                  style: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                      color: _con.selectedStatus == _con.statusList[index]
                          ? Theme.of(context).primaryColor
                          : Theme.of(context).accentColor),
                ),
              ),
            ),
          );
        });

    return Container(
      height: app.App(context).appHeight(10.0),
      child: _con.selectedStatus == null ? shimmerChips : statusListView,
    );
  }

  Widget getOrderListView() {
    final orderListView = ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: _con.orderList.length,
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (_context, index) {
          return Align(
            alignment: Alignment.center,
            child: OrderListItem(
              onViewClicked: () async {
                bool isUpdated = await Navigator.push(context,
                    MaterialPageRoute(builder: (_context) {
                  return OrderDetailsPage(
                    orderDetails: _con.orderList[index],
                  );
                })) as bool;

                if (isUpdated != null && isUpdated) {
                  _con.getOrderList(
                      context: context, orderStatus: _con.selectedStatus.id);
                }
              },
              onHeaderClicked: () {
                OrderDetails orderDetails = _con.orderList[index];
                orderDetails.isExpanded = !orderDetails.isExpanded;
                setState(() {});
              },
              orderDetails: _con.orderList[index],
            ),
          );
        });

    return Container(
      child: _con.isOrderApi
          ? ShimmerListView()
          : (_con.isEmptyOrderList
              ? EmptyFoodWidget(
                  iconData: Icons.bookmark_border,
                  message: S.of(context).noOrderFound,
                )
              : orderListView),
    );
  }
}
