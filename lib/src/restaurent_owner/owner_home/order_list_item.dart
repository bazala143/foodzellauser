import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/helpers/helper.dart';
import 'package:app/src/models/order_list_master.dart';
import 'package:app/src/restaurent_owner/owner_home/order_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderListItem extends StatelessWidget {
  OrderDetails orderDetails;
  Function onViewClicked, onEditClicked, onCancelClicked, onHeaderClicked;

  OrderListItem(
      {this.orderDetails,
      this.onViewClicked,
      this.onEditClicked,
      this.onCancelClicked,
      this.onHeaderClicked});

  @override
  Widget build(BuildContext context) {
    final idColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.of(context).order_id + orderDetails.id.toString(),
          style: Theme.of(context).textTheme.headline6,
        ),
        Text(
          orderDetails.createdAt,
          style: Theme.of(context).textTheme.subtitle1,
        )
      ],
    );

    final tvPrice = new RichText(
      text: new TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: 14.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(
              text: getTotal().toString(),
              style: Theme.of(context).textTheme.headline2),
          new TextSpan(
              text: 'SAR', style: Theme.of(context).textTheme.headline5)
        ],
      ),
    );

    final tvStatus = Align(
      alignment: Alignment.bottomRight,
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        decoration: BoxDecoration(
            color: CommonUtils.getStatusColor(orderDetails.statusId),
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(10.0),
                topLeft: Radius.circular(10.0))),
        child: Text(
          orderDetails.orderStatus,
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );

    return Column(
      children: [
        InkWell(
          onTap: onHeaderClicked,
          child: Container(
            height: app.App(context).appHeight(14),
            child: Stack(
              children: [
                Container(
                  height: app.App(context).appHeight(14),
                  margin: EdgeInsets.only(top: 10.0),
                  padding: EdgeInsets.only(
                      left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      boxShadow: [
                        new BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [idColumn, tvPrice],
                  ),
                ),
                tvStatus
              ],
            ),
          ),
        ),
        orderDetails.isExpanded
            ? Container(
                margin: EdgeInsets.only(top: 5.0),
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2.0,
                      )
                    ],
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10.0)),
                child: ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: orderDetails.foods.length,
                          itemBuilder: (context, index) {
                            return OrderItem(
                              food: orderDetails.foods[index],
                            );
                          }),
                      getColumn(context),
                    ],
                  ),
                ),
              )
            : Container(
                height: 1,
                width: 1,
              ),
        getButtonView(context)
      ],
    );
  }

  Widget getColumn(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        children: [
          getPriceView(
              context,
              S.of(context).delivery_fee,
              double.parse(orderDetails.deliveryFee.toString()),
              Theme.of(context).textTheme.headline6),
          getPriceView(
              context,
              S.of(context).tax,
              double.parse(orderDetails.tax.toString()),
              Theme.of(context).textTheme.headline6),
          getPriceView(context, S.of(context).total, getTotal(),
              Theme.of(context).textTheme.headline6),
        ],
      ),
    );
  }

  Widget getPriceView(
      BuildContext context, String label, double price, TextStyle textStyle) {
    final tvLable = Text(
      label,
      style: textStyle,
    );
    final tvPrice = Helper.getPrice(price, context);
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [tvLable, tvPrice],
      ),
    );
  }

  Widget getButtonView(BuildContext context) {
    final tvView = InkWell(
      onTap: onViewClicked,
      child: myButton(label: "View", color: Colors.blue, context: context),
    );
    /*  final tvEdit = InkWell(
        onTap: onEditClicked,
        child: myButton(label: "Edit", color: Colors.green, context: context));*/
    /* final tvCancel = InkWell(
        onTap: onCancelClicked,
        child: myButton(label: "Cancel", color: Colors.red, context: context));*/

    return Container(
      margin: EdgeInsets.only(top: 10.0),
      height: 40.0,
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [tvView],
      ),
    );
  }

  Widget myButton({String label, Color color, BuildContext context}) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
      decoration: BoxDecoration(
          border: Border.all(color: color, width: 1),
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(30.0)),
      child: Text(
        label,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }

  double getTotal() {
    double sum = 0;
    for (FoodItems foodItems in orderDetails.foods) {
      sum = sum +
          (double.parse(foodItems.price) * double.parse(foodItems.quantity));
    }

    sum = sum +
        double.parse(orderDetails.deliveryFee) +
        double.parse(orderDetails.tax);

    return sum;
  }
}
