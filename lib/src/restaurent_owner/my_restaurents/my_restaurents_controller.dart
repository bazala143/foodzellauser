import 'dart:convert';

import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyRestaurentsController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<RestaurentDetails> restaurentList = new List();
  bool isApiLoading = true;
  int startLimit = 0;
  int totalCount = 0;
  bool isEmptyList = false;

  MyRestaurentsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  Future<void> getRestauretLIst({BuildContext context}) async {
    isApiLoading = true;
    isEmptyList = false;
    notifyListeners();
    RestaurentListMaster deliveryBoysMaster =
        await ownerRepository.getRestaurentsList(
            params: getListParams(),
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (deliveryBoysMaster != null) {
      totalCount = deliveryBoysMaster.totalCount;
      if (startLimit == 0) {
        restaurentList.clear();
      }
      if (deliveryBoysMaster.result.length > 0) {
        restaurentList.addAll(deliveryBoysMaster.result);
        notifyListeners();
      } else {
        if (startLimit == 0) {
          isEmptyList = true;
        }
     //   CommonUtils.showToast(deliveryBoysMaster.message);
      }
      isApiLoading = false;
      notifyListeners();
    } else {
      CommonUtils.showToast("oops sometihing went wrong");
      isApiLoading = false;
      notifyListeners();
    }
  }

  String getListParams() {
    Map<String, dynamic> map = new Map();
    map[ApiParams.START_LIMIT] = startLimit;
    map[ApiParams.USER_ID] = userRepository.currentUser.value.id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }
}
