import 'package:app/generated/i18n.dart';
import 'package:app/src/elements/empty_food_widget.dart';
import 'package:app/src/elements/restaurent_list_shimmer.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/restaurent_owner/add_restaurent/add_restaurant_view.dart';
import 'package:app/src/restaurent_owner/my_restaurents/my_restaurents_controller.dart';
import 'package:app/src/restaurent_owner/my_restaurents/restaurent_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MyRestaurentView extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  MyRestaurentView({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _MyRestaurentViewState createState() => _MyRestaurentViewState();
}

class _MyRestaurentViewState extends StateMVC<MyRestaurentView> {
  MyRestaurentsController _con;

  _MyRestaurentViewState() : super(MyRestaurentsController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      _con.getRestauretLIst(context: context);
    });
  }

  @override
  Widget build(BuildContext context) {
    final restaurentListview = ListView.builder(
        padding: EdgeInsets.all(10.0),
        shrinkWrap: true,
        itemCount: _con.isApiLoading
            ? _con.restaurentList.length + 1
            : _con.restaurentList.length,
        physics: ClampingScrollPhysics(),
        itemBuilder: (_context, index) {
          if (index == _con.restaurentList.length) {
            return CommonUtils.getBottomProgressBar(context);
          } else {
            return RestaurentItem(
              restaurentDetails: _con.restaurentList[index],
              onClick: () async {
                bool isAdded = await Navigator.push(context,
                    MaterialPageRoute(builder: (context) {
                  return AddRestaurentView(
                    id: _con.restaurentList[index].id.toString(),
                  );
                }));
                if (isAdded != null && isAdded) {
                  _con.startLimit = 0;
                  _con.getRestauretLIst(context: context);
                }
              },
            );
          }
        });

    final notificationListview = NotificationListener<ScrollNotification>(
      child: restaurentListview,
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (_con.startLimit < _con.totalCount && !_con.isApiLoading) {
            _con.startLimit = _con.startLimit + 5;
            _con.getRestauretLIst(context: context);
          }
        }
      },
    );

    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            bool isAdded = await Navigator.push(context,
                MaterialPageRoute(builder: (_context) {
              return AddRestaurentView();
            })) as bool;

            if (isAdded) {
              _con.startLimit = 0;
              _con.getRestauretLIst(context: context);
            }
          },
          child: Center(
            child: Text(
              S.of(context).add,
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
        ),
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
            onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).accentColor,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).myRestaurents,
            style: Theme.of(context).textTheme.headline6.merge(TextStyle(
                letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
          ),
        ),
        key: _con.scaffoldKey,
        body: (_con.isApiLoading && _con.startLimit == 0)
            ? RestaurentListShimmer()
            : (_con.isEmptyList
                ? EmptyFoodWidget(
                    message: S.of(context).noRestaurentFound,
                    iconData: Icons.restaurant)
                : notificationListview));
  }
}
