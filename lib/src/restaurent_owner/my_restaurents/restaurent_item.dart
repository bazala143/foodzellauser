import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/helpers/helper.dart';
import 'package:app/src/models/restaurent_list_master.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class RestaurentItem extends StatelessWidget {
  final RestaurentDetails restaurentDetails;
  Function onClick;
  RestaurentItem({this.restaurentDetails, this.onClick});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        width: app.App(context).appWidth(100),
        margin: EdgeInsets.only(top: 15),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.5),
                blurRadius: 15,
                offset: Offset(0, 5)),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // Image of the card
            Hero(
              tag: restaurentDetails.id,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                child: CachedNetworkImage(
                  height: 150,
                  fit: BoxFit.cover,
                  imageUrl: restaurentDetails.image,
                  placeholder: (context, url) => Image.asset(
                    'assets/img/loading.gif',
                    fit: BoxFit.cover,
                    height: 150,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          restaurentDetails.name,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                        Text(
                          Helper.skipHtml(restaurentDetails.description),
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme.of(context).textTheme.caption,
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: Helper.getStarsList(
                              double.parse(restaurentDetails.rating)),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 15),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
