import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/api_params.dart';
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/models/message_master.dart';
import 'package:app/src/models/restaurent_detail_master.dart';
import 'package:app/src/models/restaurent_request.dart';
import 'package:app/src/repository/owner_repository.dart' as ownerRepository;
import 'package:app/src/repository/settings_repository.dart' as sett;
import 'package:app/src/repository/user_repository.dart' as userRepository;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_flutter_platform_interface/src/types/location.dart';
import 'package:google_maps_webservice/places.dart' as pred;
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddRestaurentController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final mobileController = TextEditingController();
  final adressController = TextEditingController();
  final emailController = TextEditingController();
  final adminFeeController = TextEditingController();
  final deliveryFeeController = TextEditingController();
  final descriptionController = TextEditingController();
  final informationController = TextEditingController();

  List<int> boysIdsList = new List();
  bool isAddApiLoading = false;

  List<DeliveryBoysDetails> selectedBoysList = new List();

  List<Marker> allMarkers = <Marker>[];
  LocationData currentLocation;
  CameraPosition cameraPosition;
  Completer<GoogleMapController> mapController = Completer();
  bool isAddressFetching = false;

  String fetchedAddress = "Full address";
  String addressTitle = "Landmark";
  pred.GoogleMapsPlaces _places =
      pred.GoogleMapsPlaces(apiKey: sett.setting.value.googleMapsKey);
  LatLng selectedLatLng;

  Future<void> setDefaultLocation() async {
    cameraPosition = CameraPosition(
      target: LatLng(0.0, 0.0),
      zoom: 14.4746,
    );
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/img/my_marker.png', 120);
    final Marker marker = Marker(
        markerId: MarkerId("1001"),
        icon: BitmapDescriptor.fromBytes(markerIcon),
        anchor: Offset(0.5, 0.5),
        position: LatLng(0.0, 0.0));
    allMarkers.add(marker);
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<void> setSelectedLatLong(LatLng latLng, String fetchedAddress) async {
    allMarkers.clear();
    selectedLatLng = latLng;
    if (fetchedAddress != null) {
      adressController.text = fetchedAddress;
    }
    cameraPosition = CameraPosition(
      target: selectedLatLng,
      zoom: 10,
    );
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/img/my_marker.png', 120);
    final Marker marker = Marker(
        markerId: MarkerId("1001"),
        icon: BitmapDescriptor.fromBytes(markerIcon),
        anchor: Offset(0.5, 0.5),
        position: selectedLatLng);
    allMarkers.add(marker);
    final GoogleMapController controller = await mapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    notifyListeners();
  }

  Future<void> setSelectedBysList(List<DeliveryBoysDetails> boysList) async {
    if (selectedBoysList.length > 0) {
      selectedBoysList.clear();
    }

    if (boysIdsList.length > 0) {
      boysIdsList.clear();
    }
    //  debugger();

    if (boysList == null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String pString = await prefs.getString('list');
      List<DeliveryBoysDetails> _result = new List<DeliveryBoysDetails>();
      if (pString != null) {
        json.decode(pString).forEach((v) {
          _result.add(new DeliveryBoysDetails.fromJson(v));
        });
      }

      selectedBoysList.addAll(_result);
      for (DeliveryBoysDetails deliveryBoysDetails in selectedBoysList) {
        boysIdsList.add(int.parse(deliveryBoysDetails.id));
      }
    } else {
      try {
        selectedBoysList.addAll(boysList);
      } catch (e) {
        print("Exception" + e.toString());
      }

      for (DeliveryBoysDetails deliveryBoysDetails in selectedBoysList) {
        boysIdsList.add(int.parse(deliveryBoysDetails.id));
      }
    }

    notifyListeners();
  }

  final picker = ImagePicker();
  PickedFile image;
  String imagePath = null;

  Future<void> pickFile() async {
    image = await picker.getImage(source: ImageSource.gallery);
    imagePath = image.path;
    notifyListeners();
  }

  AddRestaurentController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  Future<void> saveResaturentApi() async {
    isAddApiLoading = true;
    notifyListeners();
    RestaurentRequest request = new RestaurentRequest();
    request.name = nameController.text;
    request.address = adressController.text;
    request.phone = phoneController.text;
    request.mobile = mobileController.text;
    request.adminCommission = adminFeeController.text;
    request.deliveryFee = deliveryFeeController.text;
    request.description = descriptionController.text;
    request.information = informationController.text;
    request.latitude = selectedLatLng.latitude.toString();
    request.longitude = selectedLatLng.longitude.toString();
    request.deliveryBoy = boysIdsList;
    request.userId = userRepository.currentUser.value.id;

    await ownerRepository.addRestaurent(
        params: jsonEncode(request),
        imagePath: image.path,
        onFailed: () {
      //    CommonUtils.showToast("Failed to add");
          isAddApiLoading = false;
          notifyListeners();
        },
        onSuccess: (MessageMaster addRestaurentMaster) {
          if (addRestaurentMaster.success == true) {
        //    CommonUtils.showToast("Restaurent added");
            Navigator.pop(scaffoldKey.currentContext, true);
          } else {
            CommonUtils.showToast(addRestaurentMaster.message);
          }
          isAddApiLoading = false;
          notifyListeners();
        });
  }

  Future<void> updateResaturentApi({String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    RestaurentRequest request = new RestaurentRequest();
    request.id = id;
    request.name = nameController.text;
    request.address = adressController.text;
    request.phone = phoneController.text;
    request.mobile = mobileController.text;
    request.adminCommission = adminFeeController.text;
    request.deliveryFee = deliveryFeeController.text;
    request.description = descriptionController.text;
    request.information = informationController.text;
    request.latitude = selectedLatLng.latitude.toString();
    request.longitude = selectedLatLng.longitude.toString();
    request.deliveryBoy = boysIdsList;
    request.userId = userRepository.currentUser.value.id;

    await ownerRepository.updateRestaurent(
        params: jsonEncode(request),
        imagePath: imagePath,
        onFailed: () {
          CommonUtils.showToast("Failed to add");
          isAddApiLoading = false;
          notifyListeners();
        },
        onSuccess: (MessageMaster addRestaurentMaster) {
          if (addRestaurentMaster.success == true) {
            CommonUtils.showToast("Restaurent Updated");
            Navigator.pop(scaffoldKey.currentContext, true);
          } else {
            CommonUtils.showToast(addRestaurentMaster.message);
          }
          isAddApiLoading = false;
          notifyListeners();
        });
  }

  Future<void> getRestaurentDetails({BuildContext contxet, String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    RestaurentDetailMaster restaurentDetailMaster =
        await ownerRepository.getRestarentDetails(
            params: getOrderListParams(id),
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (restaurentDetailMaster != null) {
      if (restaurentDetailMaster.success) {
        nameController.text = restaurentDetailMaster.result.name;
        descriptionController.text = restaurentDetailMaster.result.description;
        deliveryFeeController.text =
            restaurentDetailMaster.result.deliveryFee.toString();
        adminFeeController.text =
            restaurentDetailMaster.result.adminCommission.toString();
        informationController.text = restaurentDetailMaster.result.information;
        phoneController.text = restaurentDetailMaster.result.phone;
        mobileController.text = restaurentDetailMaster.result.mobile;
        adressController.text = restaurentDetailMaster.result.address;
        imagePath = restaurentDetailMaster.result.image;
        setSelectedBysList(restaurentDetailMaster.result.deliveryBoy);
        setSelectedLatLong(
            new LatLng(double.parse(restaurentDetailMaster.result.latitude),
                double.parse(restaurentDetailMaster.result.longitude)),
            null);
      } else {
        CommonUtils.showToast(restaurentDetailMaster.message);
      }
    } else {
      CommonUtils.showToast("Oops something went wrong");
    }

    isAddApiLoading = false;
    notifyListeners();
  }

  Future<void> deleteRestaurent({BuildContext contxet, String id}) async {
    isAddApiLoading = true;
    notifyListeners();
    MessageMaster restaurentDetailMaster =
        await ownerRepository.deleteRestaurent(
            params: id,
            onStartLoading: () {},
            onStopLoading: () {},
            onNoInternet: () {});

    if (restaurentDetailMaster != null) {
      if (restaurentDetailMaster.success) {
        CommonUtils.showToast(restaurentDetailMaster.message);
        Navigator.pop(context, true);
      } else {
        CommonUtils.showToast(restaurentDetailMaster.message);
      }
    } else {
      CommonUtils.showToast("Oops something went wrong");
    }

    isAddApiLoading = false;
    notifyListeners();
  }

  String getOrderListParams(String id) {
    Map<String, dynamic> map = new Map();
    map[ApiParams.ID] = id;
    print("params" + jsonEncode(map));
    return jsonEncode(map);
  }

  bool checkValiDation() {
    if (imagePath == null) {
      CommonUtils.showToast(S.of(context).pleaseSelectRestaurentImage);
      return false;
    } else if (nameController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterRestaurentName);
      return false;
    } else if (phoneController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterPhone);
      return false;
    } else if (mobileController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterMobile);
      return false;
    } else if (adminFeeController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterAdminFee);
      return false;
    } else if (deliveryFeeController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterDeliveryFee);
      return false;
    } else if (descriptionController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterDesc);
      return false;
    } else if (informationController.text.isEmpty) {
      CommonUtils.showToast(S.of(context).pleaseEnterOtherInformation);
      return false;
    } else if (boysIdsList.length == 0) {
      CommonUtils.showToast(S.of(context).pleaseEnterDeliveryBoys);
      return false;
    } else if (selectedLatLng == null) {
      CommonUtils.showToast(S.of(context).pleaseSelectLocation);
      return false;
    }
    return true;
  }

  void removeItem(int index) {
    boysIdsList.removeAt(index);
    selectedBoysList.removeAt(index);
    notifyListeners();
  }
}
