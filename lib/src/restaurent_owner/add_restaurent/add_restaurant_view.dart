import 'dart:io';

import 'package:app/generated/i18n.dart';
import 'package:app/src/helpers/app_config.dart' as app;
import 'package:app/src/helpers/common_utils.dart';
import 'package:app/src/models/delivery_boys_master.dart';
import 'package:app/src/pages/select_location_page.dart';
import 'package:app/src/restaurent_owner/delievry_boys_list/delivery_boys_view.dart';
import 'package:app/src/restaurent_owner/delievry_boys_list/selected_boys_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'add_restaurent_controller.dart';

class AddRestaurentView extends StatefulWidget {
  final String id;

  AddRestaurentView({this.id});

  @override
  _AddRestaurentViewState createState() => _AddRestaurentViewState();
}

class _AddRestaurentViewState extends StateMVC<AddRestaurentView> {
  AddRestaurentController _con;
  String _mapStyle;

  _AddRestaurentViewState() : super(AddRestaurentController()) {
    _con = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _con.setDefaultLocation();

    Future.delayed(Duration.zero, () {
      if (widget.id != null) {
        _con.getRestaurentDetails(contxet: context, id: widget.id);
      }
    });

    rootBundle.loadString('assets/cfg/map_style.txt').then((string) {
      setState(() {
        _mapStyle = string;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final imageView = InkWell(
      onTap: () {
        _con.pickFile();
      },
      child: Container(
        width: app.App(context).appWidth(100),
        height: app.App(context).appHeight(30),
        decoration: BoxDecoration(
          color: app.Colors().accentColor(0.2),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          clipBehavior: Clip.antiAlias,
          child: _con.imagePath != null
              ? FadeInImage(
                  width: app.App(context).appWidth(100),
                  height: app.App(context).appHeight(30),
                  fit: BoxFit.cover,
                  placeholder: AssetImage("assets/img/ic_place_holder.jpg"),
                  image: _con.imagePath.startsWith("http")
                      ? NetworkImage(_con.imagePath)
                      : FileImage(new File(_con.imagePath)))
              : FadeInImage(
                  fit: BoxFit.cover,
                  placeholder: AssetImage("assets/img/ic_place_holder.jpg"),
                  image: AssetImage("assets/img/ic_place_holder.jpg")),
        ),
      ),
    );

    final btnSave = InkWell(
      onTap: () {
        if (_con.checkValiDation()) {
          _con.saveResaturentApi();
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).save,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnDelete = InkWell(
      onTap: () {
        _con.deleteRestaurent(contxet: context, id: widget.id);
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          "Delete",
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnUpdate = InkWell(
      onTap: () {
        if (_con.checkValiDation()) {
          _con.updateResaturentApi(id: widget.id);
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(6.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).update,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnRow = Container(
      child: Row(
        children: [
          Flexible(
            child: btnDelete,
            flex: 1,
          ),
          Flexible(
            child: btnUpdate,
            flex: 1,
          )
        ],
      ),
    );

    final selectedBoysListView = _con.selectedBoysList.length == 0
        ? Container(
            height: 1,
            width: 1,
          )
        : Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: app.App(context).appHeight(15.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  physics: ClampingScrollPhysics(),
                  itemCount: _con.selectedBoysList.length,
                  itemBuilder: (_context, index) {
                    return SelectedBoysItem(_con.selectedBoysList[index], () {
                      _con.removeItem(index);
                    });
                  }),
            ),
          );

    final googleMap = _con.cameraPosition == null
        ? Container(
            height: 1,
            width: MediaQuery.of(context).size.width,
          )
        : Container(
            margin: EdgeInsets.only(top: 10.0),
            height: app.App(context).appHeight(20.0),
            decoration: BoxDecoration(
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 0.5)],
                borderRadius: BorderRadius.circular(10.0)),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              clipBehavior: Clip.antiAlias,
              child: GoogleMap(
                mapType: MapType.normal,
                zoomControlsEnabled: false,
                initialCameraPosition: _con.cameraPosition,
                markers: Set.from(_con.allMarkers),
                onMapCreated: (GoogleMapController controller) {
                  _con.mapController.complete(controller);
                  controller.setMapStyle(_mapStyle);
                },
              ),
            ),
          );

    final btnAddDelivery = InkWell(
      onTap: () async {
        List<DeliveryBoysDetails> list = await Navigator.push(context,
            MaterialPageRoute(builder: (_context) {
          return DeliveryBoysView(_con.selectedBoysList);
        }));
        if (list != null && list.length > 0) {
          _con.setSelectedBysList(null);
        } else {
          CommonUtils.showToast("No selection");
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
        decoration: BoxDecoration(
          color: app.Colors().accentColor(0.2),
          borderRadius: BorderRadius.circular(30.0),
        ),
        height: app.App(context).appHeight(7.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).addDeliveryBoys,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    final btnSelectLocation = InkWell(
      onTap: () async {
        var list = await Navigator.push(context,
            MaterialPageRoute(builder: (_context) {
          return SelectLocationWidget(from: "AddRestaurentView");
        })) as List;

        LatLng latLng = list[0] as LatLng;
        String address = list[1] as String;

        if (map != null && latLng != null && address != null) {
          _con.setSelectedLatLong(latLng, address);
        }
      },
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
        decoration: BoxDecoration(
          color: app.Colors().accentColor(0.2),
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: app.App(context).appHeight(7.0),
        width: app.App(context).appWidth(100),
        child: Text(
          S.of(context).selectLocation,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).createRestaurent,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(
              letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
      ),
      key: _con.scaffoldKey,
      bottomNavigationBar: Visibility(
        child: Container(
          child: widget.id == null ? btnSave : btnRow,
        ),
        visible: !_con.isAddApiLoading,
      ),
      body: _con.isAddApiLoading
          ? Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Form(
              child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: [
                  imageView,
                  getLabeledInput(
                      label: S.of(context).name,
                      controller: _con.nameController,
                      textInputType: TextInputType.text,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).phone,
                      controller: _con.phoneController,
                      textInputType: TextInputType.phone,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).mobile,
                      controller: _con.mobileController,
                      textInputType: TextInputType.phone,
                      maxlines: 1),
                  googleMap,
                  btnSelectLocation,
                  getLabeledInput(
                      label: S.of(context).address,
                      controller: _con.adressController,
                      textInputType: TextInputType.text,
                      maxlines: 3),
                  getLabeledInput(
                      label: S.of(context).delivery_fee,
                      controller: _con.deliveryFeeController,
                      textInputType: TextInputType.number,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).adminFee,
                      controller: _con.adminFeeController,
                      textInputType: TextInputType.number,
                      maxlines: 1),
                  getLabeledInput(
                      label: S.of(context).description,
                      controller: _con.descriptionController,
                      textInputType: TextInputType.text,
                      maxlines: 4),
                  getLabeledInput(
                      label: S.of(context).information,
                      controller: _con.informationController,
                      textInputType: TextInputType.text,
                      maxlines: 4),
                  selectedBoysListView,
                  btnAddDelivery,
                ],
              ),
            )),
    );
  }

  Widget getLabeledInput(
      {String label,
      int maxlines,
      TextEditingController controller,
      TextInputType textInputType}) {
    final tvLable = Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        label,
        style: Theme.of(context).textTheme.button,
      ),
    );

    final inputField = Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      decoration: BoxDecoration(
          color: app.Colors().accentColor(0.1),
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: app.Colors().accentColor(0.2))),
      child: TextField(
        controller: controller,
        keyboardType: textInputType,
        textAlign: TextAlign.start,
        textInputAction: TextInputAction.next,
        maxLines: maxlines,
        onSubmitted: (_) {
          if (controller == _con.informationController) {
            FocusScope.of(context).unfocus();
          } else {
            FocusScope.of(context).nextFocus();
          }
        },
        style: Theme.of(context).textTheme.button,
        decoration: InputDecoration(border: InputBorder.none),
      ),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        tvLable,
        SizedBox(
          height: 10.0,
        ),
        inputField
      ],
    );
  }
}
