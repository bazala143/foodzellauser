import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class UserListShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        shrinkWrap: true,
        itemBuilder: (_context, index) {
          return getShimmerItem(_context);
        });
  }

  Widget getShimmerItem(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 10.0),
      elevation: 2.0,
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
        child: InkWell(
          onTap: () {},
          child: Align(
            alignment: Alignment.center,
            child: Container(
              child: Shimmer.fromColors(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                        flex: 2,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          child: Container(
                            color: Colors.grey,
                            height: 40,
                            width: 40,
                          ),
                        )),
                    SizedBox(width: 15),
                    Flexible(
                      flex: 8,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "name",
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  maxLines: 2,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .merge(TextStyle(
                                          color: Theme.of(context).hintColor)),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            "foodzela@fmail.com",
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.caption,
                          )
                        ],
                      ),
                    ),
                    Spacer(),
                    Flexible(
                        flex: 2,
                        child: Icon(
                          Icons.check_circle,
                          color: Theme.of(context).accentColor,
                        ))
                  ],
                ),
                baseColor: Colors.black12,
                highlightColor: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
