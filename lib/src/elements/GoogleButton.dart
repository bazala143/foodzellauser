import 'package:app/generated/i18n.dart';
import 'package:flutter/material.dart';

class GoogleButton extends StatelessWidget {
  const GoogleButton(
      {Key key,
      this.image,
      this.text,
      @required this.color,
      @required this.onPressed})
      : super(key: key);

  final Color color;
  final String image;
  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: this.color.withOpacity(0.4),
                blurRadius: 40,
                offset: Offset(0, 15)),
            BoxShadow(
                color: this.color.withOpacity(0.4),
                blurRadius: 13,
                offset: Offset(0, 3))
          ],
          borderRadius: BorderRadius.all(Radius.circular(100)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              image ?? "assets/img/ic_google.png",
              height: 20,
              width: 20,
            ),
            SizedBox(
              width: 20.0,
            ),
            Text(
              text ?? S.of(context).loginWithGoogle,
              style:
                  TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
