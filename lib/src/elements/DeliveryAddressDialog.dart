import 'dart:convert';

import 'package:app/generated/i18n.dart';
import 'package:app/src/models/google_address.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../helpers/checkbox_form_field.dart';
import '../models/address.dart';

// ignore: must_be_immutable
class DeliveryAddressDialog {
  BuildContext context;
  Address address;
  ValueChanged<Address> onChanged;
  GlobalKey<FormState> _deliveryAddressFormKey = new GlobalKey<FormState>();

  DeliveryAddressDialog({this.context, this.address, this.onChanged}) {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
//            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            titlePadding: EdgeInsets.fromLTRB(16, 25, 16, 0),
            title: Row(
              children: <Widget>[
                Icon(
                  Icons.place,
                  color: Theme.of(context).hintColor,
                ),
                SizedBox(width: 10),
                Text(
                  S.of(context).add_delivery_address,
                  style: Theme.of(context).textTheme.body2,
                )
              ],
            ),
            children: <Widget>[
              Form(
                key: _deliveryAddressFormKey,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: new TextFormField(
                        style: TextStyle(color: Theme.of(context).hintColor),
                        keyboardType: TextInputType.text,
                        decoration: getInputDecoration(
                            hintText: S.of(context).home_address,
                            labelText: S.of(context).description),
                        initialValue: address.description?.isNotEmpty ?? false
                            ? address.description
                            : null,
                        validator: (input) => input.trim().length == 0
                            ? 'Not valid address description'
                            : null,
                        onSaved: (input) => address.description = input,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: new TextFormField(
                        style: TextStyle(color: Theme.of(context).hintColor),
                        keyboardType: TextInputType.text,
                        decoration: getInputDecoration(
                            hintText: S.of(context).hint_full_address,
                            labelText: S.of(context).full_address),
                        initialValue: address.address?.isNotEmpty ?? false
                            ? address.address
                            : null,
                        validator: (input) => input.trim().length == 0
                            ? 'Not valid address'
                            : null,
                        onFieldSubmitted: (value) {
                          _getLatLongFromAddress(value, address);
                        },
                        onSaved: (input) {
                          address.address = input;
                        },
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: CheckboxFormField(
                        context: context,
                        initialValue: address.isDefault ?? false,
                        onSaved: (input) => address.isDefault = input,
                        title: Text('Make it default'),
                      ),
                    )
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      S.of(context).cancel,
                      style: TextStyle(color: Theme.of(context).hintColor),
                    ),
                  ),
                  MaterialButton(
                    onPressed: _submit,
                    child: Text(
                      S.of(context).save,
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.end,
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.body1.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).hintColor.withOpacity(0.2))),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).hintColor)),
      hasFloatingPlaceholder: true,
      labelStyle: Theme.of(context).textTheme.body1.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  void _submit() {
    if (_deliveryAddressFormKey.currentState.validate()) {
      _deliveryAddressFormKey.currentState.save();
      onChanged(address);
      Navigator.pop(context);
    }
  }

  // https://maps.googleapis.com/maps/api/geocode/json?address=Nava%20bilodara,Karmviar%20nagar,Nadiad&key=AIzaSyC3YYz8jqvHY3Yup1lzIdlU51FsjHKH5yE
  _getLatLongFromAddress(String strAddress, Address address) async {
    final String url =
        'https://maps.googleapis.com/maps/api/geocode/json?address=$strAddress&key=AIzaSyC3YYz8jqvHY3Yup1lzIdlU51FsjHKH5yE';
    print(url);
    final response = await http.get(url);
    if (response != null) {
      GoogleAdress googleAdress =
          GoogleAdress.fromJson(jsonDecode(response.body));
      Results results = googleAdress.results[0];
      Geometry geometry = results.geometry;
      address.latitude = geometry.location.lat.toString();
      address.longitude = geometry.location.lng.toString();
      print("response " + geometry.location.lat.toString());
    } else {
      print("response is null");
    }
  }
}
