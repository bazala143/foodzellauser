import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        shrinkWrap: true,
        padding: EdgeInsets.all(10.0),
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return getShimmerItem(context);
        });
  }

  Widget getShimmerItem(BuildContext context) {
    final column = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 10,
          width: 100,
          color: Colors.grey,
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          height: 5,
          width: 150,
          color: Colors.grey,
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          height: 2,
          width: 150,
          color: Colors.grey,
        )
      ],
    );

    return Shimmer.fromColors(
        child: Container(
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Colors.transparent, blurRadius: 2.0)
          ], borderRadius: BorderRadius.circular(10.0)),
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(5.0)),
                height: 80,
                width: 80,
              ),
              SizedBox(
                width: 10.0,
              ),
              column
            ],
          ),
        ),
        baseColor: Colors.grey,
        highlightColor: Colors.white);
  }
}
