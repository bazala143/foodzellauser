import 'package:app/src/helpers/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class RestaurentListShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        shrinkWrap: true,
        itemBuilder: (_context, index) {
          return getShimmerItem(_context);
        });
  }

  Widget getShimmerItem(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 10.0),
      elevation: 2.0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
        child: InkWell(
          onTap: () {},
          child: Align(
            alignment: Alignment.center,
            child: Container(
              child: Shimmer.fromColors(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    // Image of the card
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      child: Container(
                        height: 150,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Restaurent name",
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: Theme.of(context).textTheme.subhead,
                                ),
                                Text(
                                  "Some information about restaurent",
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children:
                                      Helper.getStarsList(double.parse("5.0")),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 15),
                        ],
                      ),
                    )
                  ],
                ),
                baseColor: Colors.black12,
                highlightColor: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
