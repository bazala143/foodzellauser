import UIKit
import Flutter
import GoogleMaps
import GoogleSignIn
import AuthenticationServices

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        
        GIDSignIn.sharedInstance().clientID = "641250324724-7ifblo4oehnq4fa0l0p8uuiburdam4jg.apps.googleusercontent.com"
        GMSServices.provideAPIKey("AIzaSyC3YYz8jqvHY3Yup1lzIdlU51FsjHKH5yE")
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }
        
        GeneratedPluginRegistrant.register(with: self)
        
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let appleSigninChannel = FlutterMethodChannel(name: "com.foodzela.app.channel",
                                                       binaryMessenger: controller.binaryMessenger)
        appleSigninChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if call.method == "GoogleLogin" {
                GIDSignIn.sharedInstance().delegate = self
                GIDSignIn.sharedInstance()?.presentingViewController = controller
                GIDSignIn.sharedInstance().signIn()
            } else if call.method == "AppleLogin" {
                if #available(iOS 13.0, *) {
                    let appleIDProvider = ASAuthorizationAppleIDProvider()
                    let request = appleIDProvider.createRequest()
                    request.requestedScopes = [.fullName, .email]
                    
                    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                    authorizationController.delegate = self
                    authorizationController.presentationContextProvider = self
                    authorizationController.performRequests()
                } else {
                    // Fallback on earlier versions
                }
                
            }
        })
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    override func application(_ application: UIApplication,
                              open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    @available(iOS 9.0, *)
    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func showAlert(_ title: String!, message: String, buttonTitle: String = "OK") {
        if let controller = window?.rootViewController {
            let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { (action) in
                
            }))
            controller.present(alert, animated: true, completion: nil)
        }
    }
}

extension AppDelegate: GIDSignInDelegate {
    //MARK: - GIDSignInDelegate Methods
    //MARK: -
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            
            self.showAlert("Error", message: error.localizedDescription)
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        var imageURL = ""
        if (user.profile.hasImage) {
            imageURL = user.profile.imageURL(withDimension: 200)?.path ?? ""
        }
        print("userId : \(String(describing: userId))")
        print("idToken : \(String(describing: idToken))")
        print("fullName : \(String(describing: fullName))")
        print("givenName : \(String(describing: givenName))")
        print("familyName : \(String(describing: familyName))")
        print("email : \(String(describing: email))")
        
        let dict = [
            "name": fullName,
            "email": email,
            "profilePic": imageURL
        ]
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(dict) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
                let googleSigninChannel = FlutterMethodChannel(name: "com.foodzela.app.channel",
                                                               binaryMessenger: controller.binaryMessenger)
                googleSigninChannel.invokeMethod("GoogleLogin", arguments: jsonString)
            } else {
                self.showAlert("Error", message: "Something went wrong.")
            }
        } else {
            self.showAlert("Error", message: "Something went wrong.")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        print("\(error.localizedDescription)")
        self.showAlert("Error", message: error.localizedDescription)
        
        
    }
}
extension AppDelegate: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            // For the purpose of this demo app, store the `userIdentifier` in the keychain.
            self.saveUserInKeychain(userIdentifier)
            
            // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
            //self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
            var name = ""
            if let firstName = fullName?.givenName {
                name = firstName
            }
            if let familyName = fullName?.familyName {
                if (name.count > 0) {
                    name.append(" ")
                }
                name.append(familyName)
            }
            
            let dict = [
                "name": name,
                "email": email,
                "profilePic": ""
            ]
            
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(dict) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
                    let googleSigninChannel = FlutterMethodChannel(name: "com.foodzela.app.channel",
                                                                   binaryMessenger: controller.binaryMessenger)
                    googleSigninChannel.invokeMethod("AppleLogin", arguments: jsonString)
                } else {
                    self.showAlert("Error", message: "Something went wrong.")
                }
            } else {
                self.showAlert("Error", message: "Something went wrong.")
            }
        
        case let passwordCredential as ASPasswordCredential:
        
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }
            
        default:
            break
        }
    }
    
    private func saveUserInKeychain(_ userIdentifier: String) {
        do {
            try KeychainItem(service: Bundle.main.bundleIdentifier!, account: "userIdentifier").saveItem(userIdentifier)
        } catch {
            print("Unable to save userIdentifier to keychain.")
        }
    }
    
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        showAlert("Keychain Credential Received", message: message, buttonTitle: "Dismiss")
    }
    
    /// - Tag: did_complete_error
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension AppDelegate: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        if let ctr = window?.rootViewController {
            return ctr.view.window!
        }
        return self.window
    }
}
