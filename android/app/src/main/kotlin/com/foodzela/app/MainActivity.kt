package com.foodzela.app

import android.content.Intent
import android.os.Bundle
import androidx.annotation.NonNull
import io.flutter.app.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterFragmentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
        // getWindow().addFlags(LayoutParams.FLAG_SECURE)




    }
}